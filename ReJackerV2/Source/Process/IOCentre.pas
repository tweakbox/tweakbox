unit IOCentre;

interface

uses
  Windows, KOL;

type
  TLiteralType = (ltLiteral, ltPascal, ltShort, ltLen, ltUnicodeLiteral, ltWidePascal, ltShortUnicode, ltUnicodeLen);
  PGuruStringRecItem = ^GuruStringRecItem;
  GuruStringRecItem = record
    Offset: Integer; // startpos
    Length: Integer; // crbyte
    LatinCount: Integer;
    HanziCount: Integer;
    SwitchCount: Integer;
    RP: Double;
    Ref: Integer;
    LinkType: TLiteralType;
    RecLine: AnsiString; // yes, not KOLString
    RawLine: AnsiString; // before convert
    HaveEscape: Boolean;
  end;
  GuruStringArray = Array of GuruStringRecItem;

  // TODO: I'll merge it to an more common unit than iocentre and addonfuncunt
  TExfMode = (emBIG5, emGB2312, emGBK, emJIS);
  TRipMode = (rmBIG5, rmGB2312, rmGBK, rmJIS);
  TReclineType = (rtLatin, rtCJK, rtMixed); 
  tagBinarySourceRecItem = record
    Checked: Boolean;
    DisplayName: KOLString;
    InputPath: KOLString;
    SchemePath: KOLString;
    PlainPath: KOLString;
    LocalizedPath: KOLString;
    WriteBackPath: KOLString;
    NiceSwitch: Boolean;
    DetectedAsPE: Boolean;
    StrictSwitch: Boolean;
    RipEng: Boolean;
    RipHan: Boolean;
    RipMix: Boolean;
    LimEng: Integer;
    LimHan: Integer;
    LimMix: Integer;
    ExfMode: TExfMode;
    RipMode: TRipMode;
    noWholeBuff: Boolean;
  end;

type
  PProjectWriter = ^TProjectWriter;
  TProjectWriter = Object(TObj)
  private
    fGuruCount: Integer;
    fGuruStrings: GuruStringArray;
    fSchemeOption: tagBinarySourceRecItem;
  protected
    constructor Create;
    destructor Destroy; virtual;
  public
    function AddGuruRecord(const GuruRecord: GuruStringRecItem): Boolean;
    function WriteTranlateTask(Schemename, Basename: KOLString; NiceSwitch: Boolean = True): Boolean; overload;
    function WriteTranlateTask(): Boolean; overload;
  public
    property GuruCount: Integer read fGuruCount;
  end;

function NewProjectWriter(): PProjectWriter;
function NewProjectWriterEx(const SchemeOption: tagBinarySourceRecItem): PProjectWriter;

function MergeSchemeFromIDs(SchemeList, CleanList: PStrList): Boolean;
function InitBinarySourceFromSource(var Item: tagBinarySourceRecItem; Filename: KOLString): Boolean;
function InitBinarySourceFromScheme(var Item: tagBinarySourceRecItem; Filename: KOLString): Boolean;

function LinkType2Str(LinkType: TLiteralType): AnsiString;
function LinkType2Char(LinkType: TLiteralType): AnsiChar;
function Char2LinkType(Char: AnsiChar): TLiteralType;


implementation

uses
  PEFiltersUnt;

function NewProjectWriter(): PProjectWriter;
begin
  New(Result, Create);
end;

function NewProjectWriterEx(const SchemeOption: tagBinarySourceRecItem): PProjectWriter;
begin
  New(Result, Create);
  with Result^ do
  begin
    fSchemeOption := SchemeOption;
  end;
end;  

function TProjectWriter.AddGuruRecord(const GuruRecord: GuruStringRecItem): Boolean;
begin
  //Result := False;
  if Length(fGuruStrings) < fGuruCount + 1 then
  begin
    SetLength(fGuruStrings, (fGuruCount div 1024 + 1) * 1024);
  end;
  //count as index
  fGuruStrings[fGuruCount] := GuruRecord; // @copyrecord
  Inc(fGuruCount);
  Result := True;
end;

function TProjectWriter.WriteTranlateTask(Schemename, Basename: KOLString; NiceSwitch: Boolean): Boolean;
begin
  fSchemeOption.SchemePath := Schemename;
  fSchemeOption.PlainPath := Basename;
  fSchemeOption.NiceSwitch := NiceSwitch;
  Result := WriteTranlateTask();
end;

function TProjectWriter.WriteTranlateTask(): Boolean;
var
  I: Integer;
  GoodFile, FuzzyFile, FakeFile, CurrFile: PStream;
  SchemeFile: PStream;
  GoodFilename, FuzzyFilename, FakeFilename, PreFilename: KOLString;
  OptionStr: AnsiString;
begin
  Result := False;
  if fGuruCount <= 0 then Exit;

  PreFilename := ExtractFilePath(fSchemeOption.PlainPath) + ExtractFileNameWOext(fSchemeOption.PlainPath);
  SchemeFile := NewWriteFileStream(fSchemeOption.SchemePath);
  // write Scheme Settings
  // TODO: refactory Writing n Getting
  SchemeFile.WriteStr(fSchemeOption.DisplayName + #$0D#$0A); // 0
  SchemeFile.WriteStr(fSchemeOption.InputPath + #$0D#$0A);   // 1
  SchemeFile.WriteStr(fSchemeOption.PlainPath + #$0D#$0A);   // 2
  SchemeFile.WriteStr(fSchemeOption.LocalizedPath + #$0D#$0A); // 3
  SchemeFile.WriteStr(fSchemeOption.WriteBackPath + #$0D#$0A); // 4
  OptionStr := Int2Str(Byte(fSchemeOption.NiceSwitch)) + Int2Str(Byte(fSchemeOption.RipMode)) + Int2Str(Byte(fSchemeOption.ExfMode)); //5
  SchemeFile.WriteStr(OptionStr + #$0D#$0A);
  
  GoodFile := NewMemoryStream();
  FuzzyFile := NewMemoryStream();
  FakeFile := NewMemoryStream();
  CurrFile := nil;
  for I := 0 to fGuruCount - 1 do
  begin
    if fGuruStrings[I].RP >= 60 then
    begin
      // goodman
      CurrFile := GoodFile;
    end;
    if (fGuruStrings[I].RP < 60) and (fGuruStrings[I].RP > 40) then
    begin
      // questionman
      CurrFile := FuzzyFile;
    end;
    if fGuruStrings[I].RP <= 40 then
    begin
      // wrong man
      CurrFile := FakeFile;
    end;

    CurrFile.WriteStr(Int2Digs(I, 3));
    CurrFile.WriteStr(#$09);
{$IFDEF DEBUG}
    CurrFile.WriteStr(Double2Str(Trunc(fGuruStrings[I].RP * 1000) / 1000));
    CurrFile.WriteStr(#$09);
    CurrFile.WriteStr('S:' + Int2Digs(fGuruStrings[I].SwitchCount, 2));
    CurrFile.WriteStr(#$09);
    CurrFile.WriteStr('R:' + Int2Digs(fGuruStrings[I].Ref, 2));
    CurrFile.WriteStr(#$09);
    CurrFile.WriteStr('L:' + LinkType2Str(fGuruStrings[I].LinkType));    
    CurrFile.WriteStr(#$09);    
{$ENDIF}    
    CurrFile.WriteStr(fGuruStrings[I].RecLine);
    CurrFile.WriteStr(#$0D#$0A);

    SchemeFile.WriteStr(Int2Hex(fGuruStrings[I].Offset, 8) + ',' + Int2Hex(fGuruStrings[I].Length, 8) + ',' + Int2Hex(I, 8));
    SchemeFile.WriteStr(AnsiString(',') + LinkType2Char(fGuruStrings[I].LinkType));
    SchemeFile.WriteStr(#$0D#$0A);
  end;

  if fSchemeOption.NiceSwitch then
  begin
    GoodFilename := PreFilename + '_nice' + ExtractFileExt(fSchemeOption.PlainPath);
    FuzzyFilename := PreFilename + '_fuzzy' + ExtractFileExt(fSchemeOption.PlainPath);
    FakeFilename := PreFilename + '_fake' + ExtractFileExt(fSchemeOption.PlainPath);
    CurrFile := NewWriteFileStream(GoodFilename);
    GoodFile.Position := 0;
    Stream2Stream(CurrFile, GoodFile, GoodFile.Size);
    Free_And_Nil(CurrFile);

    CurrFile := NewWriteFileStream(FuzzyFilename);
    FuzzyFile.Position := 0;
    Stream2Stream(CurrFile, FuzzyFile, FuzzyFile.Size);
    Free_And_Nil(CurrFile);

    CurrFile := NewWriteFileStream(FakeFilename);
    FakeFile.Position := 0;
    Stream2Stream(CurrFile, FakeFile, FakeFile.Size);
    Free_And_Nil(CurrFile);
  end
  else
  begin
    CurrFile := NewWriteFileStream(fSchemeOption.PlainPath);
    GoodFile.Position := 0;
    CurrFile.WriteStr('// nice strings'#$0D#$0A);
    Stream2Stream(CurrFile, GoodFile, GoodFile.Size);
    CurrFile.WriteStr('// fuzzy strings'#$0D#$0A);
    Stream2Stream(CurrFile, FuzzyFile, FuzzyFile.Size);
    CurrFile.WriteStr('// fake strings'#$0D#$0A);
    Stream2Stream(CurrFile, FakeFile, FakeFile.Size);
    Free_And_Nil(CurrFile);
  end;

  Free_And_Nil(GoodFile);
  Free_And_Nil(FuzzyFile);
  Free_And_Nil(FakeFile);
  Free_And_Nil(SchemeFile);

  Result := True;
end;

constructor TProjectWriter.Create;
begin
  fGuruCount := 0;
end;

destructor TProjectWriter.Destroy;
begin
  SetLength(fGuruStrings, 0);
end;

function MergeSchemeFromIDs(SchemeList, CleanList: PStrList): Boolean;
var
  J: Integer;
  CleanIDs: PList;
  IDStr: AnsiString;
  TabPos: Integer;
begin
  Result := False;
  // Create an temp project writer?
  CleanIDs := NewList();

  for J := 0 to CleanList.Count - 1 do
  begin
    TabPos :=  IndexOfChar(CleanList.Items[J], #$09);
    if TabPos > 0 then
    begin
      IDStr := Copy(CleanList.Items[J], 1, TabPos - 1);
       CleanIDs.Add(Pointer(Str2Int(IDStr)));
    end;
  end;

  J := 6; // SchemeSettings
  while J <= SchemeList.Count - 1 do
  begin
    IDStr := Copy(SchemeList.Items[J], 19, 8);
    if CleanIDs.IndexOf(Pointer(Hex2Int(IDStr))) = -1 then
    begin
      SchemeList.Delete(J);
    end
    else
      Inc(J);
  end;
  if CleanIDs.Count > 0 then Result := True;
  Free_And_Nil(CleanIDs);
end;

function InitBinarySourceFromSource(var Item: tagBinarySourceRecItem; Filename: KOLString): Boolean;
var
  PreFilename: KOLString;
  PEFilter: PPEFilter;
begin
  //Result := False;
  Item.InputPath := Filename;
  Item.DisplayName := ExtractFileName(Filename);
  Item.SchemePath := ReplaceFileExt(Filename, '.scheme');
  PreFilename := ExtractFilePath(Filename) + ExtractFileNameWOext(Filename);
  Item.PlainPath := PreFilename + '_str.txt';
  Item.LocalizedPath := PreFilename + '_str_localized.txt'; //<body>_localized.<ext>
  {$IFDEF DEBUG}
  Item.WriteBackPath := PreFilename + '_new' + ExtractFileExt(Filename);
  {$ELSE}
  Item.WriteBackPath := ExtractFilePath(ExcludeTrailingPathDelimiter(ExtractFilePath(Filename))) + ExtractFileName(Filename);
  {$ENDIF}
  Item.NiceSwitch := True;
  Item.DetectedAsPE := False;

  PEFilter := NewPEFilterEx(Filename);
  Item.DetectedAsPE := Assigned(PEFilter);
  Free_And_Nil(PEFilter);

  Item.RipEng := False;
  Item.RipHan := True;
  Item.RipMix := True;

  Item.LimEng := 8;
  Item.LimHan := 4;
  Item.LimMix := 6;

  Item.RipMode := rmGB2312;
  Item.ExfMode := emGB2312;

  Item.noWholeBuff := False;
  Item.Checked := True;

  Result := True;
end;

function InitBinarySourceFromScheme(var Item: tagBinarySourceRecItem; Filename: KOLString): Boolean;
var
  SchemeList: PStrList;
begin
  Result := False;
  SchemeList := NewStrList(); // TODO: change to use stream
  SchemeList.LoadFromFile(Filename);
  if SchemeList.Count > 6 then
  begin
    InitBinarySourceFromSource(Item, SchemeList.Items[1]); // TODO: refactory!

    Item.SchemePath    := Filename;
    Item.DisplayName   := SchemeList.Items[0];
    //Item.InputPath   := SchemeList.Items[1];
    Item.PlainPath     := SchemeList.Items[2];
    Item.LocalizedPath := SchemeList.Items[3];
    Item.WriteBackPath := SchemeList.Items[4];
    Item.NiceSwitch    := Boolean(Str2Int(SchemeList.Items[5][1]));
    Item.RipMode       := TRipMode(Str2Int(SchemeList.Items[5][2]));
    Item.ExfMode       := TExfMode(Str2Int(SchemeList.Items[5][3]));

    Result := True;
  end;
  Free_And_Nil(SchemeList);
end;

function LinkType2Str(LinkType: TLiteralType): AnsiString;
begin
  case LinkType of
    ltLiteral: Result := 'Literal'; // C
    ltPascal:  Result := 'Pascal';  // A
    ltShort:   Result := 'Short';   // S
    ltLen:     Result := 'Length';  // L
  end;
end;

function LinkType2Char(LinkType: TLiteralType): AnsiChar;
begin
  Result := #$00;
  case LinkType of
    ltLiteral: Result := 'C';
    ltPascal:  Result := 'A';
    ltShort:   Result := 'S';
    ltLen:     Result := 'L';
    ltUnicodeLiteral : Result := 'U';
    ltWidePascal     : Result := 'W';
    ltShortUnicode   : Result := 'T';
    ltUnicodeLen  : Result := 'H';
  end;
end;

function Char2LinkType(Char: AnsiChar): TLiteralType;
begin
  Result := TLiteralType(-1);
  case Char of
    'C': Result := ltLiteral;  
    'A': Result := ltPascal;  
    'S': Result := ltShort;
    'L': Result := ltLen;  
    'U': Result := ltUnicodeLiteral;
    'W': Result := ltWidePascal;
    'T': Result := ltShortUnicode;
    'H': Result := ltUnicodeLen;  
  end;
end;

end.
