library LibMUIMaker;

{ I havn't use PacsalString for C++ so we don't need BorlandMM. }

uses
  LibMUIMakerUnt in 'LibMUIMakerUnt.pas',
  MUIMaker in 'MUIMaker.pas',
  PEReader in '..\Utils\PEReader.pas',
  PEAddon in '..\Utils\PEAddon.pas',
  PEDirector in '..\Utils\PEDirector.pas',
  LibKOLUnt in 'LibKOLUnt.pas';

{$R *.res}

exports
  LibMUIMakerUnt.NewMUIMaker,
  LibMUIMakerUnt.NewMUIMakerEx,
  LibMUIMakerUnt.FreeMUIMaker,
  LibMUIMakerUnt.MakerDoRegisterString,
  LibMUIMakerUnt.MakerDoInitString,
  LibMUIMakerUnt.MakerDoSaveToFile,
  LibMUIMakerUnt.MakerDoSaveToStream,
  LibMUIMakerUnt.UtilNukeFile,

  LibMUIMakerUnt.NewPEDirectorEx,
  LibMUIMakerUnt.FreePEDirector,
  LibMUIMakerUnt.PEDirectorDoResizeSection,
  LibMUIMakerUnt.PEDirectorDoGetLiteralSection,
  LibMUIMakerUnt.PEDirectorDoMakeupLiteralSection,
  LibMUIMakerUnt.PEDirectorDoGetPureRelocSectionIndex,
  LibMUIMakerUnt.PEDirectorDoGetPureResourceSectionIndex,
  LibMUIMakerUnt.PEDirectorDoHardRebaseModule,
  LibMUIMakerUnt.PEDirectorDoForceRemoveSection,
  LibMUIMakerUnt.PEDirectorDoFlushSectionToJackedBuf,
  LibMUIMakerUnt.PEDirectorDoFlushNTHeaderToJackedBuf,
  LibMUIMakerUnt.PEDirectorDoGetJackedBuf,
  LibMUIMakerUnt.PEDirectorDoRVA2Offset,
  LibMUIMakerUnt.PEDirectorDoVA2Offset,
  LibMUIMakerUnt.PEDirectorDoOffset2RVA,
  LibMUIMakerUnt.PEDirectorDoOffset2VA,
  //LibMUIMakerUnt.PEDirectorDoGetNTHeaders, // different in @CopyRecord for VC
  LibMUIMakerUnt.PEDirectorDoGetNTHeadersEx,
  LibMUIMakerUnt.PEDirectorDoGetSectionHeaders,

  LibKOLUnt.NewByteArray,
  LibKOLUnt.FreeByteArray,
  LibKOLUnt.ByteArrayDoResize,
  LibKOLUnt.ByteArrayDoGetSize,
  LibKOLUnt.ByteArrayDoGetData,

  LibKOLUnt.NewMemoryStream,
  LibKOLUnt.FreeMemoryStream,
  LibKOLUnt.MemoryStreamDoGetSize,
  LibKOLUnt.MemoryStreamDoRead,
  LibKOLUnt.MemoryStreamDoWrite,
  LibKOLUnt.MemoryStreamDoGetPosition,
  LibKOLUnt.MemoryStreamDoSetPosition;


begin
end.
