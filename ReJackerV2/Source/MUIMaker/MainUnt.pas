{ KOL MCK } // Do not remove this line!
{$DEFINE KOL_MCK}
{$ifdef FPC} {$mode delphi} {$endif}
unit MainUnt;

interface

uses Windows, Messages, KOL {place your units here->}
{$IFDEF LAZIDE_MCK}, Forms, mirror, Classes, Controls, mckCtrls, mckObjs, Graphics;
{$ELSE} , AddonFuncUnt; {$ENDIF}

type

  { TMainFrm }

  {$I MCKfakeClasses.inc}
  {$IFDEF KOLCLASSES} TMainFrm = class; PMainFrm = TMainFrm; {$ELSE OBJECTS} PMainFrm = ^TMainFrm; {$ENDIF CLASSES/OBJECTS}
  TMainFrm = {$IFDEF KOLCLASSES}class{$ELSE}object{$ENDIF}({$IFDEF LAZIDE_MCK}TForm{$ELSE}TObj{$ENDIF})
    Button1: TKOLButton;
    Button2: TKOLButton;
    Button3: TKOLButton;
    Form: PControl;
    KOLForm1: TKOLForm;
    KOLProject1: TKOLProject;
    procedure Button1Click(Sender: PObj);
    procedure Button2Click(Sender: PObj);
    procedure Button3Click(Sender: PObj);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  MainFrm {$IFDEF KOL_MCK} : PMainFrm {$ELSE} : TMainFrm {$ENDIF} ;

{$IFDEF KOL_MCK}
procedure NewMainFrm( var Result: PMainFrm; AParent: PControl );
{$ENDIF}

implementation

{$IFDEF KOL_MCK}
{$I MainUnt_1.inc}
{$ENDIF}

{ TMainFrm }

procedure TMainFrm.Button2Click(Sender: PObj);
begin
  Applet.Close();
end;

procedure TMainFrm.Button3Click(Sender: PObj);
begin
  MzMessageBoxEx(0, 'azsd', 'cccc');
end;

procedure TMainFrm.Button1Click(Sender: PObj);
begin
  ConsoleMain();
end;

initialization
{$IFNDEF KOL_MCK} {$I mainunt.lrs} {$ENDIF}

end.


