{ KOL MCK } // Do not remove this line!
{$DEFINE KOL_MCK}
{$ifdef FPC} {$mode delphi} {$endif}
program MUIMakerGUI;

uses
  KOL,
  MainUnt, PEAddon, PEDirector, PEReader, AddonFuncUnt;

begin // PROGRAM START HERE -- Please do not remove this comment

{$IFNDEF LAZIDE_MCK} {$I MUIMakerGUI_0.inc} {$ELSE}

  Application.Initialize;
  Application.CreateForm(TMainFrm, MainFrm);
  Application.Run;

{$ENDIF}

end.
