////////////////////////////////////////////////////////////////////////////////
// file: LibMUIMakerUnt.pas
// thiscall to stdcall convertor
// Offer flat stdcall to MUIMaker/PEDirector for C++ Application
////////////////////////////////////////////////////////////////////////////////

unit LibMUIMakerUnt;

{$H+}

interface

uses
  Windows, KOL, LibKOLUnt, MUIMaker, PEDirector;

function NewMUIMaker(): PMUIMaker; stdcall;
function NewMUIMakerEx(var Buffer; Size: Integer): PMUIMaker; stdcall;
function FreeMUIMaker(MUIMaker: PMUIMaker): LongBool; stdcall;
function MakerDoRegisterString(MUIMaker: PMUIMaker; StringID: Cardinal; Content: PWideChar): LongBool; stdcall;
function MakerDoInitString(MUIMaker: PMUIMaker; Section: PWideChar): LongBool; stdcall;
function MakerDoSaveToFile(MUIMaker: PMUIMaker; Filename: PWideChar): LongBool; stdcall;
function MakerDoSaveToStream(MUIMaker: PMUIMaker; Stream: PStream): LongBool; stdcall;
function UtilNukeFile(Filename: PWideChar; MayuseConsole: PBOOL): LongBool; stdcall;

function NewPEDirectorEx(var Buffer; Size: Integer): PPEDirectorObj; stdcall;
function FreePEDirector(PEDirector: PPEDirectorObj): LongBool; stdcall;
function PEDirectorDoResizeSection(PEDirector: PPEDirectorObj; Index: Integer; newRawSize, newVirtualSize: Cardinal; AutoFix: LongBool = False): LongBool; stdcall;
function PEDirectorDoGetLiteralSection(PEDirector: PPEDirectorObj): Integer; stdcall;
function PEDirectorDoMakeupLiteralSection(PEDirector: PPEDirectorObj; Name: PAnsiChar): LongBool; stdcall;
function PEDirectorDoGetPureRelocSectionIndex(PEDirector: PPEDirectorObj; var Dropable: LongBool): Integer; stdcall;
function PEDirectorDoGetPureResourceSectionIndex(PEDirector: PPEDirectorObj): Integer; stdcall;
function PEDirectorDoHardRebaseModule(PEDirector: PPEDirectorObj; NewBase: Cardinal): LongBool; stdcall;
function PEDirectorDoForceRemoveSection(PEDirector: PPEDirectorObj; Index: Cardinal): LongBool; stdcall;
function PEDirectorDoFlushSectionToJackedBuf(PEDirector: PPEDirectorObj): LongBool; stdcall;
function PEDirectorDoFlushNTHeaderToJackedBuf(PEDirector: PPEDirectorObj): LongBool; stdcall;
function PEDirectorDoRVA2Offset(PEDirector: PPEDirectorObj; Address: Cardinal): Cardinal; stdcall;
function PEDirectorDoVA2Offset(PEDirector: PPEDirectorObj; Address: Cardinal): Cardinal; stdcall;
function PEDirectorDoOffset2RVA(PEDirector: PPEDirectorObj; Offset: Cardinal): Cardinal; stdcall;
function PEDirectorDoOffset2VA(PEDirector: PPEDirectorObj; Offset: Cardinal): Cardinal; stdcall;
//function PEDirectorDoGetNTHeaders(PEDirector: PPEDirectorObj): IMAGE_NT_HEADERS; stdcall;
function PEDirectorDoGetNTHeadersEx(PEDirector: PPEDirectorObj; var NTHeaders: IMAGE_NT_HEADERS): LongBool; stdcall;
function PEDirectorDoGetSectionHeaders(PEDirector: PPEDirectorObj): PImageSectionHeader; stdcall;
function PEDirectorDoGetJackedBuf(PEDirector: PPEDirectorObj): PByteArray; stdcall;

implementation

function NewMUIMaker(): PMUIMaker;
begin
  Result := MUIMaker.NewMUIMaker();
end;

function NewMUIMakerEx(var Buffer; Size: Integer): PMUIMaker;
begin
  Result := MUIMaker.NewMUIMakerEx(Buffer, Size);
end;

function FreeMUIMaker(MUIMaker: PMUIMaker): LongBool;
begin
  Result := Assigned(MUIMaker);

  if Result then
    Free_And_Nil(MUIMaker);
end;

function MakerDoRegisterString(MUIMaker: PMUIMaker; StringID: Cardinal; Content: PWideChar): LongBool;
begin
  Result := Assigned(MUIMaker);
  if Result then
    Result := MUIMaker.RegisterString(StringID, Content);
end;

function MakerDoInitString(MUIMaker: PMUIMaker; Section: PWideChar): LongBool;
begin
  Result := Assigned(MUIMaker);
  if Result then
    Result := MUIMaker.InitString(Section);
end;

function MakerDoSaveToFile(MUIMaker: PMUIMaker; Filename: PWideChar): LongBool;
begin
  Result := Assigned(MUIMaker);
  if Result then
  begin
    Result := MUIMaker.SaveToFile(Filename);
  end;
end;

function MakerDoSaveToStream(MUIMaker: PMUIMaker; Stream: PStream): LongBool;
begin
  Result := Assigned(MUIMaker);
  if Result then
  begin
    Result := MUIMaker.SaveToStream(Stream);
  end;
end;

function UtilNukeFile(Filename: PWideChar; MayuseConsole: PBOOL): LongBool;
var
  MayuseConsole2: Boolean;
begin
  Result := TryReplaceExistsFile('', Filename, MayuseConsole2);
  if MayuseConsole <> nil then
    MayuseConsole^ := MayuseConsole2;
end;


function NewPEDirectorEx(var Buffer; Size: Integer): PPEDirectorObj;
begin
  // TODO: Dangerous, only calling convertions different from PEDirector one
  New(Result, Create);
  with Result^ do
  begin
    // keep buffer
    if LoadFromBuffer(Buffer, Size, False) = false then
    begin
      Free_And_Nil(Result); // for MPack
    end;
  end;
end;

function FreePEDirector(PEDirector: PPEDirectorObj): LongBool;
begin
  Result := Assigned(PEDirector);

  if Result then
    Free_And_Nil(PEDirector);
end;

function PEDirectorDoResizeSection(PEDirector: PPEDirectorObj; Index: Integer; newRawSize, newVirtualSize: Cardinal; AutoFix: LongBool = False): LongBool;
begin
  Result := Assigned(PEDirector);
  if Result then
    Result := PEDirector.ResizeSection(Index, newRawSize, newVirtualSize, AutoFix);
end;

function PEDirectorDoGetLiteralSection(PEDirector: PPEDirectorObj): Integer;
begin
  Result := -1;
  if Assigned(PEDirector) then
    Result := PEDirector.GetLiteralSection();
end;

function PEDirectorDoMakeupLiteralSection(PEDirector: PPEDirectorObj; Name: PAnsiChar): LongBool;
begin
  Result := Assigned(PEDirector);
  if Result then
    Result := PEDirector.MakeupLiteralSection(Name);
end;

function PEDirectorDoGetPureRelocSectionIndex(PEDirector: PPEDirectorObj; var Dropable: LongBool): Integer;
var
  BoolDropable: Boolean;
begin
  Result := -1;
  if Assigned(PEDirector) then
  begin
    Result := PEDirector.GetPureRelocSectionIndex(BoolDropable);
    Dropable := BoolDropable;
  end;
end;

function PEDirectorDoGetPureResourceSectionIndex(PEDirector: PPEDirectorObj): Integer;
begin
  Result := -1;
  if Assigned(PEDirector) then
  begin
    Result := PEDirector.GetPureResourceSectionIndex();
  end;
end;

function PEDirectorDoHardRebaseModule(PEDirector: PPEDirectorObj; NewBase: Cardinal): LongBool;
begin
  Result := Assigned(PEDirector);
  if Result then
    Result := PEDirector.HardRebaseModule(NewBase);
end;

function PEDirectorDoForceRemoveSection(PEDirector: PPEDirectorObj; Index: Cardinal): LongBool;
begin
  Result := Assigned(PEDirector);
  if Result then
    Result := PEDirector.ForceRemoveSection(Index);
end;

function PEDirectorDoFlushSectionToJackedBuf(PEDirector: PPEDirectorObj): LongBool;
begin
  Result := Assigned(PEDirector);
  if Result then
    Result := PEDirector.FlushSectionToJackedBuf();
end;

function PEDirectorDoFlushNTHeaderToJackedBuf(PEDirector: PPEDirectorObj): LongBool;
begin
  Result := Assigned(PEDirector);
  if Result then
    Result := PEDirector.FlushNTHeaderToJackedBuf();
end;

function PEDirectorDoRVA2Offset(PEDirector: PPEDirectorObj; Address: Cardinal): Cardinal;
begin
  Result := Cardinal(-1);
  if Assigned(PEDirector) then
    Result := PEDirector.RVA2Offset(Address);
end;

function PEDirectorDoVA2Offset(PEDirector: PPEDirectorObj; Address: Cardinal): Cardinal;
begin
  Result := Cardinal(-1);
  if Assigned(PEDirector) then
    Result := PEDirector.VA2Offset(Address);
end;

function PEDirectorDoOffset2RVA(PEDirector: PPEDirectorObj; Offset: Cardinal): Cardinal;
begin
  Result := Cardinal(-1);
  if Assigned(PEDirector) then
    Result := PEDirector.Offset2RVA(Offset);
end;

function PEDirectorDoOffset2VA(PEDirector: PPEDirectorObj; Offset: Cardinal): Cardinal;
begin
  Result := Cardinal(-1);
  if Assigned(PEDirector) then
    Result := PEDirector.Offset2VA(Offset);
end;

{function PEDirectorDoGetNTHeaders(PEDirector: PPEDirectorObj): IMAGE_NT_HEADERS;
begin
  // Warning.
  if Assigned(PEDirector) then
     Result := PEDirector.NTHeaders; // @CopyRecord
end;}

function PEDirectorDoGetNTHeadersEx(PEDirector: PPEDirectorObj; var NTHeaders: IMAGE_NT_HEADERS): LongBool;
begin
  Result := Assigned(PEDirector);

  if Result then
  begin
    NTHeaders := PEDirector.NTHeaders; // copyrecord?
  end;
end;

function PEDirectorDoGetSectionHeaders(PEDirector: PPEDirectorObj): PImageSectionHeader; stdcall;
begin
  Result := nil;
  if Assigned(PEDirector) then
  begin
    if PEDirector.NTHeaders.FileHeader.NumberOfSections > 0 then
      Result := @PEDirector.SecHeaders[0];
  end;
end;

function PEDirectorDoGetJackedBuf(PEDirector: PPEDirectorObj): PByteArray;
begin
  Result := nil;
  if Assigned(PEDirector) then
  begin
    Result := @PEDirector.JackedBuf;
  end;
end;

end.

