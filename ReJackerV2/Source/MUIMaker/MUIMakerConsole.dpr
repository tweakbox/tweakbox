program MUIMakerConsole;

{$APPTYPE CONSOLE}

uses
  AddonFuncUnt in 'AddonFuncUnt.pas',
  PEDirector in '..\Utils\PEDirector.pas',
  PEAddon in '..\Utils\PEAddon.pas',
  PEReader in '..\Utils\PEReader.pas',
{$IFDEF wince}
  ShellNotifyMsg in 'F:\KOLCE\kol\ShellNotifyMsg.pas',
{$ENDIF}
  MUIMaker in 'MUIMaker.pas';


begin
  ConsoleMain();
end.
