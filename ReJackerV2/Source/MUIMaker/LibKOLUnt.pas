unit LibKOLUnt;

interface

uses
  Windows, KOL;

type
  PByteArray = ^TByteArray;
  TByteArray = Array of Byte;

function NewByteArray(): PByteArray; stdcall;
function FreeByteArray(ByteArray: PByteArray): LongBool; stdcall;
procedure ByteArrayDoResize(ByteArray: PByteArray; NewSize: Integer); stdcall;
function ByteArrayDoGetSize(ByteArray: PByteArray): Integer; stdcall;
function ByteArrayDoGetData(ByteArray: PByteArray): Pointer; stdcall;

function NewMemoryStream(): PStream; stdcall;
function FreeMemoryStream(Stream: PStream): LongBool; stdcall;
function MemoryStreamDoGetSize(Stream: PStream): Int64; stdcall;
function MemoryStreamDoRead(Stream: PStream; var Buffer; Size: DWORD): DWORD; stdcall;
function MemoryStreamDoWrite(Stream: PStream; var Buffer; Size: DWORD): DWORD; stdcall;
function MemoryStreamDoGetPosition(Stream: PStream): DWORD; stdcall;
function MemoryStreamDoSetPosition(Stream: PStream; NewPos: DWORD): DWORD; stdcall;

implementation

function NewByteArray(): PByteArray;
begin
  New(Result);
end;

function FreeByteArray(ByteArray: PByteArray): LongBool;
begin
  Result := Assigned(ByteArray);

  if Result then
    Dispose(ByteArray);
end;

procedure ByteArrayDoResize(ByteArray: PByteArray; NewSize: Integer);
begin
  if Assigned(ByteArray) then
    SetLength(ByteArray^, NewSize);
end;

function ByteArrayDoGetSize(ByteArray: PByteArray): Integer;
begin
  Result := -1;
  if Assigned(ByteArray) then
    Result := Length(ByteArray^);
end;

function ByteArrayDoGetData(ByteArray: PByteArray): Pointer;
begin
  Result := nil;
  if Assigned(ByteArray) then
    Result := Pointer(@ByteArray^[0]);
end;

function NewMemoryStream(): PStream;
begin
  Result := KOL.NewMemoryStream();
end;

function FreeMemoryStream(Stream: PStream): LongBool;
begin
  Result := Assigned(Stream);

  if Result then
    Free_And_Nil(Stream);
end;


function MemoryStreamDoGetSize(Stream: PStream): Int64;
begin
  Result := -1;

  if Assigned(Stream) then
    Result := Stream.Size;
end;

function MemoryStreamDoRead(Stream: PStream; var Buffer; Size: DWORD): DWORD;
begin
  Result := 0;

  if Assigned(Stream) then
    Result := Stream.Read(Buffer, Size);
end;

function MemoryStreamDoWrite(Stream: PStream; var Buffer; Size: DWORD): DWORD;
begin
  Result := 0;

  if Assigned(Stream) then
    Result := Stream.Write(Buffer, Size);
end;

function MemoryStreamDoGetPosition(Stream: PStream): DWORD;
begin
  Result := Cardinal(-1);

  if Assigned(Stream) then
    Result := Stream.Position;
end;


function MemoryStreamDoSetPosition(Stream: PStream; NewPos: DWORD): DWORD;
begin
  Result := Cardinal(-1);

  if Assigned(Stream) then
  begin
    Stream.Position := Result;
    Result := Stream.Position;
  end;
end;

end.
