library LibMUIMaker;

{$mode objfpc}{$H+}

uses
  LibMUIMakerUnt in 'LibMUIMakerUnt.pas',
  MUIMaker in 'MUIMaker.pas',
  PEReader in '..\Utils\PEReader.pas',
  PEAddon in '..\Utils\PEAddon.pas',
  PEDirector in '..\Utils\PEDirector.pas';

exports
  NewMUIMaker2,
  FreeMUIMaker,
  MakerDoInitString,
  MakerDoRegisterString,
  MakerDoSaveToFile,
  UtilNukeFile;

begin
end.

