﻿unit MUIMaker;

interface

uses
  Windows, KOL, PEDirector, PEAddon{$IFDEF wince}, MzCommonDlg{$ENDIF};

type
  tagStringGroup = packed record
    TableID    : Integer;
    GroupSize  : Integer;
    Strings    : Array[0..15] of WideString;
  end;
  GroupArray = Array of tagStringGroup;
  ByteArray = Array of Byte;
  tagRCDataRecItem = packed record
    IsIDEntry  : Boolean;
    RecID      : Integer;
    RecName    : WideString;
    RawBuffer  : Array of Byte;
  end;

type
  PMUIMaker = ^TMUIMaker;
  TMUIMaker = Object(TObj)
  private
    fPEDirector: PPEDirectorObj;
    fStringTables: GroupArray;
    fRsrcBlock: ByteArray;
  public
    constructor Create();
    destructor Destroy(); virtual;
  private
    function GenerateTemplate(): Boolean;
    function MakeupResourceBlock(): Boolean;
  public
    function RegisterString(StringID: Integer; Content: WideString): Boolean;
    function InitString(Section: WideString): Boolean;
    function RegisterRCData(ResourceID: Integer; Filename: WideString): Boolean; overload;
    function RegisterRCData(ResourceName: WideString; Filename: WideString): Boolean; overload;
    function SaveToFile(Filename: WideString): Boolean;
    function SaveToStream(Stream: PStream): Boolean;
  end;
  // TODO: Inherite from PEDirector and implement MakeupResourceSection

function NewMUIMaker(): PMUIMaker;
function NewMUIMakerEx(var Buffer; Size: Integer): PMUIMaker;
function TryReplaceExistsFile(SourceFile, DestionFile: WideString; var MayuseConsole: Boolean): Boolean;

implementation

function NewMUIMaker(): PMUIMaker;
begin
  New(Result, Create);
  with Result^ do
  begin
    GenerateTemplate();
  end;

end;

function NewMUIMakerEx(var Buffer; Size: Integer): PMUIMaker;
var
  LocalBuffer: Array of Byte;
begin
  New(Result, Create);

  with Result^ do
  begin
    SetLength(LocalBuffer, Size);
    Move(Buffer, LocalBuffer[0], Size);
    fPEDirector := NewPEDirectorEx(LocalBuffer);
  end;
end;


function TMUIMaker.GenerateTemplate(): Boolean;
var
  Template: ByteArray;
begin
  //ShowMessage('Start GenerateTemplate');
  SetLength(Template, $200);
  FillChar(Template[0], Length(Template), 0);
  PImageDosHeader(@Template[0]).e_magic := $5A4D; //'MZ'
  PImageDosHeader(@Template[0]).e_cblp := 144;    // Bytes on last page of file
  PImageDosHeader(@Template[0]).e_cp := 3;        // Pages in file
  PImageDosHeader(@Template[0]).e_cparhdr := 4;   // Size of header in paragraphs
  PImageDosHeader(@Template[0]).{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF} := $C;    // File address of new exe header

  PImageNtHeaders(@Template[$C]).Signature := $4550; //'PE'
  PImageNtHeaders(@Template[$C]).FileHeader.Machine := 450; //IMAGE_FILE_MACHINE_THUMB;
  PImageNtHeaders(@Template[$C]).FileHeader.NumberOfSections := 1;
  //Cent := Str2DateTimeFmt('yyyy/MM/dd', '1970/01/01');
  PImageNtHeaders(@Template[$C]).FileHeader.TimeDateStamp := Trunc((Now() - VCLDate0 - 25569) * 86400.0);
  PImageNtHeaders(@Template[$C]).FileHeader.SizeOfOptionalHeader := $E0;
  PImageNtHeaders(@Template[$C]).FileHeader.Characteristics := IMAGE_FILE_EXECUTABLE_IMAGE or IMAGE_FILE_LARGE_ADDRESS_AWARE or IMAGE_FILE_32BIT_MACHINE or IMAGE_FILE_DLL or IMAGE_FILE_DEBUG_STRIPPED or IMAGE_FILE_RELOCS_STRIPPED;
  PImageNtHeaders(@Template[$C]).OptionalHeader.Magic := $10B;
  PImageNtHeaders(@Template[$C]).OptionalHeader.MajorLinkerVersion := 37;
  PImageNtHeaders(@Template[$C]).OptionalHeader.MinorLinkerVersion := 10;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfInitializedData := $200;
  PImageNtHeaders(@Template[$C]).OptionalHeader.BaseOfCode := $1000;
  //PImageNtHeaders(@Template[$C]).OptionalHeader.BaseOfData := $1000; // keep out
  PImageNtHeaders(@Template[$C]).OptionalHeader.ImageBase := $10000;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SectionAlignment := $1000;
  PImageNtHeaders(@Template[$C]).OptionalHeader.FileAlignment := $200;
  PImageNtHeaders(@Template[$C]).OptionalHeader.MajorOperatingSystemVersion := 4;
  PImageNtHeaders(@Template[$C]).OptionalHeader.MajorSubsystemVersion := 4;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfImage := $1000; // $107E
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfHeaders := $200;
  PImageNtHeaders(@Template[$C]).OptionalHeader.Subsystem := 9; //IMAGE_SUBSYSTEM_WINDOWS_CE_GUI;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfStackReserve := $100000;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfStackCommit := $1000;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfHeapReserve := $100000;
  PImageNtHeaders(@Template[$C]).OptionalHeader.SizeOfHeapCommit := $1000;
  PImageNtHeaders(@Template[$C]).OptionalHeader.NumberOfRvaAndSizes := 16;

  PImageSectionHeader(@Template[$104]).Misc.VirtualSize := 0;
  PImageSectionHeader(@Template[$104]).VirtualAddress := $1000;
  PImageSectionHeader(@Template[$104]).SizeOfRawData := 0;
  PImageSectionHeader(@Template[$104]).PointerToRawData := $200;
  PImageSectionHeader(@Template[$104]).Characteristics := IMAGE_SCN_MEM_READ or IMAGE_SCN_CNT_INITIALIZED_DATA;
  Move({$IFDEF FPC}'.rsrc'{$ELSE}AnsiString('.rsrc'){$ENDIF}, PImageSectionHeader(@Template[$104]).Name[0], Length('.rsrc'));

  fPEDirector := NewPEDirectorEx(Template);
  fPEDirector.NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress := $1000;
  fPEDirector.NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].Size := 0;

  SetLength(Template, 0);

  //ShowMessage('Stop GenerateTemplate');
end;

function TMUIMaker.SaveToFile(Filename: WideString): Boolean;
var
  MUIFile: PStream;
begin
  //ShowMessage('Start SaveToFile');
  MUIFile := NewWriteFileStream(Filename);
  Result := SaveToStream(MUIFile);
  Free_And_Nil(MUIFile);
  //TryReplaceExistsFile(Filename + '.new', Filename, MayuseConsole);
end;

function TMUIMaker.SaveToStream(Stream: PStream): Boolean;
begin
  MakeupResourceBlock();

  fPEDirector.SecHeaders[0].Misc.VirtualSize := Length(fRsrcBlock);
  fPEDirector.SecHeaders[0].SizeOfRawData := Length(fRsrcBlock);

  fPEDirector.NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].Size := Length(fRsrcBlock); // TopRaw

  fPEDirector.NTHeaders.OptionalHeader.SizeOfImage := fPEDirector.NTHeaders.OptionalHeader.SizeOfImage + Length(fRsrcBlock);

  fPEDirector.FlushSectionToJackedBuf;
  fPEDirector.FlushNTHeaderToJackedBuf;
  Stream.Write(fPEDirector.JackedBuf[0], Length(fPEDirector.JackedBuf));
  Stream.Write(fRsrcBlock[0], Length(fRsrcBlock));
  //ShowMessage('Saved '+Int2Str(Length(fPEDirector.JackedBuf) + Length(fRsrcBlock))+' Bytes');
  Result := True;
end;

function TMUIMaker.RegisterString(StringID: Integer; Content: WideString): Boolean;
var
  I: Integer;
  TableID, StringIndex: Integer;
begin
  Result := False;
  TableID := StringID div 16 + 1;
  StringIndex := StringID mod 16;
  for I := 0 to Length(fStringTables) - 1 do
  begin
    if TableID = fStringTables[I].TableID then
    begin
      Result := True;
      DEC(fStringTables[I].GroupSize, Length(fStringTables[I].Strings[StringIndex]) * 2);
      fStringTables[I].Strings[StringIndex] := Content;
      INC(fStringTables[I].GroupSize, Length(fStringTables[I].Strings[StringIndex]) * 2);
    end;
  end;
  if Result = False then
  begin
    if Length(Content) = 0 then Exit;
    
    I := Length(fStringTables);
    SetLength(fStringTables, I + 1);
    fStringTables[I].TableID := TableID;
    fStringTables[I].Strings[StringIndex] := Content;
    fStringTables[I].GroupSize := Length(fStringTables[I].Strings) * SizeOf(WORD) + Length(fStringTables[I].Strings[StringIndex]) * 2;
    Result := True;
  end;
end;

function TMUIMaker.InitString(Section: WideString): Boolean;
var
  IniFile: PIniFile;
  Names: PWStrList;
  I, tdivpos, ID: Integer;
  Content: WideString;
begin
  IniFile := OpenIniFile(GetStartDir() + 'LibMUIMaker.ini');
  //IniFile.Mode := ifmRead;
  IniFile.Section := Section;

  Names := NewWStrList();
  IniFile.SectionData(Names {$IFDEF wince}, True{$ENDIF});
  Names.SaveToFile(GetStartDir + 'Names.txt');

  for I := 0 to Names.Count - 1 do
  begin
    tdivpos:=Pos('=',Names.Items[I]);
    ID := Str2Int(Copy(Names.Items[I], 1, tdivpos-1));
    Content := Copy(Names.Items[I],tdivpos+1,Length(Names.Items[I])-tdivpos);
    RegisterString(ID, Content);
  end;
  Result := Names.Count > 0;
  Free_And_Nil(Names);
  Free_And_Nil(IniFile);
end;

function TMUIMaker.RegisterRCData(ResourceID: Integer; Filename: WideString): Boolean;
begin
  Result := False;
end;

function TMUIMaker.RegisterRCData(ResourceName: WideString; Filename: WideString): Boolean;
begin
  Result := False;
end;


function TMUIMaker.MakeupResourceBlock(): Boolean;
var
  CalcSize, spos, tpos, RawTop, CostedRaw, TopVA: Integer;
  I, J, K: Integer;
  GroupIDs, GroupIndexs: Array of Integer;
begin
  Result := False;

  //SortIntegerArray();
  SetLength(GroupIDs, Length(fStringTables));

  CalcSize := 0;
  for I := 0 to Length(fStringTables) - 1 do
  begin
    GroupIDs[I] := fStringTables[I].TableID;
    INC(CalcSize, fStringTables[I].GroupSize);
    INC(CalcSize, SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY));  // GROUP ID ENTRY2
    INC(CalcSize, SizeOf(IMAGE_RESOURCE_DIRECTORY));        // LANG DIR3
    INC(CalcSize, SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY));  // LANG ENTRY3
    INC(CalcSize, SizeOf(IMAGE_RESOURCE_DATA_ENTRY));       // DATA ENTRY
  end;
  INC(CalcSize, SizeOf(IMAGE_RESOURCE_DIRECTORY));          // Top DIR1
  INC(CalcSize, SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY));    // TYPES ENTRY1
  INC(CalcSize, SizeOf(IMAGE_RESOURCE_DIRECTORY));          // IDS DIR2

  SortIntegerArray(GroupIDs);
  SetLength(GroupIndexs, Length(GroupIDs));

  for I := 0 to Length(fStringTables) - 1 do
  begin
    for J := 0 to Length(GroupIDs) - 1 do
    begin
      if fStringTables[I].TableID = GroupIDs[J] then
      begin
        GroupIndexs[J] := I;
        Break;
      end;
    end;
  end;
  // Swap
  SetLength(GroupIDs, 0);

  SetLength(fRsrcBlock, CalcSize);
  FillChar(fRsrcBlock[0], Length(fRsrcBlock), 0);

  spos := 0;
  PImageResourceDirectory(@fRsrcBlock[spos]).NumberOfIdEntries := 1; // Only String Tables now
  spos := spos + sizeof(IMAGE_RESOURCE_DIRECTORY);
  PImageResourceDirectoryEntry(@fRsrcBlock[spos]).Name := Cardinal(RT_STRING); // err, case?
  PImageResourceDirectoryEntry(@fRsrcBlock[spos]).OffsetToData := (spos + SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY)) or $80000000;
  spos := spos + sizeof(IMAGE_RESOURCE_DIRECTORY_ENTRY);

  // Wait for sorting
  PImageResourceDirectory(@fRsrcBlock[spos]).NumberOfIdEntries := Length(fStringTables); // some group
  spos := spos + sizeof(IMAGE_RESOURCE_DIRECTORY);
  CostedRaw := 0;
  RawTop := Length(fStringTables) * (SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY) + SizeOf(IMAGE_RESOURCE_DIRECTORY) + SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY) + SizeOf(IMAGE_RESOURCE_DATA_ENTRY)) // Group cost
          + SizeOf(IMAGE_RESOURCE_DIRECTORY) + SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY) + SizeOf(IMAGE_RESOURCE_DIRECTORY);
  TopVA := fPEDirector.NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress;//fPEDirector.SecHeaders[0].VirtualAddress;
  for K := 0 to Length(GroupIndexs) - 1 do // 39, 40, 41
  begin
    I := GroupIndexs[K];
    PImageResourceDirectoryEntry(@fRsrcBlock[spos]).Name := fStringTables[I].TableID;
    PImageResourceDirectoryEntry(@fRsrcBlock[spos]).OffsetToData := (spos + (Length(fStringTables) - K) * SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY) + K * (SizeOf(IMAGE_RESOURCE_DIRECTORY) + SizeOf(IMAGE_RESOURCE_DIRECTORY_ENTRY))) or $80000000; // LANG

    // LANG Layer
    tpos := PImageResourceDirectoryEntry(@fRsrcBlock[spos]).OffsetToData and $7FFFFFFF;
    PImageResourceDirectory(@fRsrcBlock[tpos]).NumberOfIdEntries := 1; // Only Native
    tpos := tpos + SizeOf(IMAGE_RESOURCE_DIRECTORY);
    PImageResourceDirectoryEntry(@fRsrcBlock[tpos]).Name := 0; // LANG ID, Native
    PImageResourceDirectoryEntry(@fRsrcBlock[tpos]).OffsetToData := RawTop - (Length(fStringTables) - K) * SizeOf(IMAGE_RESOURCE_DATA_ENTRY); // -Groupsize, +I, + Pre

    // DATA ENTRY
    tpos := PImageResourceDirectoryEntry(@fRsrcBlock[tpos]).OffsetToData and $7FFFFFFF; // no neccessary
    PImageResourceDataEntry(@fRsrcBlock[tpos]).Size := fStringTables[I].GroupSize;
    PImageResourceDataEntry(@fRsrcBlock[tpos]).OffsetToData := TopVA + RawTop + CostedRaw; // fake

    // DATA!
    tpos := PImageResourceDataEntry(@fRsrcBlock[tpos]).OffsetToData - TopVA;
    for J := 0 to Length(fStringTables[I].Strings) - 1 do
    begin
      PWORD(@fRsrcBlock[tpos])^ := Length(fStringTables[I].Strings[J]);
      //tpos := tpos + SizeOf(WORD);
      if PWORD(@fRsrcBlock[tpos])^ > 0 then
      begin
        Move(fStringTables[I].Strings[J][1], fRsrcBlock[tpos + SizeOf(WORD)], PWORD(@fRsrcBlock[tpos])^ * 2); // Dangerous
        tpos := tpos + SizeOf(WORD) + PWORD(@fRsrcBlock[tpos])^ * 2;
      end
      else
        tpos := tpos + SizeOf(WORD);
    end;
    CostedRaw := CostedRaw + fStringTables[I].GroupSize;
    spos := spos + sizeof(IMAGE_RESOURCE_DIRECTORY_ENTRY);
  end;
end;

constructor TMUIMaker.Create;
begin
  inherited;

  fPEDirector := nil;
end;

destructor TMUIMaker.Destroy;
begin
  if Assigned(fPEDirector) then
    Free_And_Nil(fPEDirector);

  SetLength(fStringTables, 0);
  SetLength(fRsrcBlock, 0);

  inherited;
end;

function AutoRename(OrgFname: KOLString): KOLString;
var
  CurSuffix: Integer;
  OrgPath, OrgPrefix, OrgExt: KOLString;
begin
  OrgPath := IncludeTrailingPathDelimiter(ExtractFilePath(OrgFname)); // [c:\path\]
  OrgPreFix := ExtractFileNameWOext(OrgFname); // [name]
  OrgExt  := ExtractFileExt(OrgFname); // [.ext]
  CurSuffix := 1;
  while FileExists(OrgPath + OrgPreFix + ' (' + Int2Str(CurSuffix) + ')' + OrgExt) do
    INC(CurSuffix);
  Result := OrgPath + OrgPreFix + ' (' + Int2Str(CurSuffix) + ')' + OrgExt;
end;

function TryReplaceExistsFile(SourceFile, DestionFile: WideString; var MayuseConsole: Boolean): Boolean;
var
  NewDestionName, TmpPath: WideString;
{$IFDEF WINCE}
  TmpList: PDirList;
  I: Integer;
  IniFile: PIniFile;
  InitKey{, LaunchKey}: HKEY;
{$ENDIF}
begin
  Result := False;
  // First try to delete destionfile
  Result := not(FileExists(DestionFile));
  if Result = False then
  begin
    Result := DeleteFile(PWideChar(DestionFile)) = True;
  end;
  if (Result) then
  begin
    if FileExists(SourceFile) then
      Result := MoveFile(PWideChar(SourceFile), PWideChar(DestionFile))
    else
      Result := True; // Infact we faked target file. it's enough now
  end
  else
  begin
    // Destion in use
    NewDestionName := AutoRename(DestionFile);
    Result := MoveFile(PWideChar(DestionFile), PWideChar(NewDestionName));
    if Result then
    begin
      if FileExists(SourceFile) then
        Result := MoveFile(PWideChar(SourceFile), PWideChar(DestionFile))
      else
        Result := True; // Infact we renamed target file. it will be faked in next boot
    end;
  end;

{$IFDEF WINCE}
  // Cleanup
  MayUseConsole := False;
  TmpPath := IncludeTrailingPathDelimiter(ExtractFilePath(DestionFile));
  TmpList := NewDirList(TmpPath, ExtractFileNameWOext(DestionFile) + ' *' +  ExtractFileExt(DestionFile), 0);
  IniFile := OpenIniFile(GetStartDir() + 'TweakConsole.ini');
  IniFile.Mode := ifmWrite;
  IniFile.Section := 'Delete';
  for I := 0 to TmpList.Count - 1 do
  begin
    //
    if not(TmpList.IsDirectory[I]) then
    begin
      if not(DeleteFile(PWideChar(TmpList.Names[I]))) then
      begin
        IniFile.ValueString(TmpList.Names[I], TmpPath + TmpList.Names[I]);
        MayUseConsole := True;
      end;
    end;
  end;
  Free_And_Nil(IniFile);
  if MayUseConsole then
  begin
    InitKey := RegKeyOpenWrite(HKEY_LOCAL_MACHINE, 'init');
    //LaunchKey := RegKeyOpenCreate(InitKey, 'Launch70');
    RegKeySetStr(InitKey, 'Launch70', GetStartDir() + 'TweakConsole.exe');
    //Result := False;
    RegCloseKey(InitKey);
  end;
{$ENDIF}
end;

end.
