﻿{ KOL MCK } // Do not remove this line!

procedure NewMainFrm( var Result: PMainFrm; AParent: PControl );
begin

  {$ifndef wince}InitCommonControls;{$endif}
  {$IFDEF KOLCLASSES}
  Result := PMainFrm.Create;
  {$ELSE OBJECTS}
  New( Result, Create );
  {$ENDIF KOL CLASSES/OBJECTS}
  Result.Form := NewForm( AParent, 'MainFrm' ){$ifndef wince}.SetPosition( 290, 157 ){$endif};
  Applet :=  Result.Form;
  Result.Form.Add2AutoFree( Result );
{$IFDEF UNICODE_CTRLS}{$IFNDEF wince}
     Result.Form.SetUnicode(TRUE);
{$ENDIF wince}{$ENDIF UNICODE_CTRLS}
   {$IFDEF USE_NAMES}
    Result.Form.SetName( Applet, 'MainFrm' );
   {$ENDIF}
    {$ifndef wince}Result.Form.SetClientSize( 287, 377 );{$endif}
    Result.Form.Font.FontHeight := -20;
    Result.Form.Font.FontName := 'Arial';
    // Result.Button1.TabOrder = 0
    Result.Button1 := NewButton( Result.Form, 'Generate' ).SetPosition( 23, 27 ).SetSize( 101, 61 );
   {$IFDEF USE_NAMES}
    Result.Button1.SetName( Result.Form, 'Button1' ); 
   {$ENDIF}
   {$IFDEF UNICODE_CTRLS}{$IFNDEF wince}
   Result.Button1.SetUnicode(TRUE);
   {$ENDIF wince}{$ENDIF UNICODE_CTRLS}
    // Result.Button2.TabOrder = 1
    Result.Button2 := NewButton( Result.Form, 'Exit' ).SetPosition( 24, 111 ).SetSize( 101, 61 );
   {$IFDEF USE_NAMES}
    Result.Button2.SetName( Result.Form, 'Button2' ); 
   {$ENDIF}
   {$IFDEF UNICODE_CTRLS}{$IFNDEF wince}
   Result.Button2.SetUnicode(TRUE);
   {$ENDIF wince}{$ENDIF UNICODE_CTRLS}
    // Result.Button3.TabOrder = 2
    Result.Button3 := NewButton( Result.Form, 'Button3' ).SetPosition( 26, 191 ).SetSize( 98, 67 );
   {$IFDEF USE_NAMES}
    Result.Button3.SetName( Result.Form, 'Button3' ); 
   {$ENDIF}
   {$IFDEF UNICODE_CTRLS}{$IFNDEF wince}
   Result.Button3.SetUnicode(TRUE);
   {$ENDIF wince}{$ENDIF UNICODE_CTRLS}
      Result.Button1.OnClick := Result.Button1Click;
      Result.Button2.OnClick := Result.Button2Click;
      Result.Button3.OnClick := Result.Button3Click;

end;

