﻿unit PostLexFiltersUnt;

interface

uses
  Windows, KOL, FvString, IOCentre;

type
  PPostLexFilters = ^TPostLexFilters;
  TPostLexFilters = Object(TObj)
  protected
    fAlanGap: Boolean;
    fGBKDancer: Boolean;
    fDanceMode: TRipMode;
  private
    fGBK21003Table: Array of Word;
    fSJIS6879Table: Array of Single;
  protected
    constructor Create;
    destructor Destroy; virtual;
  private
    function AskAlan(var GuruRecord: GuruStringRecItem): Boolean;
    function DanceGBK(var GuruRecord: GuruStringRecItem): Boolean;
    function DanceSJIS_Asahi(var GuruRecord: GuruStringRecItem): Boolean;
    function DanceSJIS(var GuruRecord: GuruStringRecItem): Boolean;
    function DanceBIG5(var GuruRecord: GuruStringRecItem): Boolean;
    function LoadGBKChartTable(filename: KOLString): Boolean;
    function LoadSJISFreqTable(Filename: KOLString): Boolean;
  public
    function ProcessLine(var GuruRecord: GuruStringRecItem): Boolean;
    property DanceMode: TRipMode write fDanceMode;
  end;

function NewPostLexFilters(): PPostLexFilters;
function GBK2Index(Single: WORD): Integer;
function SJIS2Index(Single: WORD): Integer;
function IsSJISActual(B1, B2:Byte): Boolean;

implementation

function NewPostLexFilters(): PPostLexFilters;
begin
  New(Result, Create);
  Result.LoadGBKChartTable(GetStartDir() + '21003.bin');
  Result.LoadSJISFreqTable(GetStartDir() + 'sjischart.bin');
end;

function TPostLexFilters.AskAlan(var GuruRecord: GuruStringRecItem): Boolean;
begin
  Result := False;

  // pass pure latin or pure kanji
  if GuruRecord.LatinCount * GuruRecord.HanziCount > 0 then
  begin
    if ((GuruRecord.LatinCount > GuruRecord.HanziCount) and (GuruRecord.LatinCount / GuruRecord.HanziCount >= 2)) or
      (((GuruRecord.HanziCount > GuruRecord.LatinCount) and (GuruRecord.HanziCount / GuruRecord.LatinCount >= 2))) then
    begin
      INC(GuruRecord.Ref);
      if GuruRecord.LatinCount >= GuruRecord.HanziCount * 2 then
      begin
        // AAAA中文
        GuruRecord.RP := GuruRecord.RP - 28 * Abs(GuruRecord.HanziCount - GuruRecord.LatinCount) / (GuruRecord.HanziCount * GuruRecord.LatinCount) - 8 * (GuruRecord.SwitchCount / GuruRecord.HanziCount);
        if GuruRecord.SwitchCount / ( GuruRecord.HanziCount +GuruRecord.LatinCount) > 0.4 then
        begin
          GuruRecord.RP := GuruRecord.RP - 40 * (GuruRecord.SwitchCount / ( GuruRecord.HanziCount +GuruRecord.LatinCount) - 0.4);
          INC(GuruRecord.Ref);
        end;
      end{;
      if GuruRecord.HanziCount > GuruRecord.LatinCount then}
      else
      begin
        // 中文:
        GuruRecord.RP := GuruRecord.RP - 8 * (GuruRecord.LatinCount / GuruRecord.HanziCount);
        INC(GuruRecord.Ref);
      end;
    end
    else
    begin
      // A中B文C我D们
      if GuruRecord.SwitchCount / ( GuruRecord.HanziCount + GuruRecord.LatinCount) > 0.4 then
      begin
        GuruRecord.RP := GuruRecord.RP - 25 * 5 * (GuruRecord.SwitchCount / ( GuruRecord.HanziCount + GuruRecord.LatinCount) - 0.3);
        if GuruRecord.SwitchCount > 6 then GuruRecord.RP := GuruRecord.RP - (GuruRecord.SwitchCount - 6) * 0.2 - 4;
        INC(GuruRecord.Ref);
      end;
    end;
  end;

end;

function TPostLexFilters.DanceGBK(var GuruRecord: GuruStringRecItem): Boolean;
var
  Hanzilize, Garbledlize, Delta: Double;
  I: Integer;
  W1: Word;
  Gap2700Gate, All21003Gate: Integer;

begin
  Result := False;

  if GuruRecord.HanziCount > 4 then
    Gap2700Gate := 2312
  else
    Gap2700Gate := 1500;
    
  All21003Gate := 21003;
  
  if GuruRecord.HanziCount > 0 then
  begin
    I := 1;
    Hanzilize := 0;
    Garbledlize := 0;
    while I <= Length(GuruRecord.RecLine) do
    begin
      if Byte(GuruRecord.RecLine[I]) in [$81..$FE] then
      begin
        W1 := PWORD(@GuruRecord.RecLine[I])^;
        W1 := GBK2Index(W1);
        W1 := fGBK21003Table[W1]; // out of = 0
        if (W1 > 0) and (W1 <= Gap2700Gate) then
        begin
          Hanzilize := Hanzilize + Sin(((W1 - 1) / Gap2700Gate) * Pi / 2 +  Pi / 2) * 6; // +
        end;
        if W1 > Gap2700Gate then
        begin
          Garbledlize := Garbledlize + Sin(((W1 - Gap2700Gate) / (All21003Gate - Gap2700Gate)) * Pi / 2 + Pi) * 80; // -
        end;
        INC(I, 2);
      end
      else
      begin
        if Byte(GuruRecord.RecLine[I]) in [$20] then
        begin
          if Garbledlize < 0 then
            Garbledlize := Garbledlize + (GuruRecord.RP / 60)
        end;
        INC(I);
      end;
    end;
    //Delta := Delta / (GuruRecord.HanziCount / 2);
    if Hanzilize >= Abs(Garbledlize) then
    begin
      Delta := (Hanzilize + Garbledlize) / (GuruRecord.HanziCount / 3) * (GuruRecord.RP / 60);
    end
    else
    begin
      Delta := (Garbledlize + Hanzilize / 1.5) / (GuruRecord.HanziCount / 2);
    end;

    GuruRecord.RP := GuruRecord.RP + Delta;
    if Abs(Delta) > 0.5 then
      INC(GuruRecord.Ref);
  end;
end;

function TPostLexFilters.DanceSJIS_Asahi(var GuruRecord: GuruStringRecItem): Boolean;
var
  Hanzilize, Garbledlize, Delta: Double;
  I: Integer;
  W1: Word;
  Freq, Gap2700Gate: Single;
  PrevKata: Boolean;
begin
  Result := False;

  if GuruRecord.HanziCount > 4 then
    Gap2700Gate := 0.004     // 1500  巣
  else
    if GuruRecord.SwitchCount > 1 then
      Gap2700Gate := 0.021   // 843   諸
    else
      Gap2700Gate := 0.0145; // 1000  束

  if GuruRecord.HanziCount > 0 then
  begin
    I := 1;
    Hanzilize := 0;
    Garbledlize := 0;
    PrevKata := False;
    while I <= Length(GuruRecord.RecLine) do
    begin
      if Byte(GuruRecord.RecLine[I]) in [$81..$9F, $E0..$EF] then
      begin
        W1 := PWORD(@GuruRecord.RecLine[I])^;
        W1 := SJIS2Index(W1);
        if (W1 >= 376) and (W1 <= 461) then
        begin
          // from ァ to ヶ
          if PrevKata then
          begin
            // 1, 1
            if Garbledlize < 0 then Garbledlize := Garbledlize + 0.8;
            Hanzilize := Hanzilize + 4.5;
          end
          else
          begin
            // 0, 1
            Hanzilize := Hanzilize + 0.8; // chisanobonken
          end;

          PrevKata := True;
          INC(I, 2);
          Continue;
        end
        else
          PrevKata := False;

        Freq := fSJIS6879Table[W1]; // out of = 0
        if (Freq > 0) and (Freq <= Gap2700Gate) then
        begin
          Garbledlize := Garbledlize + (Gap2700Gate - Freq) * -180 + (Gap2700Gate / Freq) * -2.8;
        end;
        if Freq > Gap2700Gate then
        begin
          Hanzilize := Hanzilize + (Freq - Gap2700Gate) * 28 + (Freq / Gap2700Gate) * 0.75;
        end;
        INC(I, 2);
      end
      else
      begin
        if Byte(GuruRecord.RecLine[I]) in [$20] then
        begin
          if Garbledlize < 0 then
            Garbledlize := Garbledlize + (GuruRecord.RP / 60)
        end;
        INC(I);
      end;
    end;
    //Delta := Delta / (GuruRecord.HanziCount / 2);
    if Hanzilize >= Abs(Garbledlize) then
    begin
      Delta := (Hanzilize + Garbledlize) / (GuruRecord.HanziCount / 3) * (GuruRecord.RP / 60);
    end
    else
    begin
      Delta := (Garbledlize + Hanzilize / 1.5) / (GuruRecord.HanziCount / 2);
    end;

    GuruRecord.RP := GuruRecord.RP + Delta;
    if Abs(Delta) > 0.5 then
      INC(GuruRecord.Ref);
  end;
end;

function TPostLexFilters.DanceSJIS(var GuruRecord: GuruStringRecItem): Boolean;
var
  Hanzilize, Garbledlize, Delta: Double;
  I: Integer;
  W1: Word;
  Freq, Gap2700Gate: Single;
  PrevKata, PrevHira, PrevHalfKata: Boolean;
begin
  Result := False;

  if GuruRecord.HanziCount > 4 then
    Gap2700Gate := 0.00004     // 1500  巣
  else
    if GuruRecord.SwitchCount > 1 then
      Gap2700Gate := 0.00038   // 843   諸
    else
      Gap2700Gate := 0.00014; // 1000  束

  if GuruRecord.HanziCount > 0 then
  begin
    I := 1;
    Hanzilize := 0;
    Garbledlize := 0;
    PrevKata := False;
    PrevHira := False;
    PrevHalfKata := False;
    while I <= Length(GuruRecord.RecLine) do
    begin
      if Byte(GuruRecord.RecLine[I]) in [$81..$9F, $E0..$EF] then
      begin
        W1 := PWORD(@GuruRecord.RecLine[I])^;
        W1 := SJIS2Index(W1);
        if (W1 >= 376) and (W1 <= 461) then
        begin
          // from ァ to ヶ
          if PrevKata then
          begin
            // 1, 1
            if Garbledlize < 0 then Garbledlize := Garbledlize + 0.8;
            Hanzilize := Hanzilize + 2.5;
          end
          else
          begin
            // 0, 1
            Hanzilize := Hanzilize + 0.8; // chisanobonken
          end;
          PrevKata := True;
          INC(I, 2);
          Continue;
        end
        else
          PrevKata := False;

        if (W1 >= 282) and (W1 <= 364) then
        begin
          // from ぁ to ん
          if PrevHira then
          begin
            // 1, 1
            if Garbledlize < 0 then Garbledlize := Garbledlize + 0.8;
            Hanzilize := Hanzilize + 1.8;
          end
          else
          begin
            // 0, 1
            Hanzilize := Hanzilize + 0.5; // chisanobonken
          end;
          PrevHira := True;
          INC(I, 2);
          Continue;
        end
        else
          PrevHira := False;

        Freq := fSJIS6879Table[W1]; // out of = 0
        if (Freq > 0) and (Freq <= Gap2700Gate) then
          Garbledlize := Garbledlize + (Gap2700Gate - Freq) / Gap2700Gate * -12 + (Gap2700Gate / Freq) * -1.8;
        if Freq > Gap2700Gate then
          Hanzilize := Hanzilize + (Freq - Gap2700Gate) / Gap2700Gate * 0.7 + (Freq / Gap2700Gate) * 0.35; // 26
        if Freq = 0 then
          Garbledlize := Garbledlize + -8; // -2~-10?
        INC(I, 2);
      end
      else
      begin
        if Byte(GuruRecord.RecLine[I]) in [$20] then
        begin
          if Garbledlize < 0 then
            Garbledlize := Garbledlize + (GuruRecord.RP / 60)
        end;
        if IsHalfWidthKatakana(Byte(GuruRecord.RecLine[I])) then
        begin
          if PrevHalfKata then
          begin
            // 1, 1
            if Garbledlize < 0 then Garbledlize := Garbledlize + 0.8;
            Hanzilize := Hanzilize + 1.2;
          end
          else
          begin
            // 0, 1
            Hanzilize := Hanzilize + 0.5; // chisanobonken
          end;
          PrevHalfKata := True;
        end
        else
          PrevHalfKata := False;

        INC(I);
      end;
    end;
    //Delta := Delta / (GuruRecord.HanziCount / 2);
    if Hanzilize >= Abs(Garbledlize) then
    begin
      //
      Delta := (Hanzilize + Garbledlize) / ((GuruRecord.HanziCount - 0.8) / ((GuruRecord.HanziCount + 2) / 2.3)) * (GuruRecord.RP / 60);
    end
    else
    begin
      Delta := (Garbledlize + Hanzilize / 0.75) / ((GuruRecord.HanziCount - 0.8) / ((GuruRecord.HanziCount + 4) / 8));
    end;

    GuruRecord.RP := GuruRecord.RP + Delta;
    if Abs(Delta) > 0.5 then
      INC(GuruRecord.Ref);
  end;
end;

function TPostLexFilters.DanceBIG5(var GuruRecord: GuruStringRecItem): Boolean;
begin
  Result := False;
end;

function TPostLexFilters.ProcessLine(var GuruRecord: GuruStringRecItem): Boolean;
begin
  Result := False;

  if fAlanGap then
  begin
    // pass pure latin or pure kanji 
    AskAlan(GuruRecord);
  end;

  if fGBKDancer then
  begin
    if fDanceMode in [rmGB2312, rmGBK] then
      DanceGBK(GuruRecord);
    if fDanceMode in [rmJIS] then
      DanceSJIS(GuruRecord);
    if fDanceMode in [rmBIG5] then
      DanceBIG5(GuruRecord);
  end;

  // if chek len
  if GuruRecord.LinkType in [ltPascal, ltLen] then // Pascal, Lenth
  begin
    GuruRecord.RP := GuruRecord.RP + 8 + GuruRecord.HanziCount / 2;
    if GuruRecord.HanziCount + GuruRecord.LatinCount < 5 then
      GuruRecord.RP := GuruRecord.RP + 6;
  end;
  if GuruRecord.LinkType in [ltShort] then // ANSI
  begin
    GuruRecord.RP := GuruRecord.RP + 5;  
  end;  

  if GuruRecord.Ref > 1 then
  begin
    if GuruRecord.RP < 70 then
      GuruRecord.RP := GuruRecord.RP - (75 - GuruRecord.RP) * 1.2;
  end;
end;

function TPostLexFilters.LoadGBKChartTable(filename: KOLString): Boolean;
var
  StrangeFile: PStream;
begin
  Result := False;
  StrangeFile := NewReadFileStream(filename);
  SetLength(fGBK21003Table, $7E * $BE);
  StrangeFile.Read(fGBK21003Table[0], Length(fGBK21003Table) * SizeOf(fGBK21003Table[0]));
  StrangeFile.Free;
end;

function TPostLexFilters.LoadSJISFreqTable(Filename: KOLString): Boolean;
var
  StrangeFile: PStream;
begin
  Result := False;
  StrangeFile := NewReadFileStream(Filename);
  SetLength(fSJIS6879Table, $2F * $BC);
  StrangeFile.Read(fSJIS6879Table[0], Length(fSJIS6879Table) * SizeOf(fSJIS6879Table[0]));
  StrangeFile.Free;
end;

constructor TPostLexFilters.Create;
begin
  inherited;
  fAlanGap := True;
  fGBKDancer := True;
  fDanceMode := rmGB2312;
end;

destructor TPostLexFilters.Destroy;
begin
  SetLength(fGBK21003Table, 0);
  SetLength(fSJIS6879Table, 0);
  inherited;
end;

function GBK2Index(Single: WORD): Integer;
begin
  Result := (Single and $FF - $81) * $BE;
  Single := Single shr 8;
  if Single in [$40..$7E] then
    Single := Single - $40
  else
    if Single in [$80..$FE] then
      Single := Single - $41; // 80 - (7E - 40 + 1)
  Result := Result + Single;
end;


function SJIS2Index(Single: WORD): Integer;
begin
  Result := (Single and $FF);
  if Result in [$81..$9F] then
    Dec(Result, $81)
  else
    if Result in [$E0..$EF] then
      Dec(Result, $C1); // E0 - (9F - 81 + 1)

  Single := Single shr 8;
  if Single in [$40..$7E] then
    Single := Single - $40
  else
    if Single in [$80..$FC] then
      Single := Single - $41;
  Result := Result * $BC + Single;
end;

var
  SJISActTable: Array of Byte;

procedure InitActualTable(Filename: KOLString);
var
  StrangeFile: PStream;
begin
  StrangeFile := NewReadFileStream(Filename);
  SetLength(SJISActTable, $2F * $BC);
  StrangeFile.Read(SJISActTable[0], Length(SJISActTable) * SizeOf(SJISActTable[0]));
  StrangeFile.Free;
end;

function IsSJISActual(B1, B2: Byte): Boolean;
begin
  Result := IsJIS(B1, B2);
  if Result then
  begin
    Result := SJISActTable[SJIS2Index(B2 shl 8 + B1)] > 0;
  end;
end;

initialization
  InitActualTable(GetStartDir() + 'sjistype.bin'); // TODO: optmize

finalization
  SetLength(SJISActTable, 0);

end.

