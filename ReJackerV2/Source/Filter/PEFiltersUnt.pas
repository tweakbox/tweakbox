unit PEFiltersUnt;

interface

uses
  Windows, KOL, PEReader;

type
  PRangeRecItem = ^TRangeRecItem;
  TRangeRecItem = record
    StartPos: Integer;
    EndPos:   Integer;
  end;

type
  PPEFilter = ^TPEFilter;
  TPEFilter = object(TObj)
  private
    fReader: PPEObj;
    fStringRanges: PList;
    fRefRanges: PList;
  protected
    constructor Create;
    destructor Destroy; virtual;
  public
    function AnalyseFile(Filename: KOLString): Boolean;
    function MaybeString(StartOffset, EndOffset: Integer): Boolean;
    function RVA2Offset(Address: DWORD): DWORD;
    function VA2Offset(Address: DWORD): DWORD;
    function Offset2VA(Offset: DWORD): DWORD;
    property ReferenceRanges: PList read fRefRanges;
  end;

function NewPEFilterEx(Filename: KOLString): PPEFilter;
function Goodname(SectionName: AnsiString): Boolean;
function Goodcode(SectionName: AnsiString): Boolean;

implementation

function NewPEFilterEx(Filename: KOLString): PPEFilter;
begin
  New(Result, Create);
  with Result^ do
  begin
    if not(AnalyseFile(Filename)) then
      Free_And_Nil(Result);
  end;
end;

function TPEFilter.AnalyseFile(Filename: KOLString): Boolean;
var
  I: Integer;
  mText, iText, mCode, iCode: Boolean;
  mStart: Integer;
  Range: PRangeRecItem;
begin
  Result := fReader.LoadFromFile(Filename, True);
  if Result = false then
    Exit;

  // start analyst header
  mText := False;
  mCode := False;
  mStart := 0; 
  for I := 0 to Length(fReader.SecHeaders) - 1 do
  begin
    iText := Goodname(PAnsiChar(@fReader.SecHeaders[I].Name[0]));
    iCode := Goodcode(PAnsiChar(@fReader.SecHeaders[I].Name[0]));
    
    if iText = True then
    begin
      if mText = True then Continue;
      if mText = False then
      begin
        mStart := fReader.SecHeaders[I].PointerToRawData;
      end;
    end
    else
    begin
      //0, 0
      if mText = False then Continue;
      //0, 1
      if mText = True then
      begin
        // Add
        New(Range);
        Range.StartPos := mStart;
        Range.EndPos := fReader.SecHeaders[I].PointerToRawData + fReader.SecHeaders[I].SizeOfRawData;
        fStringRanges.Add(Range);
      end;
    end;
    mText := iText;

    if iCode = True then
    begin
      if mCode = True then Continue;
      if mCode = False then
      begin
        mStart := fReader.SecHeaders[I].PointerToRawData;
      end;
    end
    else
    begin
      //0, 0
      if mCode = False then Continue;
      //0, 1
      if mCode = True then
      begin
        // Add
        New(Range);
        Range.StartPos := mStart;
        Range.EndPos := fReader.SecHeaders[I].PointerToRawData + fReader.SecHeaders[I].SizeOfRawData;
        fRefRanges.Add(Range);
      end;
    end;
    mCode := iCode;
    
  end;
end;

function TPEFilter.MaybeString(StartOffset, EndOffset: Integer): Boolean;
var
  I: Integer;
  Range: PRangeRecItem;
begin
  Result := False;
  for I := 0 to fStringRanges.Count - 1 do
  begin
    Range := fStringRanges.Items[I];
    //if (StartOffset in [Range.StartPos..Range.EndPos]) and (EndOffset in [Range.StartPos..Range.EndPos]) then
    if (StartOffset >= Range.StartPos) and (StartOffset <= Range.EndPos) and
     (EndOffset >= Range.StartPos) and (EndOffset <= Range.EndPos) then
    begin
      Result := True;
      Exit;
    end;
  end;
end;


function TPEFilter.RVA2Offset(Address: DWORD): DWORD;
//var
//  I: Integer;
begin
  {Result := $FFFFFFFF;
  For I := 0 To fReader.NTHeaders.FileHeader.NumberOfSections - 1 do
  begin
    if (Address >= fReader.SecHeaders[I].VirtualAddress) AND (Address < fReader.SecHeaders[I].VirtualAddress + fReader.SecHeaders[I].SizeOfRawData) then
    begin
      Result := Address - fReader.SecHeaders[I].VirtualAddress + fReader.SecHeaders[I].PointerToRawData; // delta
      exit;
    end;
  end;}
  Result := fReader.RVA2Offset(Address);
end;

function TPEFilter.VA2Offset(Address: Cardinal): DWORD;
begin
  //Result := MAXDWORD;
  Result := fReader.VA2Offset(Address);
end;

function TPEFilter.Offset2VA(Offset: Cardinal): DWORD;
//var
//  I: Integer;
begin
  (*Result := MAXDWORD;
  For I := 0 To fReader.NTHeaders.FileHeader.NumberOfSections - 1 do
  begin
    {if (Address >= fReader.SecHeaders[I].VirtualAddress) AND (Address < fReader.SecHeaders[I].VirtualAddress + fReader.SecHeaders[I].SizeOfRawData) then
    begin
      Result := Address - fReader.SecHeaders[I].VirtualAddress + fReader.SecHeaders[I].PointerToRawData; // delta
      exit;
    end;}
    if (Offset >= fReader.SecHeaders[I].PointerToRawData) AND (Offset < fReader.SecHeaders[I].PointerToRawData + fReader.SecHeaders[I].Misc.VirtualSize) then
    begin
      Result := Offset - fReader.SecHeaders[I].PointerToRawData + fReader.SecHeaders[I].VirtualAddress;
      Result := Result + fReader.NTHeaders.OptionalHeader.ImageBase;
      Exit;
    end;
  end;*)
  Result := fReader.Offset2VA(Offset);
end;


constructor TPEFilter.Create;
begin
  inherited;
  fReader := NewPEObj();
  fStringRanges := NewList();
  fRefRanges := NewList(); 
end;

destructor TPEFilter.Destroy;
var
  I: Integer;
begin
  if Assigned(fReader) then
    Free_And_Nil(fReader);
  for I := 0 to fStringRanges.Count - 1 do
  begin
    Dispose(PRangeRecItem(fStringRanges.Items[I]));
  end;
  for I := 0 to fRefRanges.Count - 1 do
  begin
    Dispose(PRangeRecItem(fRefRanges.Items[I]));
  end;
  Free_And_Nil(fStringRanges);
  Free_And_Nil(fRefRanges);

  inherited;
end;


const
  TextSections : array[0..5] of AnsiString = (
    'text',
    'code', 'data', // delphi7
    '.text', '.data', '.rdata' // vc n d2007 style?
  );
  CodeSections : array[0..5] of AnsiString = (
    'text',
    'code', 'data', // delphi7
    '.text', '.data', '.rdata' // vc n d2007 style?
  );

function Goodname(SectionName: AnsiString): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(TextSections) - 1 do
  begin
    if LowerCase(SectionName) = TextSections[I] then
    //if lstrcmpiA() then
    begin
      Result:= True;
      Exit;
    end;
  end;
end;

function Goodcode(SectionName: AnsiString): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Length(CodeSections) - 1 do
  begin
    if LowerCase(SectionName) = CodeSections[I] then
    //if lstrcmpiA() then
    begin
      Result:= True;
      Exit;
    end;
  end;
end;



end.

