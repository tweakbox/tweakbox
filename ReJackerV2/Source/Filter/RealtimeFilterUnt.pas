unit RealtimeFilterUnt;

interface

uses
  Windows, KOL;

var
  DamnWords: Array[0..10] of AnsiString = (
    '确定', '取消', '重试', '关闭', '打开', '禁止', '启用', '宋体', '错误', '成功', '失败'
  );
  DamnDWORDs: Array of DWORD;
  Damned: Boolean = False;
  
function PhraseShortCircuit(Phrase: AnsiString): Boolean;
function StrDWORDScanLen(Str: PAnsiChar; Chr: DWORD; Len: Integer): PAnsiChar;

implementation

uses
  PatternUtils;


function PhraseShortCircuit(Phrase: AnsiString): Boolean;
var
  I: Integer;
  //F, P: PAnsiChar;
begin
  Result := False;
  for I := 0 to Length(DamnWords) - 1 do
  begin
    //if DamnWords[I] = Phrase then
    //if StrComp(PChar(Phrase), PChar(DamnWords[I])) >= 0 then
    {$IFNDEF DWORDSCAN}
    if Pos(DamnWords[I], Phrase) > 0 then
    {$ELSE}
    if IndexOfStr(Phrase, DamnWords[I]) > 0 then
    (*P := PAnsiChar(Phrase);
    F := StrDWORDScanLen(P, DamnDWORDs[I], Length(Phrase));
    if Integer(F) > Integer(P) then*)
    {$ENDIF}
    begin
      Result := True;
      Exit;
    end;
  end;
end;

procedure Damning();
var
  ShortList: PStrList;
  NewShort: Integer;
  I: Integer;
begin
  if Damned then Exit;
  ShortList := NewStrList();
  ShortList.LoadFromFile(GetStartDir + 'Short.list'); //TODO: add customize
  SetLength(DamnDWORDs, Length(DamnWords) + ShortList.Count);
  NewShort := 0;
  for I := 0 to Length(DamnWords) - 1 do
  begin
    if Length(DamnWords[I]) = 4 then
    begin
      DamnDWORDs[NewShort] := PDWORD(@DamnWords[I][1])^;
      INC(NewShort);
    end;
  end;
  for I := 0 to ShortList.Count - 1 do
  begin
    if Length(ShortList.Items[I]) = 4 then
    begin
      DamnDWORDs[NewShort] := PDWORD(ShortList.ItemPtrs[I])^;
      INC(NewShort);
    end;
  end;
  SetLength(DamnDWORDs, NewShort);
  Damned := True;
end;

function StrDWORDScanLen(Str: PAnsiChar; Chr: DWORD; Len: Integer): PAnsiChar; assembler;
asm
  {$IFDEF F_P}
        MOV     EAX, [Str]
        MOVZX   EDX, [Chr]
        MOV     ECX, [Len]
  {$ENDIF F_P}
        PUSH    EDI
        XCHG    EDI, EAX
        XCHG    EAX, EDX
        REPNE   SCASD
        XCHG    EAX, EDI
        POP     EDI
        { -> EAX => to next character after found or to the end of Str,
             ZF = 0 if character found. }
end {$IFDEF F_P} [ 'EAX', 'EDX', 'ECX' ] {$ENDIF};

initialization
  Damning;

end.
