unit PatternUtils;

interface

uses Windows;

function SearchAndReplace(_targetadress: DWORD; _searchpattern: DWORD; _searchmask: DWORD; _replacepattern: DWORD;
			_replacemask: DWORD; _patternsize: DWORD; _searchsize: DWORD; _patchnumber: DWORD): Boolean;
function SearchPatternIn(var Source: Array of Byte; var Pattern: Array of Byte; SearchUp: Boolean = False; Offset: Integer = -1): Integer;

implementation


function SearchAndReplace(_targetadress: DWORD; _searchpattern: DWORD; _searchmask: DWORD; _replacepattern: DWORD; 
			_replacemask: DWORD; _patternsize: DWORD; _searchsize: DWORD; _patchnumber: DWORD): Boolean;
asm
  PUSH EBP
  MOV EBP, ESP
  ADD ESP, $-8
  PUSHAD
  MOV BYTE PTR SS:[EBP-$1], 0
  MOV DWORD PTR SS:[EBP-$8], 0
  MOV EDI, DWORD PTR SS:[EBP+$8]
  MOV ESI, DWORD PTR SS:[EBP+$C]
  MOV EDX, DWORD PTR SS:[EBP+$10]
  MOV EBX, DWORD PTR SS:[EBP+$1C]
  XOR ECX, ECX
  JMP @L042
@L012:
  MOV EAX, ECX
  ADD EAX, EBX
  CMP EAX, DWORD PTR SS:[EBP+$20]
  JA @L082
  PUSH ECX
  PUSH ESI
  PUSH EDI
  PUSH EDX
  MOV ECX, EBX
@L021:
  TEST ECX, ECX
  JE @L045
  CMP BYTE PTR DS:[EDX], 1
  JE @L031
  LODS BYTE PTR DS:[ESI]
  SCAS BYTE PTR ES:[EDI]
  JNZ @L036
  INC EDX
  DEC ECX
  JMP @L021
@L031:
  INC EDI
  INC ESI
  INC EDX
  DEC ECX
  JMP @L021
@L036:
  POP EDX
  POP EDI
  POP ESI
  POP ECX
  INC EDI
  INC ECX
@L042:
  CMP ECX, DWORD PTR SS:[EBP+$20]
  JNZ @L012
  JMP @L082
@L045:
  INC DWORD PTR SS:[EBP-$8]
  POP EDX
  POP EDI
  POP ESI
  MOV EAX, DWORD PTR SS:[EBP+$24]
  CMP EAX, -1
  JE @L057
  CMP DWORD PTR SS:[EBP-$8], EAX
  JE @L057
  POP ECX
  INC EDI
  JMP @L012
@L057:
  MOV ESI, DWORD PTR SS:[EBP+$14]
  MOV EDX, DWORD PTR SS:[EBP+$18]
  XOR ECX, ECX
  JMP @L070
@L061:
  CMP BYTE PTR DS:[EDX], 1
  JE @L066
  LODS BYTE PTR DS:[ESI]
  STOS BYTE PTR ES:[EDI]
  JMP @L068
@L066:
  INC EDI
  INC ESI
@L068:
  INC EDX
  INC ECX
@L070:
  CMP ECX, EBX
  JNZ @L061
  MOV BYTE PTR SS:[EBP-$1], 1
  POP ECX
  CMP DWORD PTR SS:[EBP+$24], -1
  JNZ @L082
  SUB EDI, EBX
  INC EDI
  INC ECX
  MOV ESI, DWORD PTR SS:[EBP+$C]
  MOV EDX, DWORD PTR SS:[EBP+$10]
  JMP @L012
@L082:
  POPAD
  MOVZX EAX, BYTE PTR SS:[EBP-$1]
  LEAVE
  //RETN $20
end;

function SearchPatternIn(var Source: Array of Byte; var Pattern: Array of Byte; SearchUp: Boolean; Offset: Integer): Integer;
var
  SourceLen, PattenLen: Integer;
  iPos, mPos: Integer;
  lHit: Boolean;
  dDelta: Integer;
begin
  SourceLen := Length(Source);
  PattenLen := Length(Pattern);
  if SearchUp then dDelta := -1 else dDelta := 1;
  if SearchUp then
  begin
    iPos := SourceLen - 1;
    mPos := PattenLen - 1;
  end
  else
  begin
    iPos := 0;
    mPos := 0;
  end;
  if Offset <> -1 then iPos := Offset;
  lHit := False;
  while (SearchUp and (iPos > 0)) or (not(SearchUp) and (iPos < SourceLen)) do
  begin
    //Assert((iPos < 0) or (iPos >= SourceLen), 'iPos out of index.');

    if Source[iPos] = Pattern[mPos] then
    begin
      if not(SearchUp) and (mPos = PattenLen - 1) then
      begin
        Result := iPos - PattenLen + 1;
        Exit;
      end;
      if SearchUp and (mPos = 0) then
      begin
        Result := iPos;
        Exit;
      end;
      Inc(iPos, dDelta); // next
      Inc(mPos, dDelta); // next
      lHit := True;
    end
    else
    begin
      if lHit then
      begin
        // last hit, but this time not hit
        // rewards to start + 1 from matched byte
        if SearchUp then
          Inc(iPos, PattenLen - 1 - mPos - 1)
        else
          Dec(iPos, mPos - 1); // back
      end
      else
      begin
        // last not hit, this both not hit
        Inc(iPos, dDelta); // next
      end;
      if SearchUp then mPos := PattenLen - 1 else mPos := 0; // reset
      lHit := False;
    end;
  end;

  Result := -1;
end;


end.
