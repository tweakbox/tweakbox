unit PEDirector;

interface

uses
  Windows, KOL, PEReader, PEAddon, LibKOLUnt{$IFDEF wince}, MzCommonDlg{$ENDIF};

type
  tagResEntryReloc = record
    Offset    :DWORD; // address to correct
    RVA       :DWORD;
  end;

  tagResItemMove = record
    EntryOffset  :DWORD; // write back after correct
    ItemRVA      :DWORD;
    Size         :DWORD;
  end;

type
  PPEDirectorObj = ^TPEDirectorObj;
  TPEDirectorObj = Object(TPEObj)
  private
    function InsertSpaceInJackedBuf(pos, size: DWORD; Fill: Byte): DWORD;
    function RemoveSpaceFromJackedBuf(pos, size: Integer): Integer;
    function SwapContentOfJackedBuf(pos1, pos2, size1, size2: DWORD): DWORD;
    function SwapSecHeader(var SecHeader1, SecHeader2: IMAGE_SECTION_HEADER): Boolean;
    function CutSecHeader(Idx: Integer): Boolean;
    function InsertSecHeader(Index: Integer; const SecHeader1: IMAGE_SECTION_HEADER): Boolean;
    function MoveResourceBuf(MoveDelta, WorkingOffset: Integer): Boolean;
    function GetResourceSec(var OtherDir: Integer): Integer;
    function GetSectionIndexForDir(Dir: Integer; var Mixed: Boolean): Integer;
  public
    function ResizeSection(Index: Integer; newRawSize, newVirtualSize: Cardinal; AutoFix: Boolean = False): Boolean;
    function GetLiteralSection(): Integer;
    function MakeupLiteralSection(Name: AnsiString): Boolean; // may better move to MUIMaker
    function GetPureRelocSectionIndex(var Dropable: Boolean): Integer;
    function GetPureResourceSectionIndex(): Integer;
    function HardRebaseModule(NewBase: DWORD): Boolean; // cause a buffer write back
    function ForceRemoveSection(Index: DWORD): Boolean;
    function FlushSectionToJackedBuf(): Boolean;
    function FlushNTHeaderToJackedBuf(): Boolean;
  end;

const
  LiteralSectionsName: Array[0..3] of AnsiString = (
    '.literal', '.string', '.ansi', '.fat'
  );

function NewPEDirector(): PPEDirectorObj;
function NewPEDirectorEx(Filename: KOLString): PPEDirectorObj; overload;
function NewPEDirectorEx(var Buffer: Array of Byte): PPEDirectorObj; overload;

implementation

function NewPEDirector(): PPEDirectorObj;
begin
  New(Result, Create);
end;

function NewPEDirectorEx(Filename: KOLString): PPEDirectorObj;
begin
  New(Result, Create);
  with Result^ do
  begin
    if not(LoadFromFile(Filename, False)) then
    begin
      Free_And_Nil(Result); // already suite for MPack
    end;
  end;  
end;

function NewPEDirectorEx(var Buffer: Array of Byte): PPEDirectorObj;
begin
  New(Result, Create);
  with Result^ do
  begin
    LoadFromBuffer(Buffer, Length(Buffer), False); // keep
  end;  

end;


function TPEDirectorObj.ResizeSection(Index: Integer; newRawSize: Cardinal; newVirtualSize: Cardinal; AutoFix: Boolean): Boolean;
var
  dummyHeader: IMAGE_SECTION_HEADER;
  SecIndex, DirIndex: Integer;
  RawDelta, VirtualDelta: Integer;
  OtherDir, ResourceIndex: Integer;
begin
  Result := False;
  if Index >= Length(SecHeaders) then
    Exit;

  dummyHeader := SecHeaders[Index];
  dummyHeader.SizeOfRawData := newRawSize;
  dummyHeader.Misc.VirtualSize := newVirtualSize;

  RawDelta := AlignByBlockSize(newRawSize, NTHeaders.OptionalHeader.FileAlignment) -
     AlignByBlockSize(SecHeaders[Index].SizeOfRawData, NTHeaders.OptionalHeader.FileAlignment);
  VirtualDelta := AlignByBlockSize(newVirtualSize, NTHeaders.OptionalHeader.SectionAlignment) -
     AlignByBlockSize(SecHeaders[Index].Misc.VirtualSize, NTHeaders.OptionalHeader.SectionAlignment); 

  if (RawDelta = 0) and (VirtualDelta = 0) then
  begin
    SecHeaders[Index] := dummyHeader;
    Result := True;
    Exit;
  end;

  if RawDelta > 0 then
    InsertSpaceInJackedBuf(SecHeaders[Index].PointerToRawData, RawDelta, 0);
  if RawDelta < 0 then
    RemoveSpaceFromJackedBuf(SecHeaders[Index].PointerToRawData, 0 - RawDelta);

  for SecIndex := Index + 1 to Length(SecHeaders) - 1 do
  begin
    INC(SecHeaders[SecIndex].PointerToRawData, RawDelta);
    INC(SecHeaders[SecIndex].VirtualAddress, VirtualDelta);
  end;
  SecHeaders[Index] := dummyHeader;
  INC(NTHeaders.OptionalHeader.SizeOfImage, RawDelta);

  if Index < Length(SecHeaders) - 1 then
  begin
    // atleast one section followed. try to fix directionarys
    for DirIndex := 0 to NTHeaders.OptionalHeader.NumberOfRvaAndSizes - 1 do
    begin
      if Integer(NTHeaders.OptionalHeader.DataDirectory[DirIndex].VirtualAddress) >= Integer(SecHeaders[Index + 1].VirtualAddress) - VirtualDelta then
      begin
        if DirIndex in [IMAGE_DIRECTORY_ENTRY_RESOURCE, IMAGE_DIRECTORY_ENTRY_BASERELOC, IMAGE_DIRECTORY_ENTRY_DEBUG] then
          INC(NTHeaders.OptionalHeader.DataDirectory[DirIndex].VirtualAddress, VirtualDelta)
        else
          // Dangours!!
          INC(NTHeaders.OptionalHeader.DataDirectory[DirIndex].VirtualAddress, VirtualDelta);
      end;
    end;
  end;

  // fix resource or reloc
  OtherDir := IMAGE_DIRECTORY_ENTRY_RESOURCE;
  ResourceIndex := GetResourceSec(OtherDir);
  if (ResourceIndex > Index) and (VirtualDelta > 0) then
  begin
    MoveResourceBuf(VirtualDelta, SecHeaders[ResourceIndex].PointerToRawData); // already inc some line ago
  end;

  FlushSectionToJackedBuf();
  FlushNTHeaderToJackedBuf();
  Result := True;
end;

function TPEDirectorObj.GetLiteralSection(): Integer;
var
  I, J: Integer;
begin
  Result := -1;
  for I := 0 to Length(LiteralSectionsName) - 1 do
  begin
    for J := 0 to Length(SecHeaders) - 1 do
    begin
{$IFDEF FPC}
      if LowerCase(PAnsiChar(Name2Str(SecHeaders[J].Name))) = LiteralSectionsName[I] then
{$ELSE}
      //if lstrcmpiA(PAnsiChar(@SecHeaders[J].Name[0]), PAnsiChar(LiteralSectionsName[I])) = 0 then
      //if StrIsStartingFromNoCase() then
      if lstrcmpiA(PAnsiChar(Name2Str(SecHeaders[J].Name)), PAnsiChar(LiteralSectionsName[I])) = 0 then
{$ENDIF}
      begin
        Result:= J;
        Exit;
      end;
    end;
  end;
end;

function TPEDirectorObj.MakeupLiteralSection(Name: AnsiString): Boolean;
var
  ResourceIndex, OtherDir: Integer;
  LiteralSec: IMAGE_SECTION_HEADER;
begin
  Result := False;
  if Length(Name) > 8 then Name := Copy(Name, 1, 8);
  OtherDir := IMAGE_DIRECTORY_ENTRY_RESOURCE;
  ResourceIndex := GetResourceSec(OtherDir);
  if (ResourceIndex = -1) or
      not(OtherDir in [IMAGE_DIRECTORY_ENTRY_RESOURCE, IMAGE_DIRECTORY_ENTRY_BASERELOC]) then
    Exit;
  // if these exit not fired, we get changce
  
  LiteralSec := SecHeaders[ResourceIndex];
  FillChar(LiteralSec.Name[0], 8, 0);
  Move(Name[1], LiteralSec.Name[0], Length(Name));
  LiteralSec.SizeOfRawData := 0;
  LiteralSec.Misc.VirtualSize := 0; //dual null
  Result := InsertSecHeader(ResourceIndex, LiteralSec);
end;

function TPEDirectorObj.GetResourceSec(var OtherDir: Integer): Integer;
var
  CurSecIdx, CurDirIdx: Integer;
begin
  Result := -1;

  // search first section before only [res, reloc]
  for CurSecIdx := Length(SecHeaders) -1 downto 0 do
  begin
    for CurDirIdx := 0 to NTHeaders.OptionalHeader.NumberOfRvaAndSizes - 1 do // count of directories
    begin
      if (NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress >= SecHeaders[CurSecIdx].VirtualAddress) and
         (NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress < SecHeaders[CurSecIdx].VirtualAddress + SecHeaders[CurSecIdx].Misc.VirtualSize) then
      begin
        if CurDirIdx in [IMAGE_DIRECTORY_ENTRY_RESOURCE, IMAGE_DIRECTORY_ENTRY_BASERELOC] then
        begin
          if CurDirIdx = IMAGE_DIRECTORY_ENTRY_RESOURCE then
            Result := CurSecIdx
          else
            // must be basereloc
            if CurSecIdx <> Result then
              Continue // next dir
            else
            begin
              OtherDir := IMAGE_DIRECTORY_ENTRY_BASERELOC;
              Continue; // reloc seems can mix with resource
            end;  
        end
        else
          Exit; // non resource or basereloc directory hited, I can't deal it today.
      end;
    end;
    // if in past test found ResourceIndex, Break;
    if Result > 0 then Break;
  end;
end;

function TPEDirectorObj.MoveResourceBuf(MoveDelta, WorkingOffset: Integer): Boolean;
var
  WorkingResDirVA: DWORD;
  Spos: Integer;
  NewRVA, JackedCount: DWORD;
  L1, L2, L3: Integer;
  resOffset:  DWORD;
  DIR1, DIR2, DIR3: IMAGE_RESOURCE_DIRECTORY;
  DirEntry1, DirEntry2, DirEntry3: IMAGE_RESOURCE_DIRECTORY_ENTRY;
  DataEntry1: IMAGE_RESOURCE_DATA_ENTRY;
begin
  Result := False;
  // please make sure there are right directory n section with baditems
  WorkingResDirVA := NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress;

  if MoveDelta = 0 then
  begin
    //ShowMsg('Nothing to do. gonna sleeping... ZzzzZzz ', MB_ICONINFORMATION or MB_OK);
    exit;
  end;

  // loop 4 times for each entry item, save for desc sort
  resOffset := RVA2Offset(WorkingResDirVA);
  if resOffset <> DWORD(WorkingOffset) then
  begin
    // sona bakana!
  end;
  Spos := resOffset; // offest of resource root directionary
  JackedCount := 0;
  Move(JackedBuf[spos], DIR1, sizeof(DIR1));
  for L1 := 0 to DIR1.NumberOfNamedEntries + DIR1.NumberOfIdEntries - 1 do
  begin
    Spos := Integer(resOffset) + sizeof(DIR1) + L1 * sizeof(DirEntry1);
    Move(JackedBuf[spos], DirEntry1, sizeof(DirEntry1));
    Spos := resOffset + (DirEntry1.OffsetToData and $7FFFFFFF);
    Move(JackedBuf[spos], DIR2, sizeof(DIR2));
    for L2 := 0 to DIR2.NumberOfNamedEntries + DIR2.NumberOfIdEntries - 1 do
    begin
      Spos := Integer(resOffset) + Integer(DirEntry1.OffsetToData and $7FFFFFFF) + sizeof(DIR2) + L2 * sizeof(DirEntry2);
      Move(JackedBuf[spos], DirEntry2, sizeof(DirEntry2));
      Spos := resOffset + (DirEntry2.OffsetToData and $7FFFFFFF);
      Move(JackedBuf[spos], DIR3, sizeof(DIR3));
      for L3 := 0 to DIR3.NumberOfNamedEntries + DIR3.NumberOfIdEntries - 1 do
      begin
        Spos := Integer(resOffset) + Integer(DirEntry2.OffsetToData and $7FFFFFFF) + sizeof(DIR3) + L3 * sizeof(DirEntry3);
        Move(JackedBuf[spos], DirEntry3, sizeof(DirEntry3));
        Spos := resOffset + (DirEntry3.OffsetToData and $7FFFFFFF);
        Move(JackedBuf[spos], DataEntry1, sizeof(DataEntry1));
        NewRVA := Integer(DataEntry1.OffsetToData) + MoveDelta;
        DataEntry1.OffsetToData := NewRVA;
        Move(DataEntry1, JackedBuf[spos], sizeof(DataEntry1));
        Inc(JackedCount);
      end; // Loop3
    end; // Loop2
  end; // Loop1
  // Write back header
  NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_RESOURCE].VirtualAddress := WorkingResDirVA;
  //FlushSectionToJackedBuf();
  FlushNTHeaderToJackedBuf();
  Result := JackedCount > 0;
end;


function TPEDirectorObj.GetSectionIndexForDir(Dir: Integer; var Mixed: Boolean): Integer;
var
  CurSecIdx, CurDirIdx: Integer;
begin
  Result := -1;
  Mixed := False;

  if NTHeaders.OptionalHeader.NumberOfRvaAndSizes <= IMAGE_DIRECTORY_ENTRY_BASERELOC then
  begin
    Exit; // Failed
  end;

  // search last section entire  before only [res, reloc]
  for CurSecIdx := Length(SecHeaders) -1 downto 0 do
  begin
    if (NTHeaders.OptionalHeader.DataDirectory[Dir].VirtualAddress >= SecHeaders[CurSecIdx].VirtualAddress) and
       (NTHeaders.OptionalHeader.DataDirectory[Dir].VirtualAddress < SecHeaders[CurSecIdx].VirtualAddress + SecHeaders[CurSecIdx].Misc.VirtualSize) then
    begin
      // directory all inside single section.
      // we will check if there other dir in this sec later.
      Result := CurSecIdx;
      Break;
    end;
  end;
  if Result = -1 then
  begin
    Exit;
  end;
  CurSecIdx := Result;
  for CurDirIdx := 0 to NTHeaders.OptionalHeader.NumberOfRvaAndSizes - 1 do // count of directories
  begin
    if CurDirIdx = Dir then
    begin
      Continue;
    end;
    if (NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress >= SecHeaders[CurSecIdx].VirtualAddress) and
       (NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress < SecHeaders[CurSecIdx].VirtualAddress + SecHeaders[CurSecIdx].Misc.VirtualSize) then
    begin
      Mixed := True;
      Exit;
    end;
  end;
end;

function TPEDirectorObj.GetPureRelocSectionIndex(var Dropable: Boolean): Integer;
var
  Mixed: Boolean;
begin
  Result := GetSectionIndexForDir(IMAGE_DIRECTORY_ENTRY_BASERELOC, Mixed);
  Dropable := not(Mixed);
end;

function TPEDirectorObj.GetPureResourceSectionIndex(): Integer;
var
  Mixed: Boolean;
begin
  Result := GetSectionIndexForDir(IMAGE_DIRECTORY_ENTRY_RESOURCE, Mixed);
  if Mixed then
  begin
    Result := -1;
  end;
end;

function TPEDirectorObj.HardRebaseModule(NewBase: DWORD): Boolean;
var
  BaseReloc1: IMAGE_BASE_RELOCATION;
  tpos, idx, ipos, itemnum, HLaddr: Integer;
  RelocRec: WORD;
  ThunkDelta: Int64;
begin
  // post rebase DLL by relocation table
  // you result file may not load if you get an unaligned imagebase
  Result := False;
  if NewBase = NTHeaders.OptionalHeader.ImageBase then
  begin
    Exit;
  end;
  ThunkDelta := NewBase - NTHeaders.OptionalHeader.ImageBase;
  tpos := RVA2Offset(NTHeaders.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC].VirtualAddress);
  while True do
  begin
    Move(JackedBuf[tpos], BaseReloc1, sizeof(BaseReloc1));
    if BaseReloc1.VirtualAdress = 0 then
      Break;
    itemnum := (BaseReloc1.SizeOfBlock - sizeof(BaseReloc1)) div 2;
    for idx := 0 to itemnum - 1 do
    begin
      Move(JackedBuf[tpos + sizeof(BaseReloc1) + idx * sizeof(RelocRec)], RelocRec, sizeof(RelocRec));
      if RelocRec shr 12 = 3 then
      begin
        // Item need for full relocate
        ipos := RVA2Offset(BaseReloc1.VirtualAdress + (RelocRec and $FFF));
        Move(Jackedbuf[ipos], HLaddr, Sizeof(HLaddr));
        HLAddr := HLAddr + ThunkDelta;
        Move(HLaddr, Jackedbuf[ipos], sizeof(HLaddr));
        Result := True;
      end;
    end;
    Inc(tpos, baseReloc1.SizeOfBlock);
  end;
  // Write back NTHeaders
  Inc(Integer(NTHeaders.OptionalHeader.ImageBase), ThunkDelta);
  tpos := DOSHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF};
  Move(NTHeaders, JackedBuf[tpos], sizeof(NTHeaders));
end;

function TPEDirectorObj.ForceRemoveSection(Index: Cardinal): Boolean;
var
  CurDirIdx: Integer;
  Spos: Integer;
  CutSize: DWORD;
begin
  Result := False;
  // check PE data directionary for clean Reloc n Etc
  for CurDirIdx := 0 to NTHeaders.OptionalHeader.NumberOfRvaAndSizes - 1 do
  begin
    if (NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress >= SecHeaders[Index].VirtualAddress) and
       (NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress < SecHeaders[Index].VirtualAddress + SecHeaders[Index].Misc.VirtualSize) then
    begin
      // set the specified section to zero
      NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].VirtualAddress := 0;
      NTHeaders.OptionalHeader.DataDirectory[CurDirIdx].Size := 0;
    end;
  end;
  CutSize := SecHeaders[Index].SizeOfRawData;
  if CutSize mod NTHeaders.OptionalHeader.FileAlignment > 0 then
    CutSize := AlignByBlocksize(CutSize, NTHeaders.OptionalHeader.FileAlignment);//CutSize + NTHeaders.OptionalHeader.FileAlignment - CutSize mod NTHeaders.OptionalHeader.FileAlignment;
  RemoveSpaceFromJackedBuf(SecHeaders[Index].PointerToRawData, CutSize);
  CutSecHeader(Index);
  Spos := DOSHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF};
  Move(NTHeaders, JackedBuf[spos], sizeof(NTHeaders));
  Spos := DosHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF} + $F8;
  Move(SecHeaders[0], JackedBuf[spos], sizeof(SecHeaders[0]) * (NTHeaders.FileHeader.NumberOfSections + 1)); // Write back wiped
  //NewThreadAutoFree(OnPreJackThreadExecute); // Refresh
end;

function TPEDirectorObj.InsertSpaceInJackedBuf(pos, size: DWORD; Fill: Byte): DWORD;
var
  iBuf: Array of Byte;
begin
  // we do not check the block
  SetLength(fJackedBuf, DWORD(Length(fJackedBuf)) + size); //pre extension
  if pos + size < DWORD(Length(fJackedBuf)) then
  begin
    // move part after pos
    SetLength(iBuf, DWORD(Length(fJackedBuf)) - size - pos);
    Move(fJackedBuf[pos], iBuf[0], Length(iBuf));
    Move(iBuf[0], fJackedBuf[pos + size], Length(iBuf));
  end;
  SetLength(iBuf, 0);
  FillChar(fJackedBuf[pos], size, Fill);
  Result := pos;
end;

function TPEDirectorObj.SwapContentOfJackedBuf(pos1, pos2, size1, size2: DWORD): DWORD;
var
  iBuf1, iBuf2: Array of Byte;
begin
  SetLength(iBuf1, size1);
  SetLength(iBuf2, size2);
  Move(fJackedBuf[pos1], iBuf1[0], size1);
  Move(fJackedBuf[pos2], iBuf2[0], size2);
  Move(iBuf2[0], fJackedBuf[pos1], size2);
  Move(iBuf1[0], fJackedBuf[pos2 + size2 - size1], size1);
  SetLength(iBuf1, 0);
  SetLength(iBuf2, 0);
  Result := size1 + size2;
end;

function TPEDirectorObj.SwapSecHeader(var SecHeader1, SecHeader2: IMAGE_SECTION_HEADER): Boolean;
var
  dummyHeader: IMAGE_SECTION_HEADER;
  RSize2, VSize2: LongInt;
begin
  RSize2 := AlignByBlocksize(SecHeader2.SizeOfRawData, NTHeaders.OptionalHeader.FileAlignment);
  VSize2 := AlignByBlocksize(SecHeader2.Misc.VirtualSize, NTHeaders.OptionalHeader.SectionAlignment);
  Move(SecHeader1, dummyHeader, sizeof(dummyHeader));
  Move(SecHeader2, SecHeader1, sizeof(dummyHeader)); //now 1 n 2 both equa 2, T = 1
  SecHeader1.VirtualAddress := dummyHeader.VirtualAddress; //Reset VA1
  SecHeader1.PointerToRawData := dummyHeader.PointerToRawData; //Reset RA1
  Inc(dummyHeader.VirtualAddress, VSize2);
  Inc(dummyHeader.PointerToRawData, Rsize2);
  Move(dummyHeader, SecHeader2, sizeof(dummyHeader));
  Result := True;
end;

function TPEDirectorObj.RemoveSpaceFromJackedBuf(pos, size: Integer): Integer;
var
  Tbuf:  Array of byte;
  size2: Integer;
begin
  size2 := length(Jackedbuf) - size - pos;
  SetLength(TBuf, size2);
  Move(fJackedBuf[pos + size], Tbuf[0], size2);
  SetLength(fJackedbuf, pos + size2);
  Move(TBuf[0], fJackedBuf[pos], size2);
  SetLength(Tbuf, 0);
  Result := size2;
end;

function TPEDirectorObj.CutSecHeader(idx: Integer): Boolean;
var
  CurIdx: Integer;
  CurVSize, CurRSize: DWORD;
begin
  CurVSize := AlignByBlocksize(SecHeaders[idx].Misc.VirtualSize, NTHeaders.OptionalHeader.SectionAlignment);
  CurRSize := AlignByBlocksize(SecHeaders[Idx].SizeOfRawData, NTHeaders.OptionalHeader.FileAlignment);
  for Curidx := idx to NTHeaders.FileHeader.NumberOfSections - 2 do
  begin
    // move section forwarded and subtrate cutted size
    Move(SecHeaders[CurIdx + 1], SecHeaders[CurIdx], Sizeof(SecHeaders[0]));
    Dec(SecHeaders[CurIdx].VirtualAddress, CurVSize);
    Dec(SecHeaders[CurIdx].PointerToRawData, CurRSize);
  end;
  // clear last section, dec section count, subtrate sizeofimage
  FillChar(SecHeaders[NTHeaders.FileHeader.NumberOfSections - 1], Sizeof(SecHeaders[0]), #$00);
  Dec(NTHeaders.FileHeader.NumberOfSections);
  Dec(NTHeaders.OptionalHeader.SizeOfImage, CurVSize);
  Result := True;
end;

function TPEDirectorObj.InsertSecHeader(Index: Integer; const SecHeader1: IMAGE_SECTION_HEADER): Boolean;
var
  SecIndex, spos: Integer;
  Increment: Integer;
begin
  //Result := False;
  spos := DosHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF} + SizeOf(NTHeaders); // $F8
  spos := spos - (16 - Integer(NTHeaders.OptionalHeader.NumberOfRvaAndSizes)) * SizeOf(IMAGE_DATA_DIRECTORY);
  if Integer(SecHeaders[0].PointerToRawData) - spos < sizeof(SecHeaders[0]) * (Length(SecHeaders) + 1) then
  begin
    // Calc newHeaderSize - HeaderSize
    Increment := AlignByBlockSize(sizeof(SecHeaders[0]) * (Length(SecHeaders) + 1), NTHeaders.OptionalHeader.FileAlignment) - Integer(SecHeaders[0].PointerToRawData);
    InsertSpaceInJackedBuf(SecHeaders[0].PointerToRawData, Increment, 0);
  end;
  // seems no need to modify VA delta now
  SetLength(SecHeaders, Length(SecHeaders) + 1);
  INC(NTHeaders.FileHeader.NumberOfSections);
  for SecIndex := Length(SecHeaders) downto Index + 1 do
  begin
    SecHeaders[SecIndex] := SecHeaders[SecIndex - 1];
  end;
  // TODO: optimize to Meory Move
  SecHeaders[Index] := SecHeader1; // copy record
  FlushSectionToJackedBuf();
  FlushNTHeaderToJackedBuf();
  Result := True; 
end;

function TPEDirectorObj.FlushSectionToJackedBuf(): Boolean;
var
  spos: Integer;
begin
  //Result := False;
  if NTHeaders.FileHeader.NumberOfSections <> Length(SecHeaders) then
  begin
    //Exit;
    NTHeaders.FileHeader.NumberOfSections := Length(SecHeaders);
    FlushNTHeaderToJackedBuf();
  end;
  spos := DosHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF} + SizeOf(NTHeaders);//$F8; // sizeof NTHeaders
  spos := spos - (16 - Integer(NTHeaders.OptionalHeader.NumberOfRvaAndSizes)) * SizeOf(IMAGE_DATA_DIRECTORY);  
  Move(SecHeaders[0], JackedBuf[spos], sizeof(SecHeaders[0]) * Length(SecHeaders){NTHeaders.FileHeader.NumberOfSections});
  Result := True;
end;

function TPEDirectorObj.FlushNTHeaderToJackedBuf(): Boolean;
var
  npos: Integer;
begin
  Result := False;
  npos := DOSHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF};
  Move(NTHeaders, JackedBuf[npos], sizeof(NTHeaders));
end;

end.
