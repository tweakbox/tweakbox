unit PEReader;

interface

uses
  Windows, KOL, PEAddon{$IFDEF wince}, MzCommonDlg{$ENDIF};

(*Tags*)
type
  //PIMAGE_RESOURCE_DIRECTORY = ^IMAGE_RESOURCE_DIRECTORY;
  IMAGE_RESOURCE_DIRECTORY = packed record
    Characteristics : DWORD;
    TimeDateStamp   : DWORD;
    MajorVersion    : WORD;
    MinorVersion    : WORD;
    NumberOfNamedEntries : WORD;
    NumberOfIdEntries : WORD;
  end;

  IMAGE_RESOURCE_DIRECTORY_ENTRY = packed record
    Name : DWORD;
    OffsetToData : DWORD;
  end;
  //PIMAGE_RESOURCE_DATA_ENTRY = ^IMAGE_RESOURCE_DATA_ENTRY;
  IMAGE_RESOURCE_DATA_ENTRY = packed record
    OffsetToData    : DWORD;
    Size            : DWORD;
    CodePage        : DWORD;
    Reserved        : DWORD;
  end;
  ByteArray = Array of Byte;
  SectionArray = Array of IMAGE_SECTION_HEADER;
  
(*Objects*)
type
  PPEObj = ^TPEObj;
  TPEObj = object(TObj)
  protected
    fJackedBuf:   ByteArray;
  private
    function BuildHeaderFromBuf(): Boolean;
  public
    function LoadFromFile(Filename: KOLString; OnlyMeta: Boolean): Boolean;
    function LoadFromBuffer(var Buffer; Size: Integer; OnlyMeta: Boolean): Boolean;
    function DropJackedBuf(): Boolean;
  public
    constructor Create();
    destructor Destroy(); virtual;
  public
    DOSHeader:    IMAGE_DOS_HEADER;
    NTHeaders:    IMAGE_NT_HEADERS;
    FileHeader:   IMAGE_FILE_HEADER;
    SecHeaders:   Array of IMAGE_SECTION_HEADER;
  public
    function RVA2Offset(Address: DWORD): DWORD;
    function VA2Offset(Address: DWORD): DWORD;
    function Offset2RVA(Offset: DWORD): DWORD;
    function Offset2VA(Offset: DWORD): DWORD;
  public
  //  property Sections: Array of IMAGE_SECTION_HEADER read SecHeaders;
  // TODO: typedef them
    property JackedBuf: ByteArray read fJackedBuf; 
  end;

function NewPEObj():PPEObj;
function NewPEObjEx(Filename: KOLString; OnlyMeta: Boolean = True): PPEObj;
function AlignByBlocksize(BaseSize, BlockSize: Integer): Integer;
function Name2Str(Name:Array of Byte): AnsiString;  

implementation

function NewPEObj():PPEObj;
begin
  New(Result, Create);
end;

function NewPEObjEx(Filename: KOLString; OnlyMeta: Boolean): PPEObj;
begin
  New(Result, Create);
  with Result^ do
  begin
{$IFDEF wince}
    LoadFromFile(Filename, OnlyMeta);
{$ELSE}
    if not(LoadFromFile(Filename, OnlyMeta)) then
    begin
      Free_And_Nil(Result);
    end;
{$ENDIF}
  end;
end;

function TPEObj.BuildHeaderFromBuf(): Boolean;
var
  spos: Integer;
begin
  Result := False;

  spos := 0;
  Move(fJackedBuf[spos], DOSHeader, sizeof(DOSHeader));
  if DOSHeader.e_magic <> $5A4D then
  begin
{$IFDEF wince}
    MzMessageBoxEx(0, 'STOP! foo me? It''s not a PE File! ', '', MZ_OK);
{$ELSE}
    //ShowMsg('STOP! foo me? It''s not a PE File! ', MB_OK or MB_ICONSTOP);
{$ENDIF}
    DropJackedBuf();
    Exit;
  end;
  // yes a pe
  spos := DOSHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF};
  // FileHeader n OptionalHeader
  Move(fJackedBuf[spos], NTHeaders, sizeof(NTHeaders));
  if NTHeaders.Signature <> $00004550 then
  begin
    ShowMsg('Sorry... I can''t locate NT Header! Maybe it''s an Hardcore Jacker. ', MB_OK or MB_ICONSTOP);
    DropJackedBuf();
    Exit;
  end;
  // try to retrieve SecHeaders
  SetLength(SecHeaders, NTHeaders.FileHeader.NumberOfSections);
  spos := DosHeader.{$IFDEF FPC}e_lfanew{$ELSE}_lfanew{$ENDIF} + $F8;
  Move(fJackedBuf[spos], SecHeaders[0], sizeof(SecHeaders[0]) * NTHeaders.FileHeader.NumberOfSections);

  Result := True;
end;

function TPEObj.LoadFromFile(Filename: KOLString; OnlyMeta: Boolean): Boolean;
var
  InputFStrm: PStream;
begin
  Result := False;

  if not(FileExists(Filename)) then Exit;

  InputFStrm := NewReadFileStream(Filename);
  SetLength(fJackedBuf, InputFStrm.Size);
  InputFStrm.Read(fJackedBuf[0], InputFStrm.Size);
  InputFStrm.Free;

  Result := BuildHeaderFromBuf();

  if OnlyMeta then
    DropJackedBuf();

  //Result := True;
end;

function TPEObj.LoadFromBuffer(var Buffer; Size: Integer; OnlyMeta: Boolean): Boolean;
begin
  //Result := False;
  SetLength(fJackedBuf, Size);
  Move(Buffer, fJackedBuf[0], Size);
  Result := BuildHeaderFromBuf();

  if OnlyMeta then
    DropJackedBuf();

  //Result := True;
end;

function TPEObj.RVA2Offset(Address: DWORD): DWORD;
var
  I: Integer;
begin
  Result := $FFFFFFFF;
  For I := 0 To NTHeaders.FileHeader.NumberOfSections - 1 do
  begin
    //if (Address >= SecHeaders[I].VirtualAddress) AND (Address < SecHeaders[I].VirtualAddress + SecHeaders[I].SizeOfRawData) then
    if (Address >= SecHeaders[I].VirtualAddress) AND
       (Address < SecHeaders[I].VirtualAddress + DWORD(AlignByBlockSize(SecHeaders[I].Misc.VirtualSize, NTHeaders.OptionalHeader.SectionAlignment))) then
    begin
      Result := Address - SecHeaders[I].VirtualAddress + SecHeaders[I].PointerToRawData; // delta
      exit;
    end;
  end;
end;

function TPEObj.VA2Offset(Address: Cardinal): DWORD;
begin
  Address := Address - NTHeaders.OptionalHeader.ImageBase;
  Result := RVA2Offset(Address);
end;

function TPEObj.Offset2RVA(Offset: Cardinal): DWORD;
var
  I: Integer;
begin
  Result := MAXDWORD;
  For I := 0 To NTHeaders.FileHeader.NumberOfSections - 1 do
  begin
    //if (Offset >= SecHeaders[I].PointerToRawData) AND (Offset < SecHeaders[I].PointerToRawData + SecHeaders[I].Misc.VirtualSize) then
    if (Offset >= SecHeaders[I].PointerToRawData) AND
       (Offset < SecHeaders[I].PointerToRawData + DWORD(AlignByBlockSize(SecHeaders[I].SizeOfRawData, NTHeaders.OptionalHeader.FileAlignment))) then
    begin
      Result := Offset - SecHeaders[I].PointerToRawData + SecHeaders[I].VirtualAddress;
      Exit;
    end;
  end;
end;

function TPEObj.Offset2VA(Offset: Cardinal): DWORD;
begin
  Result := Offset2RVA(Offset);
  Result := Result + NTHeaders.OptionalHeader.ImageBase;
end;

function TPEObj.DropJackedBuf;
begin
  Result := True;
  SetLength(fJackedBuf, 0);
end;

constructor TPEObj.Create;
begin
  inherited;
end;

destructor TPEObj.Destroy;
begin
  SetLength(fJackedBuf, 0);
  inherited;
end;

function AlignByBlocksize(BaseSize, BlockSize: Integer): Integer;
begin
  //if BaseSize mod BlockSize > 0 then
  //  BaseSize := BaseSize + BlockSize - (BaseSize mod BlockSize);
  Result := (BaseSize + BlockSize - 1) div BlockSize * BlockSize;
end;

function Name2Str(Name: Array of Byte): AnsiString;
begin
  SetLength(Result, Length(Name));
  Move(Name[0], Result[1], Length(Result));
end;

end.

