#ifndef _DIRTY_DEFINATIONS
#define _DIRTY_DEFINATIONS

#include <mzfc_inc.h>

#define MZ_IDC_TOOLBAR1  101

#define IDC_PPM_OK      102
#define IDC_PPM_CANCEL  103

#define MZ_IDC_SCROLLWIN    104
#define MZ_IDC_UNLOCK       105
#define MZ_IDC_WIFI         106
#define MZ_IDC_HACKMODE     107
#define MZ_IDC_LIST         108
#define MZ_IDC_IMECOMBO     109
#define MZ_IDC_UNINSTALL    110
#define MZ_IDC_UNLOCK_ROW   111
#define MZ_IDC_UNLOCK_NEXT  112

#define Modes 3
#define UnlockFields 8

//extern CMzStringW ModeNames[Modes]; // TODO: Extract to DirtyDefinations.cpp
//extern CMzStringW UnlockFieldNames[UnlockFields];
extern int UnlockFieldIDs[UnlockFields];

#endif
