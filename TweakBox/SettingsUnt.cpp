#include <Mzfc\MzProgressDialog.h>
#include <Mzfc\FontHelper.h>
#include "..\FvMzMd\FvMzKOL.h"
#include "SettingsUnt.h"
#include "StringInterner.h"
#include <sipapi.h>

#include "DirtyDefination.h"

void AutoScrollEdit::OnFocused( UiWin * pWinPrev )
{
    // Calc clipped Scrollable Size
    SIPINFO info;
    info.cbSize = sizeof(SIPINFO);
    SipGetInfo(&info);
    //int SipPanelHeight = info.rcSipRect.bottom - info.rcSipRect.top;
    int EditorBottom = GetPosY() + GetHeight();
    if (info.rcSipRect.top - info.rcVisibleDesktop.top < EditorBottom/* || GetPosY() < 0*/) {
        // Shrink
        ((UiScrollWin*)(GetParent()))->SetPos(0, 0, GetParent()->GetWidth(), GetParent()->GetHeight() - (info.rcSipRect.bottom - info.rcSipRect.top), SP_NOMOVE);
        //((UiScrollWin*)(GetParent()))->MoveTopPos(8 - GetPosY()); // first invoke should get ScrollWin
        ((UiScrollWin*)(GetParent()))->MoveTopPos(8 - GetPosY()); // first invoke should get ScrollWin
        ((UiScrollWin*)(GetParent()))->Update();
    }
    //SetCursePos(GetCursePos()); // workaround
    UiSingleLineEdit::OnFocused(pWinPrev);
}

void AutoScrollEdit::OnLostFocus( UiWin *pWinNext )
{
    if (GetParent() == pWinNext || NULL == pWinNext ) {
        // move to main
        ((UiScrollWin*)(GetParent()))->SetTopPos(0);
        ((UiScrollWin*)(GetParent()))->SetPos(0, 0, GetParent()->GetWidth(), GetParent()->GetParent()->GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR);
        ((UiScrollWin*)(GetParent()))->Update();
    }
    UiSingleLineEdit::OnLostFocus(pWinNext); // inherited
}
// Header n unit
MZ_IMPLEMENT_DYNAMIC(GroupEdtFrm)


void GroupEdtFrm::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex==0)
            {
                // commit
                bool changed = false;
                if (fEditList != NULL){
                    for (list <tagEditRecItem>::iterator it = fEditList->begin(); it != fEditList->end(); it++) {
                        if ((*it).Value.Compare((*it).Editor->GetText()) != 0) {
                            (*it).Value = (*it).Editor->GetText();
                            changed = true;
                        }
                    }
                }
                EndModal(changed?ID_OK:ID_CANCEL);
                return;
            }

            if (nIndex==2)
            {
                // rollback
                EndModal(ID_CANCEL);
                return;
            }

        }
    }
}

BOOL GroupEdtFrm::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }

    // Then init the controls & other things in the window
    if (fEditList != NULL) {
        m_ScrollWin.SetPos(0, 0, GetWidth(), GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR);
        m_ScrollWin.EnableScrollBarV(true);
        AddUiWin(&m_ScrollWin);
        int top = 0;
        int left = 24;
        for (list <tagEditRecItem>::iterator it = fEditList->begin(); it != fEditList->end(); it++) {
            AutoScrollEdit* Editor = new AutoScrollEdit;

            top += 8;
            Editor->SetPos(left, top, GetWidth() - left * 2, MZM_HEIGHT_SINGLELINE_EDIT);
            Editor->SetText((*it).Value);
            Editor->SetMaxChars(14); // in brief UI
            Editor->SetTip2((*it).FieldName + L":");
            Editor->SetLeftInvalid(120);
            //Editor->OnFocusd = &GroupEdtFrm::OnInitDialog;
            (*it).Editor = Editor;
            m_ScrollWin.AddChild(Editor);
            top += MZM_HEIGHT_SINGLELINE_EDIT;
        }
        
        //m_PlaceHolder.SetPos(0, top, GetWidth(), m_ScrollWin.GetHeight());
        //m_ScrollWin.AddChild(&m_PlaceHolder);
    }

    m_Toolbar.SetPos(0,GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR, GetWidth(), MZM_HEIGHT_TEXT_TOOLBAR);
    m_Toolbar.SetButton(0, true, true, _tr(IDS_STRING_RETURN));
    m_Toolbar.EnableLeftArrow(true);
    m_Toolbar.SetButton(2, true, true, _tr(IDS_STRING_CANCEL));
    m_Toolbar.SetID(MZ_IDC_TOOLBAR1);
    AddUiWin(&m_Toolbar);

    return TRUE;
}

GroupEdtFrm::~GroupEdtFrm()
{
    if (fEditList != NULL){
        for (list <tagEditRecItem>::iterator it = fEditList->begin(); it != fEditList->end(); it++) {
            delete (*it).Editor;
        }
        //delete fEditList;
    }
}
