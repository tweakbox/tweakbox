#ifndef _UNINSTALL_UNIT
#define _UNINSTALL_UNIT

#include <mzfc_inc.h>
#include "resource.h"
#include <list>

struct tagInstallionRecItem {
    CMzStringW  CabFile;
    CMzStringW  AppName;
    bool        Checked;
};
typedef tagInstallionRecItem* PInstallionRecItem;

// A new list control derived from UiList
class LiteList: public UiList
{
private:
    
public:
    // override the DrawItem member function to do your own drawing of the list
    void DrawItem(HDC hdcDst, int nIndex, RECT* prcItem, RECT *prcWin, RECT *prcUpdate);
protected:
private:
};

// Popup window derived from CMzWndEx
class UninstallFrm:
    public CMzWndEx
{
    MZ_DECLARE_DYNAMIC(UninstallFrm);
private:
    int* fResult;
    list <PInstallionRecItem> fInstallionList;
public:
    UninstallFrm(){ };
    ~UninstallFrm();
protected:
    UiToolbar_Text m_Toolbar;
    LiteList m_List;

    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    void UpdateAppList();
    // override the MZFC window messages handler
    LRESULT MzDefWndProc(UINT message, WPARAM wParam, LPARAM lParam);

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);
private:
};

#endif