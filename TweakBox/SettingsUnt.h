#ifndef _SETTINGS_UNIT
#define _SETTINGS_UNIT

#include <mzfc_inc.h>
#include "resource.h"
#include <list>

using std::list;

class AutoScrollEdit;
struct tagEditRecItem {
    int                 FieldID;
    CMzStringW          FieldName;
    CMzStringW          Value;
    AutoScrollEdit*   Editor;
};
typedef tagEditRecItem* PEditRecItem;

class AutoScrollEdit: public UiSingleLineEdit {
protected:
    virtual void OnFocused( UiWin *  pWinPrev );
    virtual void OnLostFocus( UiWin *pWinNext );
};

// Popup window derived from CMzWndEx
class GroupEdtFrm:
    public CMzWndEx
{
    MZ_DECLARE_DYNAMIC(GroupEdtFrm);
private:
    int* fResult;
    list <tagEditRecItem>* fEditList;
public:
    GroupEdtFrm(list <tagEditRecItem>* editlist):fEditList(editlist){ };
    ~GroupEdtFrm();
protected:
    UiToolbar_Text m_Toolbar;
    UiScrollWin m_ScrollWin;
    UiStatic m_PlaceHolder;

    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);
private:
};

#endif