#ifndef _IME_HELPER
#define _IME_HELPER

#include <mzfc_inc.h>
#include <sipapi.h>

struct tagIMERecItem {
    CMzStringW  DisplayName;
    CLSID       ClassID;
};
typedef tagIMERecItem* PIMERecItem;

class TIMEHelper {
public:
    TIMEHelper();
    ~TIMEHelper();
public:
    static int IMEEnumer(IMENUMINFO* pIMInfo);
private:
    static PIMERecItem fIMEInfos;
    static int fIMECount, fEnumingIndex, fCurrentIMEIndex;
public:
    PIMERecItem GetIMEInfos() const { return fIMEInfos; }
    int GetIMECount() const { return fIMECount; }
    int GetCurrentIMEIndex() const { return fCurrentIMEIndex; }
};

typedef TIMEHelper* PIMEHelper;

extern PIMEHelper imehelper;

#endif
