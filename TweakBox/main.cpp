#include <mzfc_inc.h>
#include "MainUnt.h"
#include "StringInterner.h"
#include "IMEHelper.h"

// Application class derived from CMzApp
class CBoxApp: public CMzApp
{
public:
    CBoxApp() {
        //stringstore = new TStringStore;
    }
    ~CBoxApp() {
        //delete stringstore;
    }
public:
    // The main window of the app.
    CMainWnd m_MainWnd;

    // Initialization of the application
    virtual BOOL Init()
    {
        // Init the COM relative library.
        CoInitializeEx(0, COINIT_MULTITHREADED);

        // Create the main window
        RECT rcWork = MzGetWorkArea();
        m_MainWnd.Create(rcWork.left,rcWork.top,RECT_WIDTH(rcWork),RECT_HEIGHT(rcWork), 0, 0, 0);
        m_MainWnd.SetBgColor(MzGetThemeColor(TCI_WINDOW_BG));
        m_MainWnd.Show();

        // return TRUE means init success.
        return TRUE;
    }
};

// The global variable of the application.
CBoxApp theApp;
