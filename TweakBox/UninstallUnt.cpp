#include <Mzfc\MzProgressDialog.h>
#include <Mzfc\FontHelper.h>
#include "..\FvMzMd\FvMzKOL.h"
#include "UninstallUnt.h"
#include "StringInterner.h"

#define MZ_IDC_TOOLBAR1  101

#define IDC_PPM_OK      102
#define IDC_PPM_CANCEL  103

#define MZ_IDC_SCROLLWIN    104
#define MZ_IDC_UNLOCK       105
#define MZ_IDC_WIFI         106
#define MZ_IDC_HACKMODE     107
#define MZ_IDC_LIST         108
#define MZ_IDC_IMECOMBO     109

void LiteList::DrawItem( HDC hdcDst, int nIndex, RECT* prcItem, RECT *prcWin, RECT *prcUpdate )
{
    // draw the high-light background for the selected item
    if (nIndex == GetSelectedIndex())
    {
        MzDrawSelectedBg(hdcDst, prcItem);
    }

    ListItem* pItem = GetItem(nIndex);
    if (pItem) { // infact safe
       // draw an image on the left
        ImagingHelper *pimg = ImagingHelper::GetImageObject(MzGetInstanceHandle(),
            (PInstallionRecItem(pItem->Data)->Checked?IDR_PNG_CHECKON:IDR_PNG_CHECKOFF), true);
        RECT rcImg = *prcItem;
        rcImg.right = rcImg.left + MZM_MARGIN_MAX * 2;
        if (pimg)
        {
            pimg->Draw(hdcDst, &rcImg, false, false);
        }

        // draw the text
        RECT rcText = *prcItem;
        rcText.left = rcImg.right;

        if (nIndex == GetSelectedIndex()) {
            rcText.bottom -= 18;
            MzDrawText(hdcDst, pItem->Text.C_Str(), &rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
            rcText = *prcItem;
            rcText.top = rcText.bottom - 18;
            rcText.left = rcImg.right;
            rcText.right -= 4;
            HFONT oldFont = HFONT(SelectObject(hdcDst, HGDIOBJ(FontHelper::GetFont(14))));
            MzDrawText(hdcDst, PInstallionRecItem(pItem->Data)->CabFile.C_Str(), &rcText, DT_RIGHT | DT_TOP | DT_SINGLELINE | DT_END_ELLIPSIS);
            //DeleteObject(SelectObject(hdcDst, oldFont));
            SelectObject(hdcDst, HGDIOBJ(oldFont));
        } else {
            MzDrawText(hdcDst, pItem->Text.C_Str(), &rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
        }
    }
}

// Header n unit
MZ_IMPLEMENT_DYNAMIC(UninstallFrm)

LRESULT UninstallFrm::MzDefWndProc( UINT message, WPARAM wParam, LPARAM lParam )
{
    switch(message)
    {
    case MZ_WM_MOUSE_NOTIFY:
        {
            int nID = LOWORD(wParam);
            int nNotify = HIWORD(wParam);
            int x = LOWORD(lParam);
            int y = HIWORD(lParam);

            // process the mouse left button down notification
            if (nID==MZ_IDC_LIST && nNotify==MZ_MN_LBUTTONDOWN)
            {
                if (!m_List.IsMouseDownAtScrolling() && !m_List.IsMouseMoved())
                {
                    int nIndex = m_List.CalcIndexOfPos(x, y);
                    if (nIndex != -1) {
                        m_List.SetSelectedIndex(nIndex);
                        if ((x >= MZM_MARGIN_MAX - 24 - 4) && (x <= MZM_MARGIN_MAX + 24 + 4)) {
                            ListItem* item = m_List.GetItem(nIndex);
                            PInstallionRecItem(item->Data)->Checked = !PInstallionRecItem(item->Data)->Checked;
                        }
                        m_List.Invalidate();
                        m_List.Update();
                    }
                }
                return 0;
            }
        }
        return 0;
    }
    return CMzWndEx::MzDefWndProc(message,wParam,lParam);
}

void UninstallFrm::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex==0)
            {
                // rollback
                EndModal(ID_OK);
                return;
            }

            if (nIndex==2)
            {
                // remove selected cab installion
                // EndModal(ID_CANCEL);
                int pendingchanges = 0;
                for (list <PInstallionRecItem>::iterator it = fInstallionList.begin(); it != fInstallionList.end(); it++) {
                    if ((*it)->Checked) {
                        pendingchanges++;
                    }
                }
                if (pendingchanges == 0) {
                    MzMessageBoxEx(m_hWnd, _tr(IDS_STRING_NOTHINGTOUNINSTALL), _tr(IDS_STRING_HINT), MZ_OK, false);
                    return;
                }
                CMzStringW HintMessage(64);
                _snwprintf(HintMessage.C_Str(), 64, _tr(IDS_STRING_REMOVECONFIRM), pendingchanges);
                if (MzMessageBoxEx(m_hWnd, HintMessage, _tr(IDS_STRING_HINT), MZ_YESNO, false) == IDOK) {
                    MzProgressDialog dlg;
                    dlg.BeginProgress(m_hWnd);
                    dlg.SetRange(0, pendingchanges);
                    for (list <PInstallionRecItem>::iterator it = fInstallionList.begin(); it != fInstallionList.end(); it++) {
                        if ((*it)->Checked) {
                            // unload
                            dlg.SetInfo((*it)->AppName);
                            dlg.UpdateProgress();
                            DWORD Flags = CREATE_NEW_CONSOLE;
                            /*STARTUPINFO Startup;
                            Startup.cb = sizeof STARTUPINFO;
                            Startup.wShowWindow = SW_SHOWDEFAULT;
                            Startup.dwFlags = STARTF_USESHOWWINDOW;*/
                            PROCESS_INFORMATION ProcInf;	
                            CMzStringW unloadfile = (*it)->AppName;//CMzStringW(L"\"\\Windows\\") + (*it)->AppName + L".unload\"";
                            if (CreateProcess(L"\\Windows\\unload.exe", unloadfile.C_Str(), NULL, NULL, FALSE, Flags, NULL, NULL, NULL, &ProcInf) == TRUE) {
                                //
                                if (WaitForSingleObject( ProcInf.hProcess, INFINITE ) == WAIT_OBJECT_0) {
                                    CloseHandle( ProcInf.hProcess );
                                    dlg.SetCurValue(dlg.GetCurValue() + 1);
                                    dlg.UpdateProgress();
                                } else {
                                    //ProcID^ := ProcInf.hProcess;
                                }
                                CloseHandle( ProcInf.hThread );
                            }
                        }
                    }
                    dlg.EndProgress();
                    UpdateAppList();
                }

                return;
            }
        }
    }
}

BOOL UninstallFrm::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }

    // Then init the controls & other things in the window
    int y = 0;
    y += MZM_HEIGHT_CAPTION;

    m_List.SetPos(0,0,GetWidth(),GetHeight()-MZM_HEIGHT_TEXT_TOOLBAR);
    m_List.SetID(MZ_IDC_LIST);
    m_List.EnableScrollBarV(true);
    m_List.EnableNotifyMessage(true);
    m_List.SetItemHeight(MZM_HEIGHT_BUTTONEX);

    //add items to list
    UpdateAppList();


    AddUiWin(&m_List);
    //m_List.SetSelectedIndex(*fResult);

    m_Toolbar.SetPos(0,GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR, GetWidth(), MZM_HEIGHT_TEXT_TOOLBAR);
    m_Toolbar.SetButton(0, true, true, _tr(IDS_STRING_RETURN));
    m_Toolbar.EnableLeftArrow(true);
    m_Toolbar.SetButton(2, true, true, _tr(IDS_STRING_UNINSTALL));
    m_Toolbar.SetID(MZ_IDC_TOOLBAR1);
    AddUiWin(&m_Toolbar);

    return TRUE;
}

void UninstallFrm::UpdateAppList()
{
    ListItem item;
    PInstallionRecItem institem;
    m_List.RemoveAll();
    fInstallionList.clear();

    HKEY AppsKey = RegKeyOpenRead(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Apps");
    TKOLStrList AppsList;
    RegKeyGetSubKeys(AppsKey, &AppsList);

    for (TKOLStrList::iterator it = AppsList.begin(); it != AppsList.end(); it++) {
        HKEY ItemKey = RegKeyOpenRead(AppsKey, *it);
        if (RegKeyValExists(ItemKey, L"Instl") && RegKeyValExists(ItemKey, L"InstlDir")) {
            institem = new tagInstallionRecItem;
            institem->Checked = false;
            institem->AppName = *it;
            institem->CabFile = RegKeyGetStr(ItemKey, L"CabFile");
            fInstallionList.push_back(institem);

            item.Text = *it;
            item.Data = institem;
            m_List.AddItem(item);
        }
        RegKeyClose(ItemKey);
    }
    RegKeyClose(AppsKey);
}

UninstallFrm::~UninstallFrm()
{
    for (list <PInstallionRecItem>::iterator it = fInstallionList.begin(); it != fInstallionList.end(); it++) {
        delete *it;
    }
}

