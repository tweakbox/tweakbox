﻿#include "IMEHelper.h"
#include "StringInterner.h"
#include "resource.h"

PIMEHelper imehelper; // imp

PIMERecItem TIMEHelper::fIMEInfos;
int TIMEHelper::fIMECount, TIMEHelper::fEnumingIndex, TIMEHelper::fCurrentIMEIndex;

TIMEHelper::TIMEHelper()
{
    int IMECount = SipEnumIM(IMENUMPROC(NULL));
    if (IMECount > 0) {
        fIMEInfos = new tagIMERecItem[IMECount];
        fIMECount = IMECount;
        fEnumingIndex = 0;
        SipEnumIM(IMEEnumer);
        // GetCurrentIME Index/ClassID
        CLSID DefaultIMEClassID;
        if (SipGetCurrentIM(&DefaultIMEClassID)) {
            for (int i = 0; i < IMECount; i++)
            {
                if (fIMEInfos[i].ClassID == DefaultIMEClassID) {
                    //
                    fCurrentIMEIndex = i;
                    break;
                }
            }
        } else {
            fCurrentIMEIndex = -1;
        }

    } else {
        fIMEInfos = NULL;
    }
}

TIMEHelper::~TIMEHelper()
{
    if (fIMEInfos != NULL) {
        delete fIMEInfos;
    }
}

int TIMEHelper::IMEEnumer( IMENUMINFO* pIMInfo )
{
    fIMEInfos[fEnumingIndex].DisplayName = pIMInfo->szName;
    fIMEInfos[fEnumingIndex].ClassID = pIMInfo->clsid;

    if (fIMEInfos[fEnumingIndex].DisplayName.Compare(L"A4?") == 0) {
        fIMEInfos[fEnumingIndex].DisplayName = _tr(IDS_STRING_A4IM);
    }
    if (fIMEInfos[fEnumingIndex].DisplayName.Compare(L"????") == 0) {
        fIMEInfos[fEnumingIndex].DisplayName = _tr(IDS_STRING_DAYHANDIM);
    }

    fEnumingIndex++;
    return (fEnumingIndex >= fIMECount? 0: 1);
}