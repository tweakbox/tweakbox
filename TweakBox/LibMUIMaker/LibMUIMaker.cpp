


////////////////////////////////////////////////////////////////////////////////////////////////////
// file: LibMUIMaker.cpp
// date: February 09 2009
// font: consolas,9pt
// 
// Interface for MUI Maker
////////////////////////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////////////////////////
#include "LibMUIMaker.h"

using namespace MakerBinarayInterface;


CMUIMaker::CMUIMaker()
{
	fMaker = NewMUIMaker2();
	if (fMaker == NULL) {
		delete this;
	}
}

CMUIMaker::CMUIMaker( WCHAR* filename )
{
	/*fMaker = NewMUIMaker2(filename);
	if (fMaker == NULL) {
		delete this;
	}*/
}

CMUIMaker::~CMUIMaker()
{
	FreeMUIMaker(fMaker);
}

bool CMUIMaker::InitString( WCHAR* content )
{
	return MakerDoInitString(fMaker, content) == TRUE;
}

bool CMUIMaker::SaveToFile( WCHAR* filename, BOOL* mayuseconsole )
{
	return MakerDoSaveToFile(fMaker, filename, mayuseconsole) == TRUE;
}

bool CMUIMaker::RegisterString( INT32 stringid, WCHAR* content )
{
    return MakerDoRegisterString(fMaker, stringid, content) == TRUE;
}