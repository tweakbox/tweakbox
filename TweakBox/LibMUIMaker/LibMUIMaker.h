


////////////////////////////////////////////////////////////////////////////////////////////////////
// file: LibMUIMaker.cpp
// date: February 09 2009
// font: consolas,9pt
// 
// Interface defination for MUI Maker
////////////////////////////////////////////////////////////////////////////////////////////////////


#ifndef _LIB_MUIMAKER
#define _LIB_MUIMAKER


////////////////////////////////////////////////////////////////////////////////////////////////////
#include <Windows.h>



#pragma comment( lib, "LibMUIMaker.lib" )

namespace MakerBinarayInterface {
// Create an director worker for down sample
extern "C" LPINT  WINAPI NewMUIMaker2( void );

// Free the blue skin
extern "C" BOOL   WINAPI FreeMUIMaker(LPINT maker);

// insert 
extern "C" BOOL   WINAPI MakerDoRegisterString(LPINT maker, INT32 stringid, WCHAR* content);

// init
extern "C" BOOL   WINAPI MakerDoInitString(LPINT maker, WCHAR* section);

// save
extern "C" BOOL   WINAPI MakerDoSaveToFile(LPINT maker, WCHAR* filename, BOOL* mayuseconsole);

}

// omake
extern "C" BOOL   WINAPI UtilNukeFile( WCHAR* filename, BOOL* mayuseconsole);

class CMUIMaker
{
public:
	CMUIMaker();
	CMUIMaker(WCHAR* filename);
	~CMUIMaker();
public:
	bool InitString(WCHAR* content);
    bool RegisterString(INT32 stringid, WCHAR* content);
	bool SaveToFile(WCHAR* filename, BOOL* mayuseconsole = NULL);
private:
	LPINT fMaker;
};


#endif // _LIB_LANCZOSDRIVER