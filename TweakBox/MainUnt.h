#ifndef _MAIN_UNIT
#define _MAIN_UNIT

#include <mzfc_inc.h>
#include "SettingsUnt.h"
#include "DirtyDefination.h"

// Main window derived from CMzWndEx
class CMainWnd: public CMzWndEx
{
    MZ_DECLARE_DYNAMIC(CMainWnd);
private:
    bool fOldEcchiUnlock, fEcchiUnlock, fOldFunkkyWifi, fFunkkyWifi;
    CMzString fOldOwnerName, fOwnerName;
    int fNewIMEIndex, fNewModeIndex;
    int ClickyCount;
    list < tagEditRecItem > fQuestionableUnlockWords; // muhahaha
    CMzStringW UnlockFieldNames[UnlockFields]; /* = {
        L"����", L"����", L"�ܾ�", L"����", L"��һ��", L"��һ��", L"��ͣ", L"����"
    }; */
    CMzStringW ModeNames[Modes]; /*= {
        L"Heavy Q&&D", L"Lightly 631", L"Verbose Missable"
    }; */
    HMODULE SateHandle;
public:
    // A button control in the window
    UiButton m_btn;
    // text toolbar control
    UiToolbar_Text m_Toolbar;
    // Demo
    UiStatic m_Label1, m_Label2, m_Label3;
    UiSingleLineEdit m_NameEdt;
    UiButtonEx m_BtnSetting1, m_BtnSetting2, m_BtnSetting3, m_BtnSetting4, m_BtnSetting5;
    UiButtonEx  m_EcchiUnlockSwitch, m_FunkkyWifiSwitch;
    UiScrollWin m_ScrollWin;
    UiPicture m_Banner;
    //UiButton m_CustomEcchiUnlockBtn;

protected:
    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);

    bool ApplyChanges(bool Silent);

public:
    CMainWnd();
    ~CMainWnd();
};
CMzStringW GetMUICode();

#endif