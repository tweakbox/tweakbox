#include "SwitcherUnt.h"
#include "IMEHelper.h"
#include "StringInterner.h"

void MyList::DrawItem( HDC hdcDst, int nIndex, RECT* prcItem, RECT *prcWin, RECT *prcUpdate )
{
    // draw the high-light background for the selected item
    if (nIndex == GetSelectedIndex())
    {
        MzDrawSelectedBg(hdcDst, prcItem);
    }

    // draw an image on the left
    ImagingHelper *pimg = ImagingHelper::GetImageObject(GetMzResModuleHandle(), (nIndex == GetSelectedIndex()?MZRES_IDR_PNG_SELECTED:MZRES_IDR_PNG_SELECT_BOX), true);
    RECT rcImg = *prcItem;
    rcImg.right = rcImg.left + MZM_MARGIN_MAX * 2;
    if (pimg)
    {
        pimg->Draw(hdcDst, &rcImg, false, false);
    }

    // draw the text
    RECT rcText = *prcItem;
    rcText.left = rcImg.right;
    ListItem* pItem = GetItem(nIndex);
    if (pItem)
    {
        MzDrawText(hdcDst, pItem->Text.C_Str(), &rcText, DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_END_ELLIPSIS);
    }
}

MZ_IMPLEMENT_DYNAMIC(HackModeWnd)

BOOL HackModeWnd::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }

    // Then init the controls & other things in the window
    int y = 0;
    y += MZM_HEIGHT_CAPTION;

    m_List.SetPos(0,0,GetWidth(),GetHeight()-MZM_HEIGHT_TEXT_TOOLBAR);
    m_List.SetID(MZ_IDC_LIST);
    m_List.EnableScrollBarV(true);
    m_List.EnableNotifyMessage(true);
    m_List.SetItemHeight(MZM_HEIGHT_BUTTONEX);
    m_List.SetSelectedIndex(0);

    //add items to list
    ListItem item;

    /*for (int i = 0; i < Modes; i++) {
        item.Text = ModeNames[i];
        m_List.AddItem(item);
    }*/
    if (fModeList != NULL) {
        for (TKOLStrList::iterator it = fModeList->begin(); it != fModeList->end(); it++) {
            item.Text = *it;
            m_List.AddItem(item);
        }
    }

    m_List.SetSelectedIndex(*fResult);
    AddUiWin(&m_List);

    m_Toolbar.SetPos(0,GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR,GetWidth(),MZM_HEIGHT_TEXT_TOOLBAR);
    m_Toolbar.SetButton(0, true, true, _tr(IDS_STRING_RETURN));
    m_Toolbar.EnableLeftArrow(true);
    m_Toolbar.SetButton(2, true, true, _tr(IDS_STRING_CANCEL));
    m_Toolbar.SetID(MZ_IDC_TOOLBAR1);
    AddUiWin(&m_Toolbar);

    return TRUE;
}

void HackModeWnd::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex==2)
            {
                // exit the modal dialog
                EndModal(ID_CANCEL);
                return;
            }

            if (nIndex==0)
            {
                //saving settings...
                *fResult = m_List.GetSelectedIndex();
                //then exit the dialog
                EndModal(ID_OK);
                return;
            }
        }
    }
}

LRESULT HackModeWnd::MzDefWndProc( UINT message, WPARAM wParam, LPARAM lParam )
{
    switch(message)
    {
    case MZ_WM_MOUSE_NOTIFY:
        {
            int nID = LOWORD(wParam);
            int nNotify = HIWORD(wParam);
            int x = LOWORD(lParam);
            int y = HIWORD(lParam);

            // process the mouse left button down notification
            if (nID==MZ_IDC_LIST && nNotify==MZ_MN_LBUTTONDOWN)
            {
                if (!m_List.IsMouseDownAtScrolling() && !m_List.IsMouseMoved())
                {
                    int nIndex = m_List.CalcIndexOfPos(x, y);
                    if (nIndex != -1) {
                        m_List.SetSelectedIndex(nIndex);
                        m_List.Invalidate();
                        m_List.Update();
                    }
                }
                return 0;
            }
        }
        return 0;
    }
    return CMzWndEx::MzDefWndProc(message,wParam,lParam);
}

MZ_IMPLEMENT_DYNAMIC(IMEEnumWnd)

BOOL IMEEnumWnd::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }

    // Then init the controls & other things in the window
    int y = 0;
    y += MZM_HEIGHT_CAPTION;

    m_List.SetPos(0,0,GetWidth(),GetHeight()-MZM_HEIGHT_TEXT_TOOLBAR);
    m_List.SetID(MZ_IDC_LIST);
    m_List.EnableScrollBarV(true);
    m_List.EnableNotifyMessage(true);
    m_List.SetItemHeight(MZM_HEIGHT_BUTTONEX);

    //add items to list
    ListItem item;

    for (int i = 0; i < imehelper->GetIMECount(); i++) {
        item.Text = imehelper->GetIMEInfos()[i].DisplayName;
        m_List.AddItem(item);
    }
    m_List.SetSelectedIndex(*fResult);
    AddUiWin(&m_List);

    m_Toolbar.SetPos(0,GetHeight()-MZM_HEIGHT_TEXT_TOOLBAR,GetWidth(),MZM_HEIGHT_TEXT_TOOLBAR);
    m_Toolbar.SetButton(0, true, true, _tr(IDS_STRING_RETURN));
    m_Toolbar.EnableLeftArrow(true);
    m_Toolbar.SetButton(2, true, true, _tr(IDS_STRING_CANCEL));
    m_Toolbar.SetID(MZ_IDC_TOOLBAR1);
    AddUiWin(&m_Toolbar);

    return TRUE;
}

void IMEEnumWnd::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex==2)
            {
                // exit the modal dialog
                EndModal(ID_CANCEL);
                return;
            }

            if (nIndex==0)
            {
                //saving settings...
                *fResult = m_List.GetSelectedIndex();
                //then exit the dialog
                EndModal(ID_OK);
                return;
            }
        }
    }
}

LRESULT IMEEnumWnd::MzDefWndProc( UINT message, WPARAM wParam, LPARAM lParam )
{
    switch(message)
    {
    case MZ_WM_MOUSE_NOTIFY:
        {
            int nID = LOWORD(wParam);
            int nNotify = HIWORD(wParam);
            int x = LOWORD(lParam);
            int y = HIWORD(lParam);

            // process the mouse left button down notification
            if (nID==MZ_IDC_LIST && nNotify==MZ_MN_LBUTTONDOWN)
            {
                if (!m_List.IsMouseDownAtScrolling() && !m_List.IsMouseMoved())
                {
                    int nIndex = m_List.CalcIndexOfPos(x, y);
                    if (nIndex != -1) {
                        m_List.SetSelectedIndex(nIndex);
                        m_List.Invalidate();
                        m_List.Update();
                    }
                }
                return 0;
            }
        }
        return 0;
    }
    return CMzWndEx::MzDefWndProc(message,wParam,lParam);
}
