#ifndef _SWITCHER_UNIT
#define _SWITCHER_UNIT

#include <mzfc_inc.h>
#include "../FvMzMd/FvMzKOL.h"
#include "DirtyDefination.h"
#include "resource.h"

// A new list control derived from UiList
class MyList:
    public UiList
{
public:
    // override the DrawItem member function to do your own drawing of the list
    void DrawItem(HDC hdcDst, int nIndex, RECT* prcItem, RECT *prcWin, RECT *prcUpdate);
protected:
private:
};

// Popup window derived from CMzWndEx
class HackModeWnd:
    public CMzWndEx
{
    MZ_DECLARE_DYNAMIC(HackModeWnd);
private:
    int* fResult;
    PKOLStrList fModeList;
public:
    HackModeWnd(int* result, PKOLStrList modelist): fResult(result), fModeList(modelist) { };
protected:
    UiToolbar_Text m_Toolbar;
    MyList m_List;

    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    // override the MZFC window messages handler
    LRESULT MzDefWndProc(UINT message, WPARAM wParam, LPARAM lParam);

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);
private:
};

// Popup window derived from CMzWndEx
class IMEEnumWnd:
    public CMzWndEx
{
    MZ_DECLARE_DYNAMIC(IMEEnumWnd);
private:
    int* fResult;
public:
    IMEEnumWnd(int* result): fResult(result) { };
protected:
    UiToolbar_Text m_Toolbar;
    MyList m_List;

    // Initialization of the window (dialog)
    virtual BOOL OnInitDialog();

    // override the MZFC window messages handler
    LRESULT MzDefWndProc(UINT message, WPARAM wParam, LPARAM lParam);

    // override the MZFC command handler
    virtual void OnMzCommand(WPARAM wParam, LPARAM lParam);
private:
};

#endif