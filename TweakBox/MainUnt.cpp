﻿
#include <mzfc_inc.h>
#include <ShellNotifyMsg.h>
#include <MotorVibrate.h>
#include "..\FvMzMd\FvMzKOL.h"
#include "MainUnt.h"
#include "IMEHelper.h"
#include "StringInterner.h"
#include "LibMUIMaker\LibMUIMaker.h"
#include "SwitcherUnt.h"
#include "UninstallUnt.h"
#include "SettingsUnt.h"

#include "DirtyDefination.h"
#include "resource.h"

/*CMzStringW ModeNames[Modes] = {
    L"Heavy Q&&D", L"Lightly 631", L"Verbose Missable"
}; // TODO: move to MainFrm member*/

int UnlockFieldIDs[UnlockFields] = {
    611, 617, 618, 619, 620, 621, 622, 623
};


MZ_IMPLEMENT_DYNAMIC(CMainWnd)

BOOL CMainWnd::OnInitDialog()
{
    // Must all the Init of parent class first!
    if (!CMzWndEx::OnInitDialog())
    {
        return FALSE;
    }

    // Then init the controls & other things in the window

    m_ScrollWin.SetPos(0, 0, GetWidth(), GetHeight() - MZM_HEIGHT_TEXT_TOOLBAR);
    m_ScrollWin.SetID(MZ_IDC_SCROLLWIN);
    m_ScrollWin.EnableScrollBarV(true);
    AddUiWin(&m_ScrollWin);

    int top = 0;
    int left = 24;

    m_Banner.LoadImage(MzGetInstanceHandle(), RT_RCDATA, MAKEINTRESOURCE(IDR_PNG_BANNER), false);
    m_Banner.SetPos(0, 0, GetWidth(), 200);
    m_ScrollWin.AddChild(&m_Banner);
    top += m_Banner.GetHeight();

    top += 8;
    m_NameEdt.SetPos(left, top, GetWidth() - left * 2, MZM_HEIGHT_SINGLELINE_EDIT);
    m_NameEdt.SetText(fOldOwnerName);
    m_NameEdt.SetMaxChars(10); // in brief UI
    m_NameEdt.SetTip2(_tr(IDS_STRING_OWNERNAME));
    m_NameEdt.SetLeftInvalid(Str2Int(_tr(IDS_STRING_OWNERNAMEWIDTH)));
    //m_NameEdt.SetRightInvalid(130); // Encapsulate
    m_NameEdt.Update();
    m_ScrollWin.AddChild(&m_NameEdt);
    top += MZM_HEIGHT_SINGLELINE_EDIT;

#ifdef DEBUG
    m_BtnSetting1.SetPos(0, top, GetWidth(), MZM_HEIGHT_BUTTONEX);
    m_BtnSetting1.SetText(_tr(IDS_STRING_HACKSTYLE));
    m_BtnSetting1.SetText2(ModeNames[fNewModeIndex]);
    m_BtnSetting1.SetID(MZ_IDC_HACKMODE);
    m_BtnSetting1.SetButtonType(MZC_BUTTON_LINE_BOTTOM);
    m_ScrollWin.AddChild(&m_BtnSetting1);
    top += MZM_HEIGHT_BUTTONEX;
#endif

    ImagingHelper *imgArrow = ImagingHelper::GetImageObject(GetMzResModuleHandle(), MZRES_IDR_PNG_ARROW_RIGHT, true);
    m_BtnSetting1.SetImage2(imgArrow);
    m_BtnSetting1.SetImageWidth2(imgArrow->GetImageWidth());
    m_BtnSetting1.SetShowImage2(true);


    //m_Label2.SetPos(left, 0, GetWidth() - 180, MZM_HEIGHT_BUTTONEX);
    //m_Label2.SetID(MZ_IDC_UNLOCK_ROW);

    //m_Label2.SetText(L"使用令人害羞的解锁提示");
    //m_Label2.SetDrawTextFormat(DT_LEFT | DT_VCENTER);

    m_EcchiUnlockSwitch.SetPos(GetWidth() - 156, 0, 156, MZM_HEIGHT_BUTTONEX);
    m_EcchiUnlockSwitch.SetID(MZ_IDC_UNLOCK);
    m_EcchiUnlockSwitch.SetButtonMode(MZC_BUTTON_MODE_HOLD);
    m_EcchiUnlockSwitch.SetButtonType(MZC_BUTTON_SWITCH);
    if (fOldEcchiUnlock) {
        m_EcchiUnlockSwitch.SetState(MZCS_BUTTON_PRESSED);
    }

    /*m_CustomEcchiUnlockBtn.SetPos(GetWidth() - 156, 0, 156, MZM_HEIGHT_BUTTONEX);
    m_CustomEcchiUnlockBtn.SetID(MZ_IDC_UNLOCK_NEXT);
    m_CustomEcchiUnlockBtn.SetVisible(false);*/

    m_BtnSetting2.SetPos(0, top, GetWidth(), MZM_HEIGHT_BUTTONEX);
    m_BtnSetting2.SetText(_tr(IDS_STRING_ECCHIUNLOCK));
    m_BtnSetting2.SetTextSize(24);
    m_BtnSetting2.SetTextMaxLen(0);
    m_BtnSetting2.SetID(MZ_IDC_UNLOCK_ROW);
    m_FunkkyWifiSwitch.SetButtonMode(MZC_BUTTON_MODE_HOLD);
    m_BtnSetting2.SetButtonType(MZC_BUTTON_LINE_BOTTOM);
    //m_BtnSetting2.AddChild(&m_Label2);
    m_BtnSetting2.AddChild(&m_EcchiUnlockSwitch);
    //m_BtnSetting2.AddChild(&m_CustomEcchiUnlockBtn);
    m_ScrollWin.AddChild(&m_BtnSetting2);
    top += MZM_HEIGHT_BUTTONEX;

#ifdef DEBUG
    m_Label3.SetPos(left, 0, GetWidth() - 180, MZM_HEIGHT_BUTTONEX);
    m_Label3.SetText(_tr(IDS_STRING_FUNKKYWAPI));
    m_Label3.SetDrawTextFormat(DT_LEFT | DT_VCENTER);
    m_ScrollWin.AddChild(&m_Label3);

    m_FunkkyWifiSwitch.SetPos(GetWidth() - 156, 0, 156, MZM_HEIGHT_BUTTONEX);
    m_FunkkyWifiSwitch.SetID(MZ_IDC_WIFI);
    m_FunkkyWifiSwitch.SetButtonMode(MZC_BUTTON_MODE_HOLD);
    m_FunkkyWifiSwitch.SetButtonType(MZC_BUTTON_SWITCH);
    m_ScrollWin.AddChild(&m_FunkkyWifiSwitch);
    if (fOldFunkkyWifi) {
        m_FunkkyWifiSwitch.SetState(MZCS_BUTTON_PRESSED);
    }

    m_BtnSetting3.SetPos(0, top, GetWidth(), MZM_HEIGHT_BUTTONEX);
    m_BtnSetting3.SetButtonType(MZC_BUTTON_LINE_BOTTOM);
    m_BtnSetting3.AddChild(&m_Label3);
    m_BtnSetting3.AddChild(&m_FunkkyWifiSwitch);
    m_ScrollWin.AddChild(&m_BtnSetting3);
    top += MZM_HEIGHT_BUTTONEX;
#endif

    m_BtnSetting4.SetPos(0, top, GetWidth(), MZM_HEIGHT_BUTTONEX);
    m_BtnSetting4.SetText(_tr(IDS_STRING_CURRENTIM));
    if (fNewIMEIndex != -1) {
        m_BtnSetting4.SetText2(imehelper->GetIMEInfos()[fNewIMEIndex].DisplayName);
    }
    m_BtnSetting4.SetID(MZ_IDC_IMECOMBO);
    m_BtnSetting4.SetButtonType(MZC_BUTTON_LINE_BOTTOM);
    m_ScrollWin.AddChild(&m_BtnSetting4);
    top += MZM_HEIGHT_BUTTONEX;

    imgArrow = ImagingHelper::GetImageObject(GetMzResModuleHandle(), MZRES_IDR_PNG_ARROW_RIGHT, true);
    m_BtnSetting4.SetImage2(imgArrow);
    m_BtnSetting4.SetImageWidth2(imgArrow->GetImageWidth());
    m_BtnSetting4.SetShowImage2(true);

    m_BtnSetting5.SetPos(0, top, GetWidth(), MZM_HEIGHT_BUTTONEX);
    m_BtnSetting5.SetText(_tr(IDS_STRING_ADDORREMOVE));
    m_BtnSetting5.SetTextMaxLen(0);
    m_BtnSetting5.SetID(MZ_IDC_UNINSTALL);
    m_BtnSetting5.SetButtonType(MZC_BUTTON_LINE_BOTTOM);
    m_ScrollWin.AddChild(&m_BtnSetting5);
    top += MZM_HEIGHT_BUTTONEX;

    imgArrow = ImagingHelper::GetImageObject(GetMzResModuleHandle(), MZRES_IDR_PNG_ARROW_RIGHT, true);
    m_BtnSetting5.SetImage2(imgArrow);
    m_BtnSetting5.SetImageWidth2(imgArrow->GetImageWidth());
    m_BtnSetting5.SetShowImage2(true);


    // Init the toolbar and add it into the window
    m_Toolbar.SetPos(0,GetHeight()-MZM_HEIGHT_TEXT_TOOLBAR,GetWidth(),MZM_HEIGHT_TEXT_TOOLBAR);
    m_Toolbar.SetButton(0, true, true, _tr(IDS_STRING_EXIT));
    m_Toolbar.SetButton(2, true, true, _tr(IDS_STRING_APPLY));
    //m_Toolbar.SetButton(2, true, true, L"Hide button");
    m_Toolbar.SetID(MZ_IDC_TOOLBAR1);
    AddUiWin(&m_Toolbar);

    return TRUE;
}

void CMainWnd::OnMzCommand( WPARAM wParam, LPARAM lParam )
{
    UINT_PTR id = LOWORD(wParam);
    switch(id)
    {
    case MZ_IDC_TOOLBAR1:
        {
            int nIndex = lParam;
            if (nIndex==0)
            {
                // pop out a PopupMenu:
                ApplyChanges(false);
                PostQuitMessage(0);
                return;
            }
            if (nIndex==2)
            {
                ApplyChanges(true);
                return;
            }
            if (nIndex==1)
            {
                /*m_btn.SetVisible(!m_btn.IsVisible());
                m_btn.Invalidate();
                m_btn.Update();
                if (m_btn.IsVisible())
                m_Toolbar.SetButtonText(2, L"Hide button");
                else
                m_Toolbar.SetButtonText(2, L"Show button");

                m_Toolbar.Invalidate();
                m_Toolbar.Update();*/
                return;
            }
        }
        break;
    case MZ_IDC_UNLOCK:
        {
            fEcchiUnlock = m_EcchiUnlockSwitch.GetState() == MZCS_BUTTON_PRESSED;
        }
        break;
    case MZ_IDC_WIFI:
        {
            fFunkkyWifi = m_FunkkyWifiSwitch.GetState() == MZCS_BUTTON_PRESSED;
        }
        break;
    case MZ_IDC_UNLOCK_ROW:
        {
            //m_CustomEcchiUnlockBtn.SetVisible(!m_CustomEcchiUnlockBtn.IsVisible());
            //m_EcchiUnlockSwitch.SetVisible(!m_EcchiUnlockSwitch.IsVisible());
            ClickyCount++;
            if (ClickyCount >= 2) {
                MzSetVibrateOn(200, 1200);
            }
            if (ClickyCount >= 3) {
                MzSetVibrateOff();
                ClickyCount = 0;
                // open next
                if (fQuestionableUnlockWords.empty()) {
                    HINSTANCE shellinst = NULL;
#ifdef SHINUI
                    CMzStringW semimuifilename = CMzStringW(L"\\Windows\\MzStrRes.") + GetMUICode() + L".mui";
#else
                    CMzStringW semimuifilename = CMzStringW(L"\\Windows\\MiniOneShell.EXE.") + GetMUICode() + L".mui";
#endif
                    if (GetUserDefaultUILanguage() == 0x0804) {
                        if (FileExists(semimuifilename)) {
                            shellinst = LoadLibraryExW(semimuifilename, NULL, LOAD_LIBRARY_AS_DATAFILE);
                        } else {
#ifndef SHINUI
                            shellinst = LoadLibraryExW(L"\\Windows\\MiniOneShell.EXE", NULL, LOAD_LIBRARY_AS_DATAFILE);
#endif
                            // else shellinst keep NULL n popup an error
                        }
                    } else {
                        shellinst = LoadLibraryExW(semimuifilename, NULL, LOAD_LIBRARY_AS_DATAFILE);
                    }

                    if (shellinst == NULL) {
                        DWORD errcode = GetLastError();
                        DEBUGMSG(true, (L"LoadLibraryW Failed with %d errorcode.\n", errcode));
                    }
                    CMzStringW nametmp;
                    tagEditRecItem item;
                    for (int i = 0; i < _countof(UnlockFieldIDs); i++) {
                        nametmp = LoadStr(shellinst, UnlockFieldIDs[i]);
                            item.FieldID = UnlockFieldIDs[i];
                            item.FieldName = UnlockFieldNames[i];
                        if (nametmp.IsEmpty() == false) {
                            item.Value = nametmp;
                            fQuestionableUnlockWords.push_back(item); // copy
                        } else {
                            item.Value = L"默认文字查询失败.";
                        }
                        //fQuestionableUnlockWords.push_back(item); // copy
                    }
                    FreeLibrary(shellinst);
                }
                if (fQuestionableUnlockWords.empty()) {
                    ShowMessage(_tr(IDS_STRING_UNLOCKINACTIVE), m_hWnd);
                    return;
                }
                GroupEdtFrm dlg(&fQuestionableUnlockWords);
                RECT rcWork = MzGetWorkArea();
                dlg.Create(rcWork.left, rcWork.top, RECT_WIDTH(rcWork),RECT_HEIGHT(rcWork), m_hWnd, 0, WS_POPUP);
                // set the animation of the window
                dlg.SetAnimateType_Show(MZ_ANIMTYPE_SCROLL_RIGHT_TO_LEFT_PUSH);
                dlg.SetAnimateType_Hide(MZ_ANIMTYPE_SCROLL_LEFT_TO_RIGHT_PUSH);
                int nRet = dlg.DoModal();
                //SipShowIM(SIPF_OFF);
                m_NameEdt.SetFocus(false);

                if (nRet==ID_OK) {
                    //...
                }
                if (nRet==ID_CANCEL) {
                    // nuked
                    fQuestionableUnlockWords.clear();
                }
            }
        }
        break;
    case MZ_IDC_HACKMODE:
        {
            // Popup mode selection window
            TKOLStrList ModeList;
            for (int i = 0; i < Modes; i++) {
                ModeList.push_back(ModeNames[i]);
            }
            HackModeWnd dlg(&fNewModeIndex, &ModeList);
            RECT rcWork = MzGetWorkArea();
            dlg.Create(rcWork.left, rcWork.top, RECT_WIDTH(rcWork),RECT_HEIGHT(rcWork), m_hWnd, 0, WS_POPUP);
            // set the animation of the window
            dlg.SetAnimateType_Show(MZ_ANIMTYPE_SCROLL_RIGHT_TO_LEFT_PUSH);
            dlg.SetAnimateType_Hide(MZ_ANIMTYPE_SCROLL_LEFT_TO_RIGHT_PUSH);
            int nRet = dlg.DoModal();
            //SipShowIM(SIPF_OFF);
            m_NameEdt.SetFocus(false);
            if (nRet==ID_OK)
            {
                //...
                // update display
                if (fNewModeIndex > 0 && fNewModeIndex < Modes) {
                    m_BtnSetting1.SetText2(ModeNames[fNewModeIndex]);
                } else {
                    fNewModeIndex = 0; // safe?
                }
            }
            return;

        }
        break;
    case MZ_IDC_IMECOMBO:
        {
            // Popup IME Selection window
            // fNewIMEIndex = imehelper->GetCurrentIMEIndex(); // reset
            IMEEnumWnd dlg(&fNewIMEIndex);
            RECT rcWork = MzGetWorkArea();
            dlg.Create(rcWork.left, rcWork.top, RECT_WIDTH(rcWork),RECT_HEIGHT(rcWork), m_hWnd, 0, WS_POPUP);
            // set the animation of the window
            dlg.SetAnimateType_Show(MZ_ANIMTYPE_SCROLL_RIGHT_TO_LEFT_PUSH);
            dlg.SetAnimateType_Hide(MZ_ANIMTYPE_SCROLL_LEFT_TO_RIGHT_PUSH);
            int nRet = dlg.DoModal();
            //SipShowIM(SIPF_OFF);
            m_NameEdt.SetFocus(false);
            if (nRet==ID_OK)
            {
                //...
                if (fNewIMEIndex != imehelper->GetCurrentIMEIndex()) {
                    m_BtnSetting4.SetText2(imehelper->GetIMEInfos()[fNewIMEIndex].DisplayName);
                }                
            }
            return;
        }
        break;
    case MZ_IDC_UNINSTALL:
        {
            // an presisdent uninstall form
            UninstallFrm dlg;
            RECT rcWork = MzGetWorkArea();
            dlg.Create(rcWork.left, rcWork.top, RECT_WIDTH(rcWork),RECT_HEIGHT(rcWork), m_hWnd, 0, WS_POPUP);
            // set the animation of the window
            dlg.SetAnimateType_Show(MZ_ANIMTYPE_SCROLL_RIGHT_TO_LEFT_PUSH);
            dlg.SetAnimateType_Hide(MZ_ANIMTYPE_SCROLL_LEFT_TO_RIGHT_PUSH);
            int nRet = dlg.DoModal();
            //SipShowIM(SIPF_OFF);
            m_NameEdt.SetFocus(false);
            if (nRet==ID_OK)
            {
                //...
            }
            return;
        }
        break;
    }
}

bool CMainWnd::ApplyChanges( bool Silent )
{
    fOwnerName = m_NameEdt.GetText();

    int pendingchanges = 0;
    if (fOldOwnerName.Compare(fOwnerName) != 0) {
        pendingchanges++;
    }
    if (imehelper->GetCurrentIMEIndex() != fNewIMEIndex) {
        pendingchanges++;
    }
    if (fOldEcchiUnlock != fEcchiUnlock) {
        pendingchanges++;
    }
    if (fOldFunkkyWifi != fFunkkyWifi) {
        pendingchanges++;
    }
    if (!Silent && (pendingchanges > 0)) {
        // Calculate pending changes
        CMzStringW HintMessage(64);
        _snwprintf(HintMessage.C_Str(), 64, _tr(IDS_STRING_APPLYPENDINGCHANGES), pendingchanges);
        /*CMzWndEx ppm;
        struct PopupMenuItemProp pmip;      

        UiStatic hint;
        hint.SetPos(0, 0, ppm.GetWidth(), MZM_HEIGHT_CAPTION);
        hint.SetTextColor(0xFFFFFF);
        hint.SetText(HintMessage);
        ppm.AddUiWin(&hint);
        // ppm.SetWindowPos(NULL, ppm.GetWindowPos().x, ppm.GetWindowPos().y, ppm.GetWidth(), ppm.GetHeight() + MZM_HEIGHT_CAPTION);

        /*pmip.itemCr = MZC_BUTTON_PELLUCID;
        pmip.itemRetID = IDC_PPM_CANCEL;
        pmip.str = _tr(IDS_STRING_CANCEL);
        ppm.AddItem(pmip);

        pmip.itemCr = MZC_BUTTON_ORANGE;
        pmip.itemRetID = IDC_PPM_OK;
        pmip.str = L"确定";
        ppm.AddItem(pmip);

        //pmip.itemCr = MZC_BUTTON_NONE;
        //pmip.str = HintMessage;
        //ppm.AddItem(pmip);

        UiButton okbtn;
        okbtn.SetID(IDC_PPM_OK);
        okbtn.SetPos(0, MZM_HEIGHT_CAPTION + MZM_HEIGHT_BUTTONEX, GetWidth(), MZM_HEIGHT_BUTTONEX);
        okbtn.SetButtonType(MZC_BUTTON_ORANGE);
        okbtn.SetText(L"确定");
        ppm.AddUiWin(&okbtn);

        UiButton cancelbtn;
        cancelbtn.SetID(IDC_PPM_CANCEL);
        cancelbtn.SetPos(0, MZM_HEIGHT_CAPTION, GetWidth(), MZM_HEIGHT_BUTTONEX);
        cancelbtn.SetButtonType(MZC_BUTTON_PELLUCID);
        cancelbtn.SetText(_tr(IDS_STRING_CANCEL));
        ppm.AddUiWin(&cancelbtn);

        //ppm.SetWindowPos(NULL, ppm.GetWindowPos().x, ppm.GetWindowPos().y, ppm.GetWidth(), ppm.GetHeight() + MZM_HEIGHT_CAPTION)
        //ppm.SetWindowPos(NULL, 0, 0, GetWidth(), MZM_HEIGHT_CAPTION + MZM_HEIGHT_BUTTONEX * 2);


        RECT rc = MzGetWorkArea();      
        rc.top = rc.bottom - MZM_HEIGHT_CAPTION + MZM_HEIGHT_BUTTONEX * 2;//ppm.GetHeight();
        ppm.Create(rc.left,rc.top,RECT_WIDTH(rc),RECT_HEIGHT(rc),m_hWnd,0,WS_POPUP);      
        int nID = ppm.DoModal();
        if (nID==IDC_PPM_OK)
        {
        // do what you want...
        Silent = true;
        }
        if (nID==IDC_PPM_CANCEL)
        {
        // do what you want...
        }*/
        Silent = MzMessageBoxEx(m_hWnd, HintMessage, _tr(IDS_STRING_HINT), MZ_YESNO, false) == IDOK;
    }
    if (Silent) {
        int rebootingsteps = 0;
        // Call pascal dll
        if (imehelper->GetCurrentIMEIndex() != fNewIMEIndex) {
            CLSID NewIMEClassID = imehelper->GetIMEInfos()[fNewIMEIndex].ClassID;
            SipShowIM(SIPF_OFF);
            if (SipSetCurrentIM(&NewIMEClassID) == FALSE) {
                MzMessageBoxEx(m_hWnd, _tr(IDS_STRING_FAILDCHANGEIM), _tr(IDS_STRING_WARNING), MZ_OK, false);
                Silent = false;
            }
            //SipShowIM(SIPF_ON);
            // TODO: lock UI thread
            // this->SetEnable(false);
            delete imehelper;
            imehelper = new TIMEHelper;
        }
        BOOL MayuseConsole;
        bool ShellUpdated = (fOldEcchiUnlock != fEcchiUnlock) || (fEcchiUnlock && !(fQuestionableUnlockWords.empty()));
        if (fOldOwnerName.Compare(fOwnerName) != 0) {
            CMzStringW muifilename = CMzStringW(GetUserDefaultUILanguage() != 0x0804?muifilename:L"\\Windows\\SMS.exe.") + GetMUICode() + L".mui";
            if (fOwnerName.Compare(_tr(IDS_STRING_DEVICEHOST)) != 0){
                CMUIMaker maker;
                //CMzStringW section = CMzStringW(L"SMS-") + GetLanguageCode() + L"-Base";
                //maker.InitString(section);
                // TODO: try init from exe itself
                HINSTANCE smsinst = LoadLibraryExW(L"\\Windows\\sms.exe", NULL, LOAD_LIBRARY_AS_DATAFILE);
                for (uint stringID = 1; stringID < 65536; stringID++){
                    CMzStringW tmpstr = LoadStr(smsinst, stringID);
                    if (tmpstr.IsEmpty() || stringID == 631) {
                        continue;
                    }
                    maker.RegisterString(stringID, tmpstr);
                }
                maker.RegisterString(631, fOwnerName);
                maker.SaveToFile(muifilename, &MayuseConsole);
                fOldOwnerName = fOwnerName;
            } else {
                // remove mui
                UtilNukeFile(muifilename, &MayuseConsole);
            }
            if (MayuseConsole) {
                rebootingsteps++;
            }
        }
        if (ShellUpdated) {
            CMzStringW muifilename = CMzStringW(L"\\Windows\\MiniOneShell.EXE.") + GetMUICode() + L".mui";
            if (fEcchiUnlock) {
                CMUIMaker maker;
                //CMzStringW section = CMzStringW(L"MiniOneShell-") + GetLanguageCode() + L"-";
                //maker.InitString(section + L"Base");
                //maker.InitString(section + L"Ext");
                HINSTANCE smsinst = LoadLibraryExW(GetUserDefaultUILanguage() != 0x0804?muifilename:L"\\Windows\\MiniOneShell.EXE", NULL, LOAD_LIBRARY_AS_DATAFILE);
                for (uint stringID = 1; stringID < 65536; stringID++){
                    CMzStringW tmpstr = LoadStr(smsinst, stringID);
                    if (tmpstr.IsEmpty()) {
                        continue;
                    }
                    maker.RegisterString(stringID, tmpstr);
                }
                if (fQuestionableUnlockWords.empty() == false) {
                    for (list <tagEditRecItem>::iterator it = fQuestionableUnlockWords.begin(); it != fQuestionableUnlockWords.end(); it++) {
                        maker.RegisterString((*it).FieldID, (*it).Value);
                    }
                }
                maker.SaveToFile(muifilename, &MayuseConsole);
                fOldEcchiUnlock = fEcchiUnlock;
            } else {
                UtilNukeFile(muifilename, &MayuseConsole);
            }
            if (MayuseConsole) {
                rebootingsteps++;
            }
        }
        if (fOldFunkkyWifi != fFunkkyWifi) {
            CMzStringW muifilename = CMzStringW(L"\\Windows\\Setting.exe.") + GetMUICode() + L".mui";
            if (fFunkkyWifi) {
                CMUIMaker maker;
                CMzStringW section = CMzStringW(L"Setting-") + GetLanguageCode() + L"-";
                //maker.InitString(section + L"Base");
                HINSTANCE smsinst = LoadLibraryExW(GetUserDefaultUILanguage() != 0x0804?muifilename:L"\\Windows\\Setting.exe", NULL, LOAD_LIBRARY_AS_DATAFILE);
                for (uint stringID = 1; stringID < 65536; stringID++){
                    CMzStringW tmpstr = LoadStr(smsinst, stringID);
                    if (tmpstr.IsEmpty()) {
                        continue;
                    }
                    maker.RegisterString(stringID, tmpstr);
                }
                maker.InitString(section + L"Ext");
                maker.SaveToFile(muifilename, &MayuseConsole);
                fOldEcchiUnlock = fEcchiUnlock;
            } else {
                UtilNukeFile(muifilename, &MayuseConsole);
            }
            if (MayuseConsole) {
                rebootingsteps++;
            }
        }
        if (rebootingsteps > 0) {
            CMzStringW HintMessage(64);
            _snwprintf(HintMessage.C_Str(), 64, _tr(IDS_STRING_CLEANLOCKFILES), rebootingsteps);
            if (MzMessageBoxEx(m_hWnd, HintMessage, _tr(IDS_STRING_HINT), MZ_YESNO, false) == IDOK) {
                MzResetSystem();
            }
        }
        if (ShellUpdated) {
            if (MzMessageBoxEx(m_hWnd, _tr(IDS_STRING_LOADUNLOCKWORDS), _tr(IDS_STRING_HINT), MZ_YESNO, false) == IDOK) {
                MzResetSystem();
            }
        }
    }
    return Silent;
}

CMainWnd::CMainWnd()
{
    // may 1st function in theApp
    SateHandle = LoadLibraryExW(GetStartDir() + L"Res" + GetLanguageCode() + L".dll", NULL, LOAD_LIBRARY_AS_DATAFILE);
    stringstore = new TStringStore(SateHandle);
    imehelper = new TIMEHelper;

    fNewModeIndex = 0;
    fNewIMEIndex = imehelper->GetCurrentIMEIndex();
    if (imehelper->GetIMECount() <= 0) {
        fNewIMEIndex = -1;
    }
    if (fNewIMEIndex >= imehelper->GetIMECount()) {
        fNewIMEIndex = imehelper->GetIMECount() - 1;
    }
    fOldOwnerName = _tr(IDS_STRING_DEVICEHOST);
    HINSTANCE smsinst = LoadLibraryExW(L"\\Windows\\sms.exe", NULL, LOAD_LIBRARY_AS_DATAFILE);
    /*WCHAR namebuf[26];
    if (LoadStringW(smsinst, 631, namebuf, 26) > 0) {
        fOwnerName = fOldOwnerName = namebuf;
    }*/
    fOwnerName = LoadStr(smsinst, 631);
    if (fOwnerName.IsEmpty()) {
        fOwnerName = fOldOwnerName;
    } else {
        fOldOwnerName = fOwnerName;
    }
    FreeLibrary(smsinst);

    CMzStringW shellmuifilename = CMzStringW(L"\\Windows\\MiniOneShell.EXE.") + GetMUICode() + L".mui";
    CMzStringW settingmuifilename = CMzStringW(L"\\Windows\\Setting.exe.") + GetMUICode() + L".mui";
    if (GetUserDefaultUILanguage() == 0x0804) {
        fEcchiUnlock = fOldEcchiUnlock = FileExists(shellmuifilename);
        fFunkkyWifi = fOldFunkkyWifi = FileExists(settingmuifilename);
    } else {
        // TODO: more accrue test
    }

    UnlockFieldNames[0] = _tr(IDS_STRING_UNLOCK);
    UnlockFieldNames[1] = _tr(IDS_STRING_TALK);
    UnlockFieldNames[2] = _tr(IDS_STRING_HANGUP);
    UnlockFieldNames[3] = _tr(IDS_STRING_SMS);
    UnlockFieldNames[4] = _tr(IDS_STRING_MUSIC_NEXT);
    UnlockFieldNames[5] = _tr(IDS_STRING_MUSIC_PREV);
    UnlockFieldNames[6] = _tr(IDS_STRING_MUSIC_PAUSE);
    UnlockFieldNames[7] = _tr(IDS_STRING_MUSIC_CONTINUE);
    /*for (int i = 0; i < UnlockFields; i++) {
        UnlockFieldNames[0] = _tr(IDS_STRING_UNLOCK + i);
    }*/
    ModeNames[0] = _tr(IDS_STRING_HEAVYQND);
    ModeNames[1] = _tr(IDS_STRING_LIGHT631);
    ModeNames[2] = _tr(IDS_STRING_MISSABLE);

    ClickyCount = 0;
}

CMainWnd::~CMainWnd()
{
    delete imehelper;
    delete stringstore;
    if (SateHandle) {
        FreeLibrary(SateHandle);
    }
}

CMzStringW GetMUICode()
{
    return Int2Hex(GetUserDefaultUILanguage(), 4);
}