//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Addon.rc
//
#define IDR_PNG_SETTINGS                101
#define IDR_PNG_BANNER                  102
#define IDR_PNG_CHECKOFF                103
#define IDR_PNG_CHECKON                 104
#define IDS_STRING_OK                   105
#define IDS_STRING_CANCEL               106
#define IDS_STRING_APPLY                107
#define IDS_STRING_EXIT                 108
#define IDS_STRING_A4IM                 109
#define IDS_STRING_HEAVYQND             110
#define IDS_STRING_LIGHT631             111
#define IDS_STRING_MISSABLE             112
#define IDS_STRING_UNLOCK               113
#define IDS_STRING_TALK                 114
#define IDS_STRING_HANGUP               115
#define IDS_STRING_SMS                  116
#define IDS_STRING_MUSIC_NEXT           117
#define IDS_STRING_MUSIC_PREV           118
#define IDS_STRING_MUSIC_PAUSE          119
#define IDS_STRING_MUSIC_CONTINUE       120
#define IDS_STRING_OWNERNAME            121
#define IDS_STRING_HACKSTYLE            122
#define IDS_STRING_ECCHIUNLOCK          123
#define IDS_STRING_FUNKKYWAPI           124
#define IDS_STRING_CURRENTIM            125
#define IDS_STRING_ADDORREMOVE          126
#define IDS_STRING_APPLYPENDINGCHANGES  127
#define IDS_STRING_HINT                 128
#define IDS_STRING_FAILDCHANGEIM        129
#define IDS_STRING_WARNING              130
#define IDS_STRING_DEVICEHOST           131
#define IDS_STRING_CLEANLOCKFILES       132
#define IDS_STRING_LOADUNLOCKWORDS      133
#define IDS_STRING_RETURN               134
#define IDS_STRING_NOTHINGTOUNINSTALL   135
#define IDS_STRING_REMOVECONFIRM        136
#define IDS_STRING_UNINSTALL            137
#define IDS_STRING_OWNERNAMEWIDTH       138
#define IDS_STRING_UNLOCKINACTIVE       139
#define IDS_STRING_DAYHANDIM            140

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
