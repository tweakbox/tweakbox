#include <time.h>
#include <cstdlib>
#include <mzfc/MzCommonDlg.h>
#include "FvMzKOL.h"

/*using std::srand;
using std::rand;*/

void TObj::Free()
{
	if (this != NULL) {
		DoDestroy();
	}
}

void TObj::Final()
{
    if (fAutoFree != NULL) {
        /*for (QList<UFUNCTION>::iterator it = fAutoFree->begin(); it != fAutoFree->end(); it++) {
            //(it->Data->*(it->Code))();
            delete it->Data;
        }*/
        delete fAutoFree;
    }
}

void TObj::DoDestroy()
{
	/*if fRefCount <> 0 then
		begin
		if not LongBool( fRefCount and 1) then
			Dec( fRefCount );
	end
		else
		Destroy;*/
}

TObj::TObj() : fAutoFree(NULL)
{

}

TObj::~TObj()
{
	Final();
}

void TObj::Add2AutoFree( PObj Obj )
{
/*	if (fAutoFree == NULL) {
        fAutoFree = new list<UFUNCTION>;
	}
    UFUNCTION func;
    func.Data = Obj;
    func.Code = &TObj::Free;
    fAutoFree->push_back(func);*/
}

void ShowMessage(const CMzStringW& msg, HWND hwnd /* = 0 */) {
    MzMessageBoxEx(hwnd, msg.C_Str(), _T(""), MZ_OK, 0);
}

KOLString GetStartDir() {
    HMODULE hmodule;
    hmodule = GetModuleHandleW(NULL);
    WCHAR path[MAX_PATH];
    int i = GetModuleFileNameW(hmodule, path, MAX_PATH);

#ifdef _WIN32
#ifdef _MSC_VER
    WCHAR detm = L'\\';
#endif
#elif defined _LINUX
    WCHAR detm = L'/';
#endif
    for (; i >=0 ; i--) {
        if (path[i] == detm) {
            path[i + 1] = 0;
            break;
        }
    }
    KOLString result = path;
    return result;
}

KOLString ExcludeTrailingChar( const KOLString S, KOLChar C ) {
    KOLString Result = S;
    if (!Result.IsEmpty()) {
        if (Result.C_Str()[Result.Length() - 1] == C) {
            Result = Result.SubStr(0, Result.Length() - 1);
        }
    }
    return Result;
}

KOLString IncludeTrailingChar( const KOLString S, KOLChar C ) {
    KOLString Result = S;
    bool mayok = false;
    if (Result.IsEmpty()) {
        mayok = true;
    } else if (Result.C_Str()[Result.Length() - 1] != C) {
        mayok = true;
    }
    if (mayok) {
        Result = Result + KOLString(C);
    }
    return Result;
}

KOLString ExcludeTrailingPathDelimiter(const KOLString S) {
#ifdef _WIN32
#ifdef _MSC_VER
    WCHAR detm = L'\\';
#endif
#elif defined _LINUX
    WCHAR detm = L'/';
#endif
    return ExcludeTrailingChar( S, detm );
}

KOLString IncludeTrailingPathDelimiter(const KOLString S) {
#ifdef _WIN32
#ifdef _MSC_VER
    WCHAR detm = L'\\';
#endif
#elif defined _LINUX
    WCHAR detm = L'/';
#endif
    return IncludeTrailingChar( S, detm );
}

KOLChar* __DelimiterLast( KOLChar* Str, KOLChar* Delimiters ) {
    PKOLChar P, F;

    P = Str;
    PKOLChar Result = P + _tcslen( Str );
    while (*Delimiters != 0)
    {
        F = _tcsrchr( P, *Delimiters );
        if (F != NULL) {
            if (*Result == 0 || unsigned(F) > unsigned(Result)) 
                Result = F;
        }
        Delimiters++;
    }
    return Result;
}

KOLString ExtractFilePath( const KOLString Path ) {
    PKOLChar P, P0;

    P0 = Path.C_Str();
    P = __DelimiterLast( P0, _T(":\\/") );
    if (*P == 0) {
        return _T("");
    } else {
        return KOLString(Path).SubStr(0, P - P0 + 1);
    }
}

KOLString ExtractFileName( const KOLString Path ) {
    PKOLChar P = __DelimiterLast( Path.C_Str(), _T(":\\/") );
    if (*P == 0) {
        return Path;
    } else {
        return KOLString(P + 1);
    }
}

KOLString ExtractFileExt( const KOLString Path ) {
    PKOLChar P = __DelimiterLast( PKOLChar( Path ), _T(".") );
    return P;
}

KOLString GetLanguageCode() {
    LANGID locale = GetUserDefaultUILanguage();

    // This switch will translate to the correct locale
    switch (locale) {
            case 0x0407://QLocale::German:
                return L"DEU";
            case 0x040C://QLocale::French:
                return L"FRA";
            case 0x0804://QLocale::Chinese:
                return L"CHS";
            case 0x0c04://QLocale::HongKong:
            case 0x1404://QLocale::Macau:
            case 0x0404://QLocale::Taiwan:
                return L"CHT";
                /*case QLocale::Spanish:
                return "spa";*/
            case 0x0411://QLocale::Japanese:
                return L"JPN";
            case 0x0412://QLocale::Korean:
                return L"KOR";
            default:
                return L"ENU";
    }
}

KOLString Int2Str( int Value )
{
    WCHAR buf[16];
    WCHAR* dst;
    bool minus = (Value < 0);
    if (minus) {
        Value = 0 - Value;
    }
    dst = &buf[15];
    *dst = 0;
    unsigned d = Value;
    do {
        dst--;
        *dst = (d % 10) + '0';
        d = d / 10;
    } while (d > 0);
    if (minus) {
        dst--;
        *dst = '-';
    }

    return KOLString(dst);
}

time_t time( time_t *inTT ) { 
    SYSTEMTIME sysTimeStruct; 
    FILETIME fTime; 
    ULARGE_INTEGER int64time; 
    time_t locTT = 0; 

    if ( inTT == NULL ) { 
        inTT = &locTT; 
    } 

    GetSystemTime( &sysTimeStruct ); 
    if ( SystemTimeToFileTime( &sysTimeStruct, &fTime ) ) { 
        memcpy( &int64time, &fTime, sizeof( FILETIME ) ); 
        /* Subtract the value for 1970-01-01 00:00 (UTC) */ 
        int64time.QuadPart -= 0x19db1ded53e8000; 
        /* Convert to seconds. */ 
        int64time.QuadPart /= 10000000; 
        *inTT = int64time.QuadPart; 
    } 

    return *inTT; 
} 


static bool randomized = false;
int random(int from, int to) {
    int result;
    if (!randomized) {
        srand((int) time(NULL));
        randomized = true;
    }
    //result = floor((double(rand())/ RAND_MAX) * (to - from)) + from;
    result = rand()%(to - from + 1) + from;
    return result;
}

HKEY RegKeyOpenRead( HKEY Key, const KOLString SubKey )
{
    HKEY Result;
    if (RegOpenKeyEx( Key, SubKey.C_Str(), 0, KEY_READ, &Result ) != ERROR_SUCCESS) {
        Result = 0;
    }
    return Result;
}

HKEY RegKeyOpenWrite( HKEY Key, const KOLString SubKey )
{
    HKEY Result;
    if (RegOpenKeyEx( Key, SubKey.C_Str(), 0, KEY_READ | KEY_WRITE, &Result ) != ERROR_SUCCESS) {
        Result = 0;
    }
    return Result;
}

HKEY RegKeyOpenCreate( HKEY Key, const KOLString SubKey )
{
    HKEY Result;
    DWORD dwDisp;
    if (RegCreateKeyEx( Key, SubKey.C_Str(), 0, NULL, 0, KEY_ALL_ACCESS, NULL, &Result, &dwDisp ) != ERROR_SUCCESS) {
        Result = 0;
    }
    return Result;
}


KOLString RegKeyGetStr( HKEY Key, const KOLString ValueName )
{
    DWORD dwType, dwSize;
    //LPTSTR Buffer;

    if (Key == 0) return KOLString(L"");
    dwSize = 0;
    //if not Query or (dwType <> REG_SZ) then Exit;
    if (RegQueryValueEx( Key, ValueName.C_Str(), NULL, &dwType, LPBYTE(NULL), &dwSize ) == ERROR_SUCCESS) {
        if (dwType == REG_SZ) {
            //
            //Buffer = new TCHAR(dwSize + 1);
            TCHAR * DestBuf = LPTSTR(malloc(sizeof(TCHAR)*(dwSize + 1)));
            DestBuf[dwSize] = '\0';
            if (RegQueryValueEx( Key, ValueName.C_Str(), NULL, &dwType, LPBYTE(DestBuf), &dwSize ) == ERROR_SUCCESS) {
                const KOLString Result = KOLString(DestBuf);
                free(DestBuf);
                return Result;
            }
        }
    }
    return KOLString(L"");
}

bool RegKeyGetSubKeys( const HKEY Key, PKOLStrList List )
{
    bool Result = false;
    DWORD Size, NumSubKeys, MaxSubKeyLen;
    KOLString KeyName;

    List->clear();
    if (RegQueryInfoKey(Key, NULL, NULL, NULL, &NumSubKeys, &MaxSubKeyLen, NULL, NULL, NULL, NULL, NULL, NULL) == ERROR_SUCCESS) {
        if (NumSubKeys > 0) {
            for (int i = 0; i < NumSubKeys; i++) {
                Size = MaxSubKeyLen + 1;
                KeyName.SetBufferSize(Size);

                RegEnumKeyEx(Key, i, KeyName.C_Str(), &Size, NULL, NULL, NULL, NULL);
                KeyName = KeyName.SubStr(0, _tcslen(KeyName.C_Str()));

                List->push_back(KeyName);
            }
        }
    }
    return Result;
}

bool RegKeyValExists( HKEY Key, const KOLString ValueName )
{
    DWORD dwType, dwSize;
    return (Key != 0) &&  (RegQueryValueEx( Key, ValueName.C_Str(), NULL, &dwType, NULL, &dwSize ) == ERROR_SUCCESS);
}

bool RegKeySetStr( HKEY Key, const KOLString ValueName, const KOLString Value )
{
    return (Key != 0) && (RegSetValueEx( Key, ValueName.C_Str(), 0,
            REG_SZ, LPBYTE(Value.C_Str()),
            (Value.Length() + 1) * sizeof(TCHAR) ) == ERROR_SUCCESS);
}

void RegKeyClose( HKEY Key )
{
    if (Key != 0) {
        RegCloseKey( Key );
    }
}

DWORD RegKeyGetDw( HKEY Key, const KOLString ValueName )
{
    DWORD dwType, dwSize;

    dwSize = sizeof( DWORD );
    DWORD Result = 0;
    if ((Key == 0) ||
        (RegQueryValueEx( Key, ValueName.C_Str(), NULL, &dwType, LPBYTE(&Result), &dwSize ) != ERROR_SUCCESS)
        || (dwType != REG_DWORD) ) Result = 0;
    return Result;
}

bool FileExists( const KOLString FileName )
{
    int Code = GetFileAttributes(FileName.C_Str());
    return ((Code != -1) && ((FILE_ATTRIBUTE_DIRECTORY & Code) == 0));
}

long long FileSize( const KOLString Path ) {
    TFindFileData FD;

    long long Result = 0;
    if (!Find_First( Path, FD )) return Result;
    Result = FD.nFileSizeLow;
    *LPDWORD(LPBYTE(&Result) + 4)  = FD.nFileSizeHigh;
    Find_Close( FD );
    return Result;
}


int S2Int( LPWSTR S )
{
    int M;
    int Result = 0;
    if (S == NULL) 
        return Result;
    M = 1;
    if (*S == '-') {
        M = -1;
        S++;
    } else if (*S == '+'){
        S++;
    }
    //while S^ in [ '0'..'9' ] do
    while (*S >= '0' && *S <= '9')
    {
        Result = Result * 10 + int( *S ) - int( '0' );
        S++;
    }
    if (M < 0){
        Result = -Result;
    }
    return Result;
}

int Str2Int( const KOLString Value )
{
    return S2Int(Value.C_Str());
}

unsigned char QuadBit2Hex(unsigned char num) {
    if (num < 10) {
        return num + '0';
    } else {
        return num + '7';
    }
}

KOLString Int2Hex( DWORD Value, int Digits )
{
    WCHAR Buf[9];
    LPWSTR Dest;

    Dest = &Buf[8];
    *Dest = 0;
    do {
        Dest--;
        *Dest = '0';
        if (Value != 0) {
            *Dest = QuadBit2Hex(Value & 0xF);
            Value = Value >> 4;
        }
        Digits--;
    } while (Value != 0 || Digits > 0);
    return Dest;
}

KOLString Num2Bytes( double Value ) {
    int V, I = 0;
    while (Value >= 1024 && I < 4)
    {
        I++;
        Value = Value / 1024.0;
    }
    KOLString Result = Int2Str( int( Value ) );
    V = int( (Value - int( Value )) * 100 );
    if (V != 0)
    {
        if ((V % 10) == 0)
            V = V / 10;
        Result = Result + _T(".") + Int2Str( V );
    }
    if (I > 0) {
        KOLChar Suffix[5] = _T("KMGT");//{L'K', L'M', L'G', L'T'};
        Result = Result + _T("?");
        Result.C_Str()[Result.Length() - 1] = Suffix[ I - 1 ];
    }
    return Result;
}

bool Find_First( const KOLString FilePathName, TFindFileData& F ) {
    F.FindHandle = FindFirstFile( FilePathName.C_Str(), &F );
    return F.FindHandle != INVALID_HANDLE_VALUE;
}

bool Find_Next( TFindFileData& F )
{
    return FindNextFile( F.FindHandle, &F ) == TRUE;
}
void Find_Close( TFindFileData& F ) {
    ::FindClose( F.FindHandle );
}

KOLString FindFilter(const KOLString Filter )
{
    KOLString Result = Filter;
    if (Result.IsEmpty()) {
        Result = _T("*.*");
    }
    return Result;
}

WideString WAnsiLowerCase(const WideString S ){
    int Len;
    WideString Result = S;
    Len = S.Length();
    if (Len > 0) {
        CharLowerBuffW(Result.C_Str(), Len);
    }
    return Result;
}

bool StrSatisfy( const KOLString S, const KOLString Mask ){
    return _StrSatisfy( WAnsiLowerCase(S),  WAnsiLowerCase(Mask) );
}

bool _StrSatisfy( KOLChar* S, KOLChar* Mask ){
    bool Result = true;
    while (Result) {
        Result = true;
        if (KOLString(S).IsEmpty() && KOLString(Mask).IsEmpty()) return Result;
        if (KOLString(Mask) == _T("*") && Mask[1] == 0) return Result;
        if (KOLString(S).IsEmpty()) {
            while (*Mask == '*')
                Mask++;
            Result = *Mask == 0;
            return Result;
        }
        Result = false;
        if (*Mask == 0) return Result;
        if (*Mask == '?') {
            S++; Mask++; continue;
        }
        if (*Mask == '*') {
            Mask++;
            while (*S != 0) {
                Result = _StrSatisfy( S, Mask );
                if (Result) return Result;
                S++;
            }
            return Result; // (Result == False)
        }
        Result = KOLString(S) == KOLString(Mask);
        S++; Mask++;
        if (Result) continue;
    }
    return Result;
}

bool TDirList::SatisfyFilter( KOLChar* FileName, DWORD FileAttr, DWORD FindAttr ) {
    KOLChar* F;
    bool HasOnlyNegFilters;

    bool Result = (((FileAttr & FindAttr) == FindAttr) ||
        BOOL(FindAttr & FILE_ATTRIBUTE_NORMAL));
    if (!Result) return Result;

    if (FileName != _T(".") &&  FileName != _T("..")){
        if (BOOL( FindAttr & FILE_ATTRIBUTE_NORMAL ) && (FindAttr != FILE_ATTRIBUTE_NORMAL)) {
            if (BOOL( FindAttr & FILE_ATTRIBUTE_DIRECTORY ) &&  BOOL( FileAttr & FILE_ATTRIBUTE_DIRECTORY )) return Result;
        }

    }

    HasOnlyNegFilters = true;
    for (TKOLStrList::iterator it = fFilters.begin(); it != fFilters.end(); ++it)
    {
        F = *it;
        if (it->IsEmpty()) continue;

        if (*it == _T(".") || *it == _T("..")) {
            if (*it == KOLString(FileName)) {
                return Result;
            }
        } else {
            if (KOLString(FileName) == _T(".") || KOLString(FileName) == _T(".."))
                continue;
        }

        if (F[ 0 ] == _T('^')) {
            if (StrSatisfy( FileName, &F[ 1 ] ) )
            {
                Result = false;
                return Result;
            }
        }  else {
            HasOnlyNegFilters = false;
            if (StrSatisfy( FileName, F ) ) {
                Result = true;
                return Result;
            }
        }
    }

    Result = HasOnlyNegFilters &&
        FileName != _T(".") && FileName != _T("..");
    return Result;
}

void TDirList::ScanDirectory( const KOLString DirPath, const KOLString Filter, DWORD Attr )
{
    Clear();
    FPath = DirPath;
    if (FPath.IsEmpty()) return;
    FPath = IncludeTrailingPathDelimiter(FPath);
    if (fFilters.empty()){
        if (Filter == _T("*.*")) {
            fFilters.push_back(_T("*"));
        } else {
            fFilters.push_back(Filter);
        }
    }
    int Action = -1;
    TFindFileData FindData;
    if (!Find_First(FPath + FindFilter(Filter), FindData))
        return;
    while (true) {
        if (SatisfyFilter(&FindData.cFileName[0], FindData.dwFileAttributes, Attr)) {
            Action = diAccept;
            /*
            if Assigned( OnItem ) then
            OnItem( @Self, FindData, Action );
            */
            if (Action == diCancel) {
                break;
            } else if (Action == diAccept) {
                FList.push_back(FindData);
            } else if (Action == diSkip) {

            }
        }
        if (!Find_Next(FindData)) break;
    }
    Find_Close(FindData);
    fFilters.clear();
}

void TDirList::ScanDirectoryEx( const KOLString DirPath, const KOLString Filters, DWORD Attr )
{
    KOLString F, FF;

    FF = Filters;
    do {
        F = Trim( Parse( FF, _T(";") ) );
        if (!F.IsEmpty())
            fFilters.push_back( F );
    } while (!FF.IsEmpty());
    ScanDirectory( DirPath, _T(""), Attr );
}

void TDirList::Clear()
{
    FList.clear(); // FList := NewList;
}

int TDirList::GetCount()
{
    return FList.size();
}

PFindFileData TDirList::Get( int Idx ) {
    return &FList[Idx]; // Dangerous
}

KOLString TDirList::GetNames( int Idx )
{
    return KOLString(FList[Idx].cFileName);
}

bool TDirList::GetIsDirectory( int Idx )
{
    return BOOL( FList[Idx].dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) == TRUE;
}

KOLString Parse( KOLString& S, const KOLString Separators ) {
    int Pos = IndexOfCharsMin( S, Separators );
    if (Pos <= 0)
        Pos = S.Length() + 1;
    KOLString Result = S;
    S = Result.SubStr(Pos, Result.Length() - Pos); //Copy( Result, Pos + 1, MaxInt );
    Result = Result.SubStr( 0, Pos - 1 );
    return Result;
}

int IndexOfCharsMin( const KOLString S, const KOLString Chars )
{
    int I, J;
    int Result = -1;
    for (I = 0; I < Chars.Length(); I++ )
    {
        J = IndexOfChar( S, Chars.C_Str()[ I ] );
        if (J > 0)
        {
            if (Result <= 0  || J < Result) 
                Result = J;
        }
    }
    return Result;
}

int IndexOfChar( const KOLString S, KOLChar Chr )
{
    PKOLChar P, F;
    P = S.C_Str();
#ifdef INPACKAGE
    F = StrScan( P, Chr );
#else
    F = StrScanLen( P, Chr, S.Length() );
#endif
    int Result = -1;
    if (F == NULL  || S.IsEmpty()) return Result;
    Result = (int( F ) - int( P )) 
#ifdef UNICODE
    / (sizeof KOLChar)
#endif
#ifdef INPACKAGE
    + 1
#endif
    ; // by byte

    if /*(Result > Length(S)) or*/ (S.C_Str()[ Result - 1 ] != Chr) 
        Result = -1;
    return Result;
}

PKOLChar StrScanLen( PKOLChar Str, KOLChar Chr, int Len )
{
    PKOLChar Result = Str;
    while (Len > 0) {
        if (*Result == Chr) {
            Result++;
            break;
        }
        Result++;
        Len--;
    }
    return Result;
}

KOLString TrimLeft( const KOLString S )
{
    int Len = S.Length();
    int I = 0;
    while (I < Len && S.C_Str()[I] <= _T(' ')) { I++; }
    return KOLString(S).SubStr( I, Len - I);
}

KOLString TrimRight( const KOLString S )
{
    int I = S.Length() - 1;
    while (I >= 0 && S.C_Str()[I] <= _T(' ')) { I--; }
    return KOLString(S).SubStr( 0, I + 1 );
}

KOLString Trim( const KOLString S )
{
    return TrimLeft( TrimRight( S ) );
}

int AnsiCompareStrNoCase(LPWSTR S1, LPWSTR S2)
{
    return CompareString(LOCALE_USER_DEFAULT, NORM_IGNORECASE, S1, -1,
        S2, -1) - 2;
}

