#ifndef _FVMZ_UNDERLAY_H
#define _FVMZ_UNDERLAY_H

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Simulation of MzDDrawOverlay Helper for window0.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <ddraw.h> // lib: ddraw.lib

#define RGB565(r,g,b)            (((r << 8) & 0xF800) | ((g << 3) & 0x07E0) | ((b >> 3) & 0x001F))

enum UnderlayPixelFormat
{
    PixFmtUnknown = -1,
    PixFmtYV12 = 0,
    PixFmtYUV422,
    PixFmtUYVY,
    PixFmtRGB555,
    PixFmtRGB565,
    PixFmtRGB24,
    PixFmtYUV420,
    PixFmtCount
};

class UnderlayHelper {
private:
public:
    UnderlayHelper();
    ~UnderlayHelper();
public:
    bool Init(HWND hWnd, int x, int y, int width, int height, COLORREF colorKey, BYTE foreAlpha, BYTE backAlpha, bool needBackBuffer, UnderlayPixelFormat pixFmt = PixFmtRGB565);
    void Clear();
    bool LockData(DDSURFACEDESC *pDDSD);
    bool UnLockData(bool flip = true);
    void ShowUnderlay();
    void HideUnderlay();

};

#endif