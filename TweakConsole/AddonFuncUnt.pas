﻿unit AddonFuncUnt;

{$H+}

interface

uses
  Windows, KOL, MzCommonDlg;

function ConsoleMain(): Boolean;
function ForceFiles(IniFilaname: WideString): Boolean;

implementation

function ConsoleMain(): Boolean;
var
  IniFilename: WideString;
  InitKey: HKEY;
begin
  Result := True;
  IniFilename := GetStartDir() + 'TweakConsole.ini';
  if FileExists(IniFilename) then
  begin
    Result := ForceFiles(IniFilename);
  end;
  if Result then
  begin
    // delete self
    DeleteFile(PWideChar(IniFilename));
    //MoveFileEx(PWideChar(ParamStr(0)), nil, MOVEFILE_DELAY_UNTIL_REBOOT);
    InitKey := RegKeyOpenWrite(HKEY_LOCAL_MACHINE, 'init');
    Result := RegKeyDelete(InitKey, 'Launch70');
    RegKeyClose(InitKey);
  end
  else
  begin
{$IFDEF wince}
    MzMessageBoxEx(0, 'TweakBox自动操作出错', '');
{$ELSE}
    ShowMessage('TweakBox自动操作出错');
{$ENDIF}
  end;
end;

function ForceFiles(IniFilaname: WideString): Boolean;
var
  IniFile: PIniFile;
  Names: PWStrList;
  I, tdivpos: Integer;
  DestFilename: WideString;
begin
  Result := True;
  IniFile := OpenIniFile(IniFilaname);

  IniFile.Mode := ifmRead;

  IniFile.Section := 'Delete';
  Names := NewWStrList();
  IniFile.SectionData(Names);

  for I := 0 to Names.Count - 1 do
  begin
    //DestFilename := IniFile.ValueString(Names.Items[I], '');
    tdivpos:=Pos('=', Names.Items[I]);
    DestFilename := Copy(Names.Items[I], tdivpos + 1, Length(Names.Items[I])-tdivpos);

    if FileExists(DestFilename) then
    begin
      if DeleteFile(PWideChar(DestFilename)) = False then
        Result := False;
    end;
  end;
  Free_And_Nil(Names);

  Free_And_Nil(IniFile);
end;

end.

