// TweakSetup.cpp : Defines the entry point for the DLL application.
//
#include <Windows.h>
#include <ce_setup.h>

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

codeINSTALL_INIT
Install_Init(
             HWND        hwndParent,
             BOOL        fFirstCall,     // is this the first time this function is being called?
             BOOL        fPreviouslyInstalled,
             LPCTSTR     pszInstallDir
             )
{
    // TODO: Add custom installation code here

    // To continue installation, return codeINSTALL_INIT_CONTINUE
    // If you want to cancel installation, 
    // return codeINSTALL_EXIT_UNINSTALL
    return codeINSTALL_INIT_CONTINUE;

}



codeINSTALL_EXIT
Install_Exit(
             HWND    hwndParent,
             LPCTSTR pszInstallDir,
             WORD    cFailedDirs,
             WORD    cFailedFiles,
             WORD    cFailedRegKeys,
             WORD    cFailedRegVals,
             WORD    cFailedShortcuts
             )
{
    // TODO: Add custom installation code here

    // To exit the installation DLL normally, 
    // return codeINSTALL_EXIT_DONE
    // To unistall the application after the function exits,
    // return codeINSTALL_EXIT_UNINSTALL
    return codeINSTALL_EXIT_DONE;
}

codeUNINSTALL_INIT
Uninstall_Init(
               HWND        hwndParent,
               LPCTSTR     pszInstallDir
               )
{
    // TODO: Add custom uninstallation code here

    // To continue uninstallation, return codeUNINSTALL_INIT_CONTINUE
    // If you want to cancel installation,
    // return codeUNINSTALL_INIT_CANCEL
    return codeUNINSTALL_INIT_CONTINUE;
}

codeUNINSTALL_EXIT
Uninstall_Exit(
               HWND    hwndParent
               )
{
    // TODO: Add custom uninstallation code here

    return codeUNINSTALL_EXIT_DONE;
}